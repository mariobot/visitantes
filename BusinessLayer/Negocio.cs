﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;


namespace BusinessLayer
{
    public class Negocio
    {

        public Negocio()
        { 
        }


        public DataTable GetUsuarioByEmail(string strCorreo)
        {
            try
            {
                DataTable dtResult = new DataTable();
                DataLayer.Datos Data = new DataLayer.Datos();
                dtResult = Data.GetUsuarioByEmail(strCorreo);
                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListUsuarios()
        {
            try
            {
                DataTable dtResult = new DataTable();
                DataLayer.Datos Data = new DataLayer.Datos();
                dtResult = Data.ListUsuarios();
                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetVisitantes(string strIdentificacion, DateTime dtFechaIni, DateTime dtFechaFin, string strUserId)
        {
            try
            {
                DataTable dtResult = new DataTable();
                DataLayer.Datos Data = new DataLayer.Datos();
                dtResult = Data.GetVisitantes(strIdentificacion, dtFechaIni, dtFechaFin, strUserId);
                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable GetVisitantesInforme(string strIdentificacion, DateTime dtFechaIni, DateTime dtFechaFin, string strUserId)
        {
            try
            {
                DataTable dtResult = new DataTable();
                DataLayer.Datos Data = new DataLayer.Datos();
                dtResult = Data.GetVisitantesInforme(strIdentificacion, dtFechaIni, dtFechaFin, strUserId);
                return dtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string AddUsuario(string strNombres, string strCorreo, string strPerfil, bool bitPiso1, bool bitPiso14, string strCreadoPor)
        {
            try
            {
                string strResult = string.Empty;
                DataLayer.Datos Data = new DataLayer.Datos();
                strResult = Data.AddUsuario(strNombres, strCorreo, strPerfil, bitPiso1, bitPiso14, strCreadoPor);
                return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteUsuario(string strId)
        {
            try
            {
                string strResult = string.Empty;
                DataLayer.Datos Data = new DataLayer.Datos();
                strResult = Data.DeleteUsuario(strId);
                return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public string AddVisitante(string strIdentificacion, string strNombres, string strEmpresa, DateTime dtFechaVisita, string strUserId, string strAutorizador, string strCorreoAutoriza, string strCreadoPor)
        {
            try
            {
                string strResult = string.Empty;
                DataLayer.Datos Data = new DataLayer.Datos();
                strResult = Data.AddVisitante(strIdentificacion, strNombres, strEmpresa, dtFechaVisita, strUserId, strAutorizador, strCorreoAutoriza, strCreadoPor);
                return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string UpdateVisitante(string strId, bool btPiso1, bool btPiso14, string strModificador)
        {
            try
            {
                string strResult = string.Empty;
                DataLayer.Datos Data = new DataLayer.Datos();
                strResult = Data.UpdateVisitante(strId, btPiso1, btPiso14, strModificador);
                return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteVisitante(string strId)
        {
            try
            {
                string strResult = string.Empty;
                DataLayer.Datos Data = new DataLayer.Datos();
                strResult = Data.DeleteVisitante(strId);
                return strResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
