USE [VISITANTES]
GO

/****** Object:  StoredProcedure [dbo].[Usuarios_GetByEmail]    Script Date: 24/09/2018 03:42:44 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usuarios_GetByEmail](@correo varchar(200))
 	AS
 	set nocount on
 	SELECT 
 	Id, nombres as 'Nombre', correo as 'Correo', perfil as 'Perfil', piso1 as 'Piso1'
 	, Piso14 as 'Piso14', 0 as 'Eliminar'
 	from [usuario] U
 	where correo = @correo
 	ORDER BY U.[nombres] ASC

GO


