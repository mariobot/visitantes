USE [VISITANTES]
GO

/****** Object:  StoredProcedure [dbo].[Visitante_Add]    Script Date: 24/09/2018 03:03:39 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Visitante_Add]
(
	@identificacion VARCHAR(50)
	, @nombres VARCHAR(200)
	, @empresa VARCHAR(200)
	, @fechavisita DATETIME
	, @userid VARCHAR(50)
	, @autorizador VARCHAR(200)
	, @correoautoriza VARCHAR(200)
	, @nombrecreacion VARCHAR(200)
)
AS
BEGIN
	BEGIN TRY
		insert into [visitante]
		([identificacion], [nombres] , [empresa] , [fechavisita] , [userid] , [autorizador] 
		, [correoautorizador] , [fechaingreso] , [piso1] , [fechaingreso2] 
		, [piso14] , [alertaenviada], [nombrecreacion] , [fechacreacion] , [nombremodificacion] 
		, [fechamodificacion]
		)
		VALUES (@identificacion,@nombres, @empresa, @fechavisita, @userid, @autorizador
		, @correoautoriza, NULL, 0, NULL, 0, 0, @nombrecreacion, GETDATE(),NULL,NULL)

		RETURN SCOPE_IDENTITY();	
	END TRY
	BEGIN CATCH	
		INSERT INTO tmp_err_procedures
		VALUES (ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), GETDATE())
		RETURN -1;
	END CATCH
END

GO


