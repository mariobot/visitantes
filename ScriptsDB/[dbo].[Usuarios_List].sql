USE [VISITANTES]
GO

/****** Object:  StoredProcedure [dbo].[Usuarios_List]    Script Date: 24/09/2018 03:02:00 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usuarios_List]
 	AS
 	set nocount on
 	SELECT 
 	Id, nombres as 'Nombre', correo as 'Correo', perfil as 'Perfil', piso1 as 'Piso1'
 	, Piso14 as 'Piso14', convert(bit,0) as 'Eliminar'
 	from [usuario] U
 	ORDER BY U.[nombres] ASC

GO


