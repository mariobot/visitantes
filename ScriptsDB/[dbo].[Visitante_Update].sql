USE [VISITANTES]
GO

/****** Object:  StoredProcedure [dbo].[Visitante_Update]    Script Date: 24/09/2018 03:06:51 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Visitante_Update]
(
	@Id VARCHAR(200)
	, @Piso1 bit
	, @Piso14 bit
	, @nombremodificacion VARCHAR(200)
)
AS
BEGIN
	BEGIN TRY
		IF(@Piso1 = 1 and @Piso14 = 1)
		begin
			UPDATE [visitante] 
			SET piso1 = @Piso1
			, piso14 = @Piso14
			, nombremodificacion = @nombremodificacion
			, fechaingreso2 = GETDATE()
			, fechamodificacion = GETDATE()
			WHERE ID = @Id
		end
		else IF(@Piso1 = 1 and @Piso14 = 0)
		begin
			UPDATE [visitante] 
			SET piso1 = @Piso1
			, piso14 = @Piso14
			, nombremodificacion = @nombremodificacion
			, fechaingreso = GETDATE()
			, fechamodificacion = GETDATE()
			WHERE ID = @Id
		end
		RETURN 1;		
	END TRY
	BEGIN CATCH	
		INSERT INTO tmp_err_procedures
		VALUES (ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), GETDATE())
		RETURN -1;
	END CATCH
END



GO


