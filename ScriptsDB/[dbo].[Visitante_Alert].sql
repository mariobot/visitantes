USE [VISITANTES]
GO

/****** Object:  StoredProcedure [dbo].[Visitante_Alert]    Script Date: 24/09/2018 03:04:36 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Visitante_Alert]
(
	@ModificadoPor VARCHAR(200)
	, @Id VARCHAR(200)
)
AS
BEGIN
	BEGIN TRY
		UPDATE visitante SET [alertaenviada] = 1
		, nombremodificacion = @ModificadoPor
		, fechamodificacion = GETDATE() 
		WHERE Id = @Id
		RETURN 1;
	END TRY
	BEGIN CATCH	
		INSERT INTO tmp_err_procedures
		VALUES (ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), GETDATE())
		RETURN -1;
	END CATCH
END

GO


