USE [VISITANTES]
GO

/****** Object:  StoredProcedure [dbo].[Visitantes_Informe]    Script Date: 24/09/2018 03:08:22 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Visitantes_Informe]
(@Identificacion varchar(50), @FechaDesde DATETIME, @FechaHasta DATETIME, @userid varchar(50))
 	AS
 	DECLARE @SelectSQL VARCHAR(2000);
 	DECLARE @JoinSQL VARCHAR(2000);

 	SET NOCOUNT ON;
 	SET DATEFORMAT dmy;
 	
 	/*
 	SET @SelectSQL = 'SELECT Identificacion AS ''Identificación'', nombres AS Nombre, Empresa, FechaIngreso AS ''Fecha Ingreso'', Autorizador, convert(bit,Piso1) AS ''Piso 1'', convert(bit,Piso14) AS ''Piso 14''
	from Visitante V WHERE 1 = 1';
	*/
	
	SET @SelectSQL = 'SELECT Identificacion AS ''Identificación'', nombres AS Nombre, Empresa, FechaIngreso AS ''Fecha Ingreso'', Autorizador, 
	(select case convert(bit,Piso1)  when 1 then ''[X]'' else '''' end) AS ''Piso 1'', 
	(select case convert(bit,Piso14) when 1 then ''[X]'' else '''' end) AS ''Piso 14''
	from Visitante V WHERE 1 = 1';
	
	
	SET @JoinSQL = '';
	
	IF(@Identificacion IS NOT NULL)
	BEGIN
		SET @JoinSQL = ' AND V.identificacion = ''' + @Identificacion + '''';
		IF(@FechaDesde IS NOT NULL AND @FechaHasta IS NOT NULL)
		BEGIN
			SET @JoinSQL = @JoinSQL + ' AND V.fechavisita BETWEEN CONVERT(DATETIME, ''' + CONVERT(VARCHAR(20),@FechaDesde, 120) + ''', 101) AND CONVERT(DATETIME, ''' + CONVERT(VARCHAR(20),@FechaHasta, 120) + ''',101)';
		END 				
	END
	ELSE IF(@Identificacion IS NULL)
	BEGIN
		IF(@FechaDesde IS NOT NULL AND @FechaHasta IS NOT NULL)
		BEGIN
			SET @JoinSQL = @JoinSQL + ' AND V.fechavisita BETWEEN CONVERT(DATETIME, ''' + CONVERT(VARCHAR(20),@FechaDesde, 120) + ''', 101) AND CONVERT(DATETIME, ''' + CONVERT(VARCHAR(20),@FechaHasta, 120) + ''',101)';
		END
	END
	
	--Si no hay documento intervalo de Fechas Consulta las visitas con pre registro de la Fecha Actual desde las 00:00:00 hasta 23:59:59
	IF(LEN(@JoinSQL) <= 0)
	BEGIN
		SET @JoinSQL = ' AND V.fechavisita BETWEEN dateadd(DAY, datediff(DAY, 0, getdate()),0)
		AND DATEADD(SECOND, -1 , dateadd(DAY, datediff(DAY, 0, getdate()+1),0))';
	END
	
	IF(@userid IS NOT NULL)	--LA CONSULTA VIENE DE FUNCIONARIO Y SÓLO DEBE VER LAS PREREGISTROS DE ÉL MISMO
	BEGIN
		SET @JoinSQL = @JoinSQL + ' AND V.userid = ''' + @userid + '''';
	END
	
	SET @JoinSQL = @JoinSQL + ' ORDER BY V.fechavisita, V.Nombres';
 	PRINT @SelectSQL + @JoinSQL;
 	--PRINT @SelectSQL;
 	--PRINT @JoinSQL;
 	EXECUTE (@SelectSQL + @JoinSQL);
	
	/*
	SELECT * FROM VISITANTE
	WHERE fechavisita BETWEEN dateadd(DAY, datediff(DAY, 0, getdate()),0)
	AND DATEADD(SECOND, -1 , dateadd(DAY, datediff(DAY, 0, getdate()+1),0))
	2018-03-23 00:00:00.000
	
	SELECT YEAR(GETDATE()) + '-' + MONTH(GETDATE())
	
	SELECT DATEADD (HOUR,-5, GETDATE())
	
	SELECT DAY(GETDATE())
	
	select dateadd(DAY, datediff(DAY, 0, getdate()),0)
	select dateadd(DAY, datediff(DAY, 0, getdate()+1),0)
	
	select  DATEADD(SECOND, -1 ,
	 dateadd(DAY, datediff(DAY, 0, getdate()+1),0)
	)
	SELECT getdate() +1
	*/


GO


