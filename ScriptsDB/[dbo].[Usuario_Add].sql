USE [VISITANTES]
GO

/****** Object:  StoredProcedure [dbo].[Usuario_Add]    Script Date: 24/09/2018 02:57:15 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usuario_Add]
(
	@nombres VARCHAR(200)
	, @correo VARCHAR(200)
	, @perfil VARCHAR(50)
	, @piso1 BIT
	, @piso14 BIT
	, @nombrecreacion VARCHAR(200)
)
AS
BEGIN
	BEGIN TRY
		insert into [usuario]
		([nombres], [correo], [perfil], [piso1], [piso14], 
		[nombrecreacion], [fechacreacion], [nombremodificacion], [fechamodificacion])
		values
		(@nombres, @correo, @perfil, @piso1, @piso14, 
		@nombrecreacion, GETDATE(),NULL, NULL)
		
		RETURN SCOPE_IDENTITY();	
	END TRY
	BEGIN CATCH	
		INSERT INTO tmp_err_procedures
		VALUES (ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), GETDATE())
		RETURN -1;
	END CATCH
END

GO


