USE [VISITANTES]
GO

/****** Object:  Table [dbo].[visitante]    Script Date: 24/09/2018 02:50:05 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[visitante](
	[id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[identificacion] [varchar](50) NOT NULL,
	[nombres] [varchar](200) NOT NULL,
	[empresa] [varchar](200) NOT NULL,
	[fechavisita] [datetime] NOT NULL,
	[userid] [varchar](50) NOT NULL,
	[autorizador] [varchar](200) NOT NULL,
	[correoautorizador] [varchar](200) NOT NULL,
	[fechaingreso] [datetime] NULL,
	[piso1] [bit] NULL,
	[fechaingreso2] [datetime] NULL,
	[piso14] [bit] NULL,
	[alertaenviada] [bit] NULL,
	[nombrecreacion] [varchar](200) NOT NULL,
	[fechacreacion] [datetime] NOT NULL,
	[nombremodificacion] [varchar](200) NULL,
	[fechamodificacion] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[visitante] ADD  DEFAULT (newsequentialid()) FOR [id]
GO

ALTER TABLE [dbo].[visitante] ADD  DEFAULT ((0)) FOR [piso1]
GO

ALTER TABLE [dbo].[visitante] ADD  DEFAULT ((0)) FOR [piso14]
GO

ALTER TABLE [dbo].[visitante] ADD  DEFAULT ((0)) FOR [alertaenviada]
GO

ALTER TABLE [dbo].[visitante] ADD  DEFAULT (getdate()) FOR [fechacreacion]
GO


