USE [VISITANTES]
GO

/****** Object:  Table [dbo].[tmp_err_procedures]    Script Date: 24/09/2018 02:55:49 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tmp_err_procedures](
	[ErrorNumber] [varchar](200) NULL,
	[ErrorSeverity] [varchar](200) NULL,
	[ErrorState] [varchar](200) NULL,
	[ErrorProcedure] [varchar](200) NULL,
	[ErrorLine] [varchar](200) NULL,
	[ErrorMessage] [varchar](max) NULL,
	[creationdate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tmp_err_procedures] ADD  DEFAULT (getdate()) FOR [creationdate]
GO


