USE [VISITANTES]
GO

/****** Object:  Table [dbo].[usuario]    Script Date: 24/09/2018 02:52:58 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[usuario](
	[id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[nombres] [varchar](200) NOT NULL,
	[correo] [varchar](200) NOT NULL,
	[perfil] [varchar](50) NOT NULL,
	[piso1] [bit] NULL,
	[piso14] [bit] NULL,
	[nombrecreacion] [varchar](200) NOT NULL,
	[fechacreacion] [datetime] NOT NULL,
	[nombremodificacion] [varchar](200) NULL,
	[fechamodificacion] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[correo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[usuario] ADD  DEFAULT (newsequentialid()) FOR [id]
GO

ALTER TABLE [dbo].[usuario] ADD  DEFAULT ((0)) FOR [piso1]
GO

ALTER TABLE [dbo].[usuario] ADD  DEFAULT ((0)) FOR [piso14]
GO

ALTER TABLE [dbo].[usuario] ADD  DEFAULT (getdate()) FOR [fechacreacion]
GO


