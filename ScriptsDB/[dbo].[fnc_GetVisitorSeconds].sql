USE [VISITANTES]
GO

/****** Object:  UserDefinedFunction [dbo].[fnc_GetVisitorSeconds]    Script Date: 24/09/2018 05:38:25 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnc_GetVisitorSeconds]
(
	-- Add the parameters for the function here
	@Id VARCHAR(200)
)
RETURNS NUMERIC(38,4)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @TiempoCrono NUMERIC(38,4)

	SET @TiempoCrono = -1;
	-- Add the T-SQL statements to compute the return value here
	SELECT @TiempoCrono = CONVERT(NUMERIC(38,4), (DATEDIFF(SECOND,FECHAINGRESO, ISNULL(FECHAINGRESO2,GETDATE()) ) ))/60
	FROM VISITANTE WHERE id = @Id

	-- Return the result of the function
	RETURN @TiempoCrono

END

GO


