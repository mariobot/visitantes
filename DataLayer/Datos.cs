﻿using DI.DataHelper.DataHelper;
using System;
using System.Configuration;
using System.Data;

namespace DataLayer
{
    public class Datos : DI.DataHelper.DataHelper.DataLayer
    {
        string cadena;
        public Datos() 
        {
            try
            {
                cadena = ConfigurationManager.ConnectionStrings["strConn"].ToString();
                
                helper = Factory.CreateSqlHelper(cadena);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable GetUsuarioByEmail(string strCorreo)
        {
            try
            {
                return helper.ExecuteDataTable("Usuarios_GetByEmail", CommandType.StoredProcedure, "@correo", strCorreo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListUsuarios()
        {
            try
            {
                return helper.ExecuteDataTable("Usuarios_List", CommandType.StoredProcedure, null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetVisitantes(string strIdentificacion, DateTime dtFechaIni, DateTime dtFechaFin, string strUserId)
        {
            try
            {
                return helper.ExecuteDataTable("Visitantes_Search", CommandType.StoredProcedure, "@Identificacion", strIdentificacion,
                    "@FechaDesde", dtFechaIni, "@FechaHasta", dtFechaFin, "@userid", strUserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable GetVisitantesInforme(string strIdentificacion, DateTime dtFechaIni, DateTime dtFechaFin, string strUserId)
        {
            try
            {
                return helper.ExecuteDataTable("Visitantes_Informe", CommandType.StoredProcedure, "@Identificacion", strIdentificacion,
                    "@FechaDesde", dtFechaIni, "@FechaHasta", dtFechaFin, "@userid", strUserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string AddUsuario(string strNombres, string strCorreo, string strPerfil, bool bitPiso1, bool bitPiso14, string strCreadoPor)
        {
            try
            {
                return Convert.ToString(helper.ExecuteReturn("Usuario_Add", CommandType.StoredProcedure, "@nombres", strNombres, "@correo", strCorreo
                    , "@perfil", strPerfil, "@piso1", bitPiso1, "@piso14", bitPiso14, "@nombrecreacion", strCreadoPor));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DeleteUsuario(string strId)
        {
            try
            {
                return Convert.ToString(helper.ExecuteReturn("Usuario_Delete", CommandType.StoredProcedure, "@Id", strId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string AddVisitante(string strIdentificacion, string strNombres, string strEmpresa, DateTime dtFechaVisita, string strUserId, string strAutorizador, string strCorreoAutoriza, string strCreadoPor)
        {
            try
            {
                return Convert.ToString(helper.ExecuteReturn("Visitante_Add", CommandType.StoredProcedure,
                    "@identificacion", strIdentificacion, 
                    "@nombres", strNombres, "@empresa", strEmpresa
                    , "@fechavisita", dtFechaVisita, "@userid", strUserId, "@autorizador", strAutorizador
                    , "@correoautoriza", strCorreoAutoriza, "@nombrecreacion", strCreadoPor));
                            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string UpdateVisitante(string strId, bool btPiso1, bool btPiso14, string strModificador)
        {
            try
            {
                return Convert.ToString(helper.ExecuteReturn("Visitante_Update", CommandType.StoredProcedure, "@Id", strId,
                    "@Piso1", btPiso1, "@Piso14", btPiso14, "@nombremodificacion", strModificador));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string DeleteVisitante(string strId)
        {
            try
            {
                return Convert.ToString(helper.ExecuteReturn("Visitante_Delete", CommandType.StoredProcedure, "@Id", strId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
