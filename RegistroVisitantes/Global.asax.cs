﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

using System.Web.Optimization;
using AjaxControlToolkit;
using System.IO;

namespace RegistroVisitantes
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            //BundleTable.Bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
            //"~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
            //"~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
            //"~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
            //"~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));

            //BundleTable.EnableOptimizations = true;

            //var tempFolder = Server.MapPath(ToolkitConfig.TempFolder);
            //foreach (var dir in Directory.EnumerateDirectories(tempFolder))
            //{
            //    try
            //    {
            //        //Directory.Delete(dir, true);
            //    }
            //    catch { }
            //}
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}