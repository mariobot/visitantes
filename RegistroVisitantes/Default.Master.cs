﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RegistroVisitantes.App_Code;
namespace RegistroVisitantes
{
    public partial class Default_master : System.Web.UI.MasterPage
    {
        

        protected void Page_Load(object sender, EventArgs e)        
        {
            if (!IsPostBack)
            {
                lblLoginName.Text = Session["username"].ToString().ToUpper();
                CenterName.Text = Session["userRol"].ToString().ToUpper();                
                /*POR DEFECTO DEJO OCULTOS AMBOS BOTONES, PUES DESDE EL LOGIN DECIDO 
                 A DONDE REDIRECCIONARLO USUARIO.ASPX O VISITANTE.ASPX POR EL ROL*/
                LevelAccess();
            }
        }

        public void LevelAccess()
        {
            try
            {  
                if (Session["userRol"].ToString().Equals(BaseView.Perfil.Administrador.ToString()))
                {
                    btnVisitantes.Visible = true;
                    btnUsuarios.Visible = true;
                }
                else if (Session["userRol"].ToString().Equals(BaseView.Perfil.Funcionario.ToString()))
                {
                    btnVisitantes.Visible = false;
                    btnUsuarios.Visible = false;
                }
                else if (Session["userRol"].ToString().Equals(BaseView.Perfil.Recepcionista.ToString()))
                {
                    btnVisitantes.Visible = false;
                    btnUsuarios.Visible = false;
                }
                //if (Session["userSamAccountName"].ToString().Equals("vjsalinas"))
                //{
                //    btnVisitantes.Visible = true;
                //    btnUsuarios.Visible = true;
                //}
            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
            }
        }

        protected void cerrarCesion_btn_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("SessionEnded.aspx");
        }

        protected void btnUsuarios_Click(object sender, EventArgs e)
        {
            Response.Redirect("Usuario.aspx");
        }

        protected void btnVisitantes_Click(object sender, EventArgs e)
        {
            Response.Redirect("Visitante.aspx");
        }


        protected void btnAceptar1_Click(object sender, EventArgs e)
        {
            if (lblMessage1.Text.Equals("Visitante registrado correctamente"))
            {
                Response.Redirect("visitante.aspx?ban=nuevo");
            }
        }


    }
}