﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="RegistroVisitantes.Login" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>APLICACION REGISTRO VISITANTES LOGIN : Login</title>
<link href="estilosApp/login.css" rel="stylesheet" type="text/css" />
<link href="Estilos/cargadores.css" rel="stylesheet" type="text/css" />

</head>
<body>
<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(IniciaRequest);
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

    var _Instance = Sys.WebForms.PageRequestManager.getInstance();

    function IniciaRequest(sender, arg) {
        // INICIALIZACION DEL REQUEST
        if (_Instance.get_isInAsyncPostBack()) {
            window.alert("Existe un proceso en marcha. Espere");
            arg.set_cancel(true);
        }
    }

    function BeginRequestHandler(sender, args) {
        $find("mpe").show();
    }

    function EndRequestHandler(sender, args) {
        $find("mpe").hide();
    }

    function checkEnter(e) {

        var characterCode

        if (e && e.which) {
            e = e
            characterCode = e.which
        }
        else {
            e = event
            characterCode = e.keyCode
        }

        if (characterCode == 13) { //if generated character code is equal to ascii 13 (if enter key)
            __doPostBack('btnIngresar', 'btnIngresar_Click')
            return false
        }
        else {
            return true
        }

    }
</script>
<asp:HyperLink ID="lnkModal" runat="server"></asp:HyperLink>
        <table class="modalPopup" style="display: none" id="pnlPopup" runat="server" cellspacing="0"
            cellpadding="0" width="25%" align="center" border="0">
            <tbody>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        Cargando Datos. Por Favor Espere...
                        <br />
                        <asp:Image ID="Image122" runat="server" ImageUrl="~/Images/cargador.gif"></asp:Image>
                    </td>
                </tr>
            </tbody>
        </table>
        <cc1:ModalPopupExtender ID="mpe" runat="server" BehaviorID="mpe" PopupControlID="pnlPopup"
            BackgroundCssClass="modalBackground" DropShadow="false" TargetControlID="lnkModal" />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
  <ContentTemplate>
     <table class="modalPopup" id="pnlMsgBoxInfo" runat="server" style="display:none;">
      <tr>
        <td valign="top">
             <div class="modalPopupContent">
                <asp:Image ID="ImgMsgBox" runat="server" />
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:Button ID="btnMsgAceptar" runat="server" Text="" CausesValidation="False" />
            </div>
        </td>
      </tr>
    </table>
    <cc1:ModalPopupExtender ID="mpe_MsgBox" runat="server" DropShadow="True" OkControlID="btnMsgAceptar"
      PopupControlID="pnlMsgBoxInfo" TargetControlID="lnkMsgBox" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <asp:LinkButton ID="LinkButton1" runat="server" Enabled="False"></asp:LinkButton>
    <asp:LinkButton ID="lnkMsgBox" runat="server" Enabled="False"></asp:LinkButton>
    <table border="0" class="modalPopup" id="pnlConfirmBox" runat="server" width="323"
        style="display: none;">
        <tr>
            <td valign="top">
                 <div class="modalPopupContent">
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/MsgBox/msgPregunta.jpg" />
                    <asp:Label ID="lblConfirmText" runat="server"></asp:Label>
                 </div>
                 <div align="center">
                    <asp:Button ID="btnAceptarConfirmBox" runat="server" Text="Aceptar" CausesValidation="False" OnClick="ButtonClickedHandler" />
                    <asp:Button ID="btnCancelConfirmBox" runat="server" Text="Cancelar" CausesValidation="False" />
                 </div>   
            </td>
        </tr>
    </table>
    <cc1:ModalPopupExtender ID="mpe_ConfirmBox" runat="server" DropShadow="True" PopupControlID="pnlConfirmBox" CancelControlID="btnCancelConfirmBox" TargetControlID="lnkBtnConfirm" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <asp:LinkButton ID="lnkBtnConfirm" runat="server" Enabled="False"></asp:LinkButton>
    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="login100">
      <tr>
        <td scope="col" class="top">&nbsp;
        </td>
      </tr>
      <tr>
        <td scope="col" class="middleTable"><div class="containerLogin" align="center">
      <div class="top">&nbsp;</div>
      <div class="middle">
        <div class="logotipo"></div>
        <div class="tituloApp">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="initTApp"></td>
              <td class="midTApp">
                  Bienvenidos <asp:Label ID="lblProyecto" runat="server" Text="Aplicacion Registros Visitantes"></asp:Label>          </td>
              <td class="endTApp"></td>
            </tr>
          </table>
        </div>
        <div class="logotipoApp"></div>
        <div class="formLogin">
          <table border="0">
            <tr>
              <td>
                <asp:Label ID="lblUserName" runat="server" Text="Usuario"></asp:Label></td>
              <td>
                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtUserName" runat="server" ControlToValidate="txtUserName"
                  Display="None" ErrorMessage="Campo requerido" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <cc1:ValidatorCalloutExtender ID="vcerfvtxtUserName" runat="server" TargetControlID="rfvtxtUserName">
                </cc1:ValidatorCalloutExtender>
                <asp:RegularExpressionValidator ID="revtxtUserName" runat="server" ControlToValidate="txtUserName"
                  Display="None" ErrorMessage="Datos incorrectos" SetFocusOnError="True" ValidationExpression="^[a-zA-Z0-9\sáéíóúñÑÁÉÍÓÚ,.]+$"></asp:RegularExpressionValidator>
                <cc1:ValidatorCalloutExtender ID="vcerevtxtUserName" runat="server" TargetControlID="revtxtUserName">
                </cc1:ValidatorCalloutExtender>
              </td>
            </tr>
            <tr>
              <td>
                <asp:Label ID="lblPassword" runat="server" Text="Contraseña"></asp:Label></td>
              <td>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" onkeypress="javascript:return checkEnter(event)" Visible="False"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ID="rfvtxtPassword" runat="server" ControlToValidate="txtPassword"
                  Display="None" ErrorMessage="Campo requerido" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <cc1:ValidatorCalloutExtender ID="vcerfvtxtPassword" runat="server" TargetControlID="rfvtxtPassword">
                </cc1:ValidatorCalloutExtender>--%>
              </td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: center">
                <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" Font-Bold="True" ForeColor="#833177" /></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="bottom">&nbsp;</div>
    </div></td>
      </tr>
      <tr>
        <td scope="col" class="bottom">&nbsp;</td>
      </tr>
    </table>
    </ContentTemplate>
</asp:UpdatePanel>
</form>
</body>
</html>