using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace RegistroVisitantes
{
    public partial class SessionEnded : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            lblProyecto.Text = ConfigurationManager.AppSettings["NombreProyecto"];
            string message = "";
            string type = (Request.QueryString["p1"] != null ? Request.QueryString["p1"] : "");
            switch (type)
            {
                case "1"://finalizacion remota
                    message = "Su sesion ha sido terminada remotamente. {Fecha} {Hora}"; break;
                case "2":
                    message = "El Sistema de Información ha finalizado la sesión exitosamente. {Fecha} {Hora} "; break;
                default://finalizacion por timeout
                    message = ((Hashtable)Application["htAppParams"])[ParamNames.EndSessionMessage].ToString(); break;
               
            }
            lblMensajeFinSesion.Text = message.Replace("{Fecha}", DateTime.Today.ToShortDateString()).Replace("{Hora}", DateTime.Now.ToShortTimeString().Replace(".", "").ToUpper());
          //  SecurityService
            */
            Session.Abandon();
        }

        protected void lnkRegresar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }
    }
}