﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Usuario.aspx.cs" Inherits="RegistroVisitantes.Usuario" enableEventValidation="true" %>
<%@ MasterType VirtualPath="~/Default.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="Menu" runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">

    <%--  <form id="form1" runat="server">--%>
    <%--<div class="contentForms">--%>
        <asp:MultiView ID="vwMain" runat="server">
            <%--<table>
                <tr><td align="center"><div></div>
                    <h1><font color="#833177"><b>REGISTRO DE VISITANTES</b></font></h1>
                    <h5>En este portal debes registrar tu(s) visitante(s) para que pueda(n) acceder a las oficinas de Frontera Energy.<br />
                    Recuerda ingresar los datos completos.  </h5>
                    </td>
                </tr>
            </table>--%>
            <asp:View ID="vwConsultar" runat="server">
                <asp:UpdatePanel id="pnlupdConsultar" runat="server">
                <ContentTemplate>
                <asp:Panel ID="pnlConsultar" runat="server" GroupingText="Consultar Perfiles" CssClass="column_4" Font-Bold="True" ForeColor="#833177">                
                    <table style="width:75%;">
                        <tr>
                            <td width="24%"></td>
                            <td width="24%"></td>
                            <td width="24%"></td>
                            <td width="24%"></td>
                            <td width="24%"></td>
                            <td>
                                <asp:Button ID="btnNuevo" runat="server" ForeColor="#833177" OnClick="btnNuevo_Click" Text="Nuevo" Width="88px" Font-Bold="True" />
                            </td>
                        </tr>
                    </table>
                    
                    <br />
                    <asp:Panel ID="pnlResultado" runat="server" Width="99%">
                        <asp:GridView ID="grvUsuarios" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="Id,Nombre,Correo,Perfil,Piso1,Piso14,Eliminar" Width="75%" Font-Bold="False" OnDataBound="grvUsuarios_DataBound">
                            <AlternatingRowStyle ForeColor="Black" />
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Correo" HeaderText="Correo" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Perfil" HeaderText="Perfíl" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Piso 1" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <div id="Div0" runat="server" align="center">
                                        <asp:CheckBox ID="chkPiso1" runat="server" Checked='<%# Bind("Piso1") %>' readonly="true" Enabled="false"></asp:CheckBox>
                                        </div>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Piso 14" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <div id="Div1" runat="server" align="center">
                                        <asp:CheckBox ID="chkPiso14" runat="server" Checked='<%# Bind("Piso14") %>' readonly="true" Enabled="false"></asp:CheckBox>
                                        </div>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Eliminar" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <div id="Div2" runat="server" align="center">
                                        <asp:CheckBox ID="chkEliminar" runat="server" Checked='<%# Bind("Eliminar") %>' Enabled ="true"></asp:CheckBox>
                                        </div>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle BackColor="#833177" ForeColor="White" />
                            <RowStyle ForeColor="Black" />
                        </asp:GridView>
                        <br />

                        <table style="width:75%;">
                            <tr>
                                <td width="24%"></td>
                                <td width="24%"></td>
                                <td width="24%"></td>
                                <td width="24%"></td>
                                <td width="24%"></td>
                                <td> 
                                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" OnClick="btnEliminar_Click" ForeColor="#833177" Width="100px" Font-Bold="True" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                 </asp:Panel>
                </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnNuevo"  />
                        <asp:AsyncPostBackTrigger ControlID="btnEliminar" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

            </asp:View>
            <asp:View ID="vwGuardar" runat="server">
                <asp:UpdatePanel id="pnlupdGuardar" runat="server">
                <ContentTemplate>
                <asp:Panel ID="pnlRegistrar" runat="server" GroupingText="Registrar Perfiles" Font-Bold="True" ForeColor="#833177"  Width="45%">
                    <table style="width: 511px">
                        <tr>
                            <td>
                                <asp:Label ID="lblNombre" runat="server" Text="Nombre Completo *"></asp:Label><br />
                                <asp:TextBox ID="txtNombre" runat="server" MaxLength="200" Width="300px"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="vldNombre" runat="server" ControlToValidate="txtNombre" ErrorMessage="Ingresar Nombres" ForeColor="Red" ValidationGroup="Grabar" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator id="RegularExpressionValidatorFromNombre"
                                        ControlToValidate="txtNombre"
                                        Display="Dynamic"
                                        ValidationGroup="Grabar"
                                        ForeColor="Red"
                                        ErrorMessage="El formato del nombre no es válido!"
                                        ValidationExpression="^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$"
                                        runat="server"/> 
                                <br /><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCorreo" runat="server" Text="Correo *"></asp:Label><br />
                                <asp:TextBox ID="txtCorreo" runat="server" MaxLength="200" Width="300px"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="vldCorreo" runat="server" ErrorMessage="Ingresar Correo" 
                                ForeColor="Red" ValidationGroup="Grabar" ControlToValidate="txtCorreo" Display="Dynamic">
                                </asp:RequiredFieldValidator><br />
                                <asp:RegularExpressionValidator id="RegularExpressionValidatorFromEmail"
                                ControlToValidate="txtCorreo"
                                Display="Dynamic"
                                ValidationGroup="Grabar"
                                ForeColor="Red"
                                ErrorMessage="El formato del correo electrónico no es válido!"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                runat="server"/> 
                                <br /><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td style="width: 297px">
                                            <asp:Label ID="lblPerfil" runat="server" Text="Perfíl *"></asp:Label><br />
                                            <asp:DropDownList ID="ddlPerfil" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPerfil_SelectedIndexChanged">
                                                <asp:ListItem Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1">Administrador</asp:ListItem>
                                                <asp:ListItem Value="2">Recepcionista</asp:ListItem>
                                            </asp:DropDownList><br />
                                            <asp:RequiredFieldValidator ID="vldPerfil" runat="server" 
                                                ControlToValidate="ddlPerfil" Display="Dynamic" ValidationGroup="Grabar" ErrorMessage="Seleccionar Perfíl" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPiso" runat="server" Text="Piso"></asp:Label><br />
                                            <asp:CheckBoxList ID="chkLstPiso" runat="server" RepeatDirection="Horizontal" Width="185px">
                                                <asp:ListItem Value="1">Piso 1</asp:ListItem>
                                                <asp:ListItem Value="2">Piso 14</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table style="width: 511px">
                            <tr>
                                <td class="auto-style9" style="width: 403px" />
                                <td align="center">
                                    <asp:Button ID="btnGrabar" runat="server" OnClick="btnGrabar_Click" Text="Grabar" ValidationGroup="Grabar" ForeColor="#833177" Width="100px" Font-Bold="True"  />
                                </td>
                                <td align="center">
                                    <asp:Button ID="btnConsultar" runat="server" OnClick="btnConsultar_Click" Text="Regresar" ForeColor="#833177" Width="100px" Font-Bold="True" />
                                </td>
                            </tr>
                        </table>
                </asp:Panel>
                </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnGrabar" EventName="Click" />
                        <asp:PostBackTrigger ControlID="btnConsultar"  />
                    </Triggers>
                </asp:UpdatePanel>
             </asp:View>
        </asp:MultiView>

<%--</form>--%>
        <%--</div>--%>
</asp:Content>