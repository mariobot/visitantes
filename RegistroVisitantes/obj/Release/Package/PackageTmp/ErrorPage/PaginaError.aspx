﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaError.aspx.cs" Inherits="RegistroVisitantes.PaginaError"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Error de Ingreso a la aplicación</title>
    <!--JULINAS 25.01.2010, ENRUTAR CORRECTAMETNE LA HOJA DE ESTILOS-->
    <%--<link href="../App_Themes/SkinFile/stylesheet.css" rel="stylesheet" type="text/css" />--%>
    <!--JULINAS 25.01.2010, ENRUTAR CORRECTAMETNE LA HOJA DE ESTILOS-->
    <script language="javascript" type="text/javascript">
        function NoAtras() {
            history.go(1)
        }
    </script>
</head>
<body class="color_login" onload="NoAtras()">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="login100">
      <tr>
        <td scope="col" class="top">&nbsp;
        </td>
      </tr>
      <tr>
        <td scope="col" class="middleTable"><div class="containerLogin" align="center">
      <div class="top">&nbsp;</div>
      <div class="middle">
        <div class="logotipo"></div>
        <div class="tituloApp login">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="initTApp" style="height: 16px"></td>
              <td class="midTApp" style="height: 16px">
                <span class="salida" style="font-size:16px !important;">&nbsp;&nbsp;ERROR &nbsp;&nbsp;</span> 
              </td>
              <td class="endTApp" style="height: 16px"></td>
            </tr>
          </table>
        </div>
        <div class="logotipoApp"></div>
        <div class="formLogin">
        <form id="form1" runat="server">
            
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="height: 65px" valign="top">
                    <div class="info">
                        En este momento no es posible ingresar a la aplicación. <br/> El horario permitido de conexión es
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="False" Font-Size="Small" Text="" />
                    </div>
                  </td>
                </tr>
                <tr>
                    <td>
                         <span class="salida" style="font-size:11px !important;">
                            Cualquier duda o comentario, favor remitirse al administrador de la aplicación
                        </span>
                            
                    </td>
                </tr>
            </table>
           </form>

        </div>
      </div>
      <div class="bottom">&nbsp;</div>
    </div></td>
      </tr>
      <tr>
        <td scope="col" class="bottom">&nbsp;</td>
      </tr>
    </table>
   
</body>
</html>
