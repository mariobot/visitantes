﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Menu.ascx" Inherits="UserControls_Menu" %>
<asp:Menu ID="Menu" runat="server" OnLoad="Menu_Load" DataSourceID="XmlDSMenu" 
    Orientation="Horizontal">
  <DataBindings>
    <asp:MenuItemBinding DataMember="MenuItem" NavigateUrlField="NavigateUrl" TextField="Text"
      ToolTipField="ToolTip" />
  </DataBindings>
</asp:Menu>
<asp:XmlDataSource ID="XmlDSMenu" runat="server" TransformFile="~/UserControls/TransformXSLT.xsl"
  XPath="MenuItems/MenuItem" EnableCaching="false"></asp:XmlDataSource>