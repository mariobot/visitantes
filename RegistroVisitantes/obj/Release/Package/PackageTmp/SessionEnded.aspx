<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SessionEnded.aspx.cs" Inherits="RegistroVisitantes.SessionEnded" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Fin de sesi�n</title>

<link href="EstilosApp/login.css" rel="stylesheet" type="text/css" />
<link href="Estilos/cargadores.css" rel="stylesheet" type="text/css" />
<link href="EstilosApp/estilos_estructura.css" rel="stylesheet" type="text/css" />

<script language ="javascript" type="text/javascript">
function NoBack()
{
location.href="login.aspx"; 
}

</script>
</head>
<body onunload="NoBack()">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="login100">
      <tr>
        <td scope="col" class="top">&nbsp;
        </td>
      </tr>
      <tr>
        <td scope="col" class="middleTable"><div class="containerLogin" align="center">
      <div class="top">&nbsp;</div>
      <div class="middle">
        <div class="logotipo"></div>
        <div class="tituloApp">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="initTApp" style="height: 16px"></td>
              <td  style="height: 16px"><span class="salida" style="font-size:12px !important;">&nbsp;&nbsp;Fin sesi�n &nbsp;&nbsp;</span> 
                <asp:Label ID="lblProyecto" runat="server" Text="Registro Visitantes"></asp:Label> </td>
              <td class="endTApp" style="height: 16px"></td>
            </tr>
          </table>
        </div>
        <div class="logotipoApp"></div>
        <div class="formLogin">
        <form id="form1" runat="server">
            
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="height: 50px" valign="top">
                    <div class="info">
                        <asp:Label ID="lblMensajeFinSesion" runat="server" 
                            Text="El Sistema de Informaci�n ha finalizado la sesi�n exitosamente" 
                            Font-Bold="True" Font-Size="X-Small" ></asp:Label></div>
                  </td>
                </tr>
                <tr>
                    <td>
                        <div style="clear:both;">
                            <asp:LinkButton SkinID="tipoBoton" ID="lnkRegresar" runat="server" OnClick="lnkRegresar_Click" Text="Reingresar al sistema"></asp:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
        </div>
      </div>
      <div class="bottom">&nbsp;</div>
    </div></td>
      </tr>
      <tr>
        <td scope="col" class="bottom">&nbsp;</td>
      </tr>
    </table>
</body>
</html>