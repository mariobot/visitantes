﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;


namespace RegistroVisitantes
{
    public partial class Visitante : App_Code.BaseView // System.Web.UI.Page
    {
        BusinessLayer.Negocio negocio = new BusinessLayer.Negocio();
        string strMensage = string.Empty;

        IFormatProvider culture = CultureInfo.GetCultureInfo("es-CO");

        private readonly static int VisitorMinutes = Int16.Parse(ConfigurationManager.AppSettings["VisitorMinutes"].ToString());

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ban"] != null)
                {
                    ClearControls();
                    Validar("Consultar");
                    vwMain.SetActiveView(vwGuardar);
                    LevelAccess();
                    return;
                }
                ViewState["dtVisitantes"] = null;
                //this.btnGrabar.Attributes.Add("onclick", "javascript:return confirm('¿Está seguro de almacenar la información diligenciada?')");
                this.btnGrabarIngreso.Attributes.Add("onclick", "javascript:return confirm('¿Está seguro de actualizar la información marcada?')");
                this.btnEliminar.Attributes.Add("onclick", "javascript:return confirm('¿Está seguro de eliminar el/los Visitantes(s) seleccionado(s)?')");                
                Validar("Consultar");

                if (Session["userRol"].ToString().Trim().Equals(Perfil.Recepcionista.ToString()))
                {
                    vwMain.SetActiveView(vwConsultar);
                    //POR DEFECTO INVOCAR CONSULTAR A LA PRIEMRA
                    Consultar();
                }
                else  //va directo a Nuevo Visitante si es administrador o Funcionario.
                {
                    vwMain.SetActiveView(vwGuardar);
                }               
                LevelAccess();
            }
        }


        public void LevelAccess()
        {
            try
            {
                //if (Session["userSamAccountName"].ToString().Equals("vjsalinas"))
                //    return;

                //SI EL ROL NO PERTENECE A LA PÁGINA SACARLO
                if (Session["userRol"].ToString().Trim().Equals(Perfil.Administrador.ToString()))
                {
                    /*
                    string strMensage = String.Format("el Rol '{0}' no Tiene Acceso a esta página", Session["userRol"].ToString().Trim());
                    //MessageBox(strMensage, this.Page);
                    mostrarMensaje(2, strMensage);
                    Response.Redirect("SessionEnded.aspx");
                    */
                    btnGrabarIngreso.Visible = false;
                    btnInforme.Visible = false;
                    btnNuevo.Visible = true;
                    btnEliminar.Visible = true;
                }
                //SI EL ROL NO PERTENECE A LA PÁGINA SACARLO


                if (Session["userRol"].ToString().Trim().Equals(Perfil.Funcionario.ToString()))
                {
                    btnGrabarIngreso.Visible = false;
                    btnInforme.Visible = false;
                    btnNuevo.Visible = true;
                    btnEliminar.Visible = true;

                    pnlFechas.Visible = true;
                    txtFechaIni.Enabled = true;
                    txtHoraIni.Enabled = true;
                    txtFechaFin.Enabled = true;
                    txtHoraFin.Enabled = true;
                }
                else if (Session["userRol"].ToString().Trim().Equals(Perfil.Recepcionista.ToString()))
                {
                    btnGrabarIngreso.Visible = true;
                    btnInforme.Visible = true;
                    btnNuevo.Visible = false;
                    btnEliminar.Visible = false;

                    pnlFechas.Visible = false;

                    txtFechaIni.Enabled = false;
                    txtFechaIni.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtHoraIni.Enabled = false;

                    txtFechaFin.Enabled = false;
                    txtFechaFin.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtHoraFin.Enabled = false;
                        
                }
            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }


        public void Consultar()
        {
            Validar("Consultar");
            DataTable dtVisitantes = new DataTable();
            DataTable dtVisitantesInforme = new DataTable();
            try
            {
                DateTime FechaIni;
                DateTime FechaFin;

                string strUserId = string.Empty;
                //if (Session["userRol"].Equals(Perfil.Funcionario.ToString()))
                //    strUserId = Session["userSamAccountName"].ToString();

                if (Session["userRol"].Equals(Perfil.Funcionario.ToString()) ||
                    Session["userRol"].Equals(Perfil.Administrador.ToString()))
                    strUserId = Session["userSamAccountName"].ToString();
                
                if (txtIdentificacion.Text.Trim().Length <= 0
                    && txtFechaIni.Text.Trim().Length <= 0
                    && txtFechaFin.Text.Trim().Length <= 0)
                {
                    dtVisitantes = negocio.GetVisitantes(string.Empty, DateTime.MinValue, DateTime.MinValue, strUserId);
                    dtVisitantesInforme = negocio.GetVisitantesInforme(string.Empty, DateTime.MinValue, DateTime.MinValue, strUserId);
                }
                else if (txtIdentificacion.Text.Trim().Length > 0
                    && txtFechaIni.Text.Trim().Length <= 0
                    && txtFechaFin.Text.Trim().Length <= 0)
                {
                    dtVisitantes = negocio.GetVisitantes(txtIdentificacion.Text.Trim(), DateTime.MinValue, DateTime.MinValue, strUserId);
                    dtVisitantesInforme = negocio.GetVisitantesInforme(txtIdentificacion.Text.Trim(), DateTime.MinValue, DateTime.MinValue, strUserId);
                }
                else if (txtIdentificacion.Text.Trim().Length <= 0
                    && txtFechaIni.Text.Trim().Length > 0
                    && txtFechaFin.Text.Trim().Length > 0)
                {
                    try
                    {
                        DateTime.TryParse(txtFechaIni.Text.Trim() + " " + txtHoraIni.Text.Trim(), out FechaIni);
                        FechaIni = Convert.ToDateTime(txtFechaIni.Text.Trim() + " " + txtHoraIni.Text.Trim(), culture);
                    }
                    catch (Exception ex)
                    {
                        txtFechaIni.Text = string.Empty;
                        txtHoraIni.Text = string.Empty;
                        txtFechaIni.Focus();
                        strMensage = "Error validando Fecha y Hora Inicial";
                        //MessageBox(strMensage, this.Page);
                        mostrarMensaje(2, strMensage);
                        return;
                    }
                    try
                    {
                        DateTime.TryParse(txtFechaFin.Text.Trim() + " " + txtHoraFin.Text.Trim(), out FechaFin);
                        FechaFin = Convert.ToDateTime(txtFechaFin.Text.Trim() + " " + txtHoraFin.Text.Trim(), culture);
                    }
                    catch (Exception ex)
                    {
                        txtFechaFin.Text = string.Empty;
                        txtHoraFin.Text = string.Empty;
                        txtFechaFin.Focus();
                        strMensage = "Error validando Fecha y Hora Final";
                        //MessageBox(strMensage, this.Page);
                        mostrarMensaje(2, strMensage);
                        return;
                    }
                    dtVisitantes = negocio.GetVisitantes(string.Empty, FechaIni, FechaFin, strUserId);
                    dtVisitantesInforme = negocio.GetVisitantesInforme(string.Empty, FechaIni, FechaFin, strUserId);
                }
                else if (txtIdentificacion.Text.Trim().Length > 0
                && txtFechaIni.Text.Trim().Length > 0
                && txtFechaFin.Text.Trim().Length > 0)
                {
                    try
                    {
                        DateTime.TryParse(txtFechaIni.Text.Trim() + " " + txtHoraIni.Text.Trim(), out FechaIni);
                        FechaIni = Convert.ToDateTime(txtFechaIni.Text.Trim() + " " + txtHoraIni.Text.Trim(), culture);
                    }
                    catch (Exception ex)
                    {
                        txtFechaIni.Text = string.Empty;
                        txtHoraIni.Text = string.Empty;
                        txtFechaIni.Focus();
                        strMensage = "Error validando Fecha y Hora Inicial";
                        //MessageBox(strMensage, this.Page);
                        mostrarMensaje(2, strMensage);
                        return;
                    }
                    try
                    {
                        DateTime.TryParse(txtFechaFin.Text.Trim() + " " + txtHoraFin.Text.Trim(), out FechaFin);
                        FechaFin = Convert.ToDateTime(txtFechaFin.Text.Trim() + " " + txtHoraFin.Text.Trim(), culture);
                    }
                    catch (Exception ex)
                    {
                        txtFechaIni.Text = string.Empty;
                        txtHoraFin.Text = string.Empty;
                        txtFechaIni.Focus();
                        strMensage = "Error validando Fecha y Hora Final";
                        //MessageBox(strMensage, this.Page);
                        mostrarMensaje(2, strMensage);
                        return;
                    }
                    dtVisitantes = negocio.GetVisitantes(txtIdentificacion.Text.Trim(), FechaIni, FechaFin, strUserId);
                    dtVisitantesInforme = negocio.GetVisitantesInforme(txtIdentificacion.Text.Trim(), FechaIni, FechaFin, strUserId);
                }
                else if( ( (txtFechaIni.Text.Trim().Length > 0
                    && txtFechaFin.Text.Trim().Length <= 0)
                    || (txtFechaIni.Text.Trim().Length <= 0
                    && txtFechaFin.Text.Trim().Length > 0) )
                    && txtIdentificacion.Text.Trim().Length <= 0
                    )
                {
                    strMensage = "Se deben Diligenciar Fecha Inicial y Fecha Final";
                    //MessageBox(strMensage, this.Page);
                    mostrarMensaje(2, strMensage);
                    return;
                }
                else if (((txtFechaIni.Text.Trim().Length > 0
                    && txtFechaFin.Text.Trim().Length <= 0)
                    || (txtFechaIni.Text.Trim().Length <= 0
                    && txtFechaFin.Text.Trim().Length > 0))
                    && txtIdentificacion.Text.Trim().Length > 0
                    )
                {   //Sí sólo diligencia una Feacha pero sí ingresa el documento, busca sólo por Documento.
                    dtVisitantes = negocio.GetVisitantes(txtIdentificacion.Text.Trim(), DateTime.MinValue, DateTime.MinValue, strUserId);
                    dtVisitantesInforme = negocio.GetVisitantesInforme(txtIdentificacion.Text.Trim(), DateTime.MinValue, DateTime.MinValue, strUserId);
                }

                grvVisitantes.DataSource = dtVisitantes;
                grvVisitantes.DataBind();
                if (dtVisitantes.Rows.Count > 0)
                    ViewState["dtVisitantes"] = dtVisitantesInforme;
                else
                    ViewState["dtVisitantes"] = null;

            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }



        protected void btnInforme_Click(object sender, EventArgs e)
        {
            //TODO Exportar a Excel.
            DataSet dtVisitante = new DataSet();

            //GARANTIZAR BUSQUEDA REFRESCADA
            Validar("Consultar");            
            Consultar();    //Automáticamente busca por defecto lo del día.
            //GARANTIZAR BUSQUEDA REFRESCADA

            if (ViewState["dtVisitantes"] != null)
            {
                dtVisitante.Tables.Add((DataTable)ViewState["dtVisitantes"]);
                if(txtFechaIni.Text.Trim().Length  > 0 && txtFechaFin.Text.Trim().Length > 0 )
                    ExportarExcel(dtVisitante, "Total Visitantes ", dtVisitante.Tables[0].Rows.Count.ToString()
                        , "De ", txtFechaIni.Text.Trim()
                        , " a ", txtFechaFin.Text.Trim());
                else
                    ExportarExcel(dtVisitante, "Total Visitantes ", dtVisitante.Tables[0].Rows.Count.ToString());
            }
            else
            {
                strMensage = "Sin datos para Exportar a Excel";
                //MessageBox(strMensage, this.Page);
                mostrarMensaje(2, strMensage);
            }
        }

        

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            int posFila = 0;
            string strUniqueId = string.Empty;
            try
            {
                if (!Validar("Borrar"))
                {
                    //MessageBox(strRespuesta, this.Page);
                    mostrarMensaje(2, strRespuesta);
                    return;
                }
                foreach (GridViewRow fila in grvVisitantes.Rows)
                {
                    CheckBox chkEliminar = (CheckBox)fila.FindControl("chkEliminar");
                    if (chkEliminar.Checked)
                    {
                        strUniqueId = negocio.DeleteVisitante(grvVisitantes.DataKeys[posFila]["Id"].ToString());
                        if (!strUniqueId.Equals("-1"))
                        {
                            iTipo = 1;
                            strRespuesta = "Registro(s) eliminado(s) correctamente";
                        }
                        else
                        {
                            iTipo = 2;
                            strRespuesta = "Se ha presentado un Error Eliminando el Visitante: " + grvVisitantes.DataKeys[posFila]["Nombre"].ToString().Trim();
                            break; //sale de FOREACH
                        }
                    }
                    posFila = posFila + 1;
                }
                
                //MessageBox(strRespuesta, this.Page);
                mostrarMensaje(iTipo, strRespuesta);
                Consultar();
            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            ClearControls();
            Validar("Consultar");
            vwMain.SetActiveView(vwGuardar);
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            if (!Validar("Grabar"))
            {
                //MessageBox(strRespuesta, this.Page);
                mostrarMensaje(2, strRespuesta);
                return;
            }
            try
            {
                btnGrabar.Enabled = false;
                string strUniqueId = string.Empty;
                DateTime Fecha;
                try
                {
                    DateTime.TryParse(txtFechaVisita.Text.Trim(), out Fecha);
                    Fecha = Convert.ToDateTime(txtFechaVisita.Text.Trim() + " " + txtHoraVisita.Text.Trim(), culture);
                }
                catch (Exception ex)
                {
                    txtFechaVisita.Text = string.Empty;
                    txtHoraVisita.Text = string.Empty;
                    txtFechaVisita.Focus();
                    strMensage = "Error validando Fecha y Hora de Visita";
                    //MessageBox(strMensage, this.Page);
                    mostrarMensaje(2, strMensage);
                    return;
                }

                strUniqueId = negocio.AddVisitante(txtIdentificacionAdd.Text.Trim(), txtNombre.Text.Trim(),
                    txtEmpresa.Text.Trim(), Fecha, Session["userSamAccountName"].ToString(), Session["username"].ToString()
                    , Session["userEmailAddress"].ToString(),  Session["username"].ToString());

                if (!strUniqueId.Equals("-1"))
                {
                    iTipo = 1;
                    strRespuesta = "Visitante registrado correctamente";
                    try
                    {
                        string strDatosRegistro = txtNombre.Text.Trim() + "|" + txtIdentificacionAdd.Text.Trim()
                            + "|" + txtEmpresa.Text.Trim() + "|" + Fecha.ToString();
                        SendMail("Crear", strDatosRegistro);
                    }
                    catch
                    {

                    }
                    ClearControls();
                }
                else
                {
                    iTipo = 2;
                    strRespuesta = "Se ha presentado un Error creando el Visitante, Valide los datos ingresados";
                }

                btnGrabar.Enabled = true;
                //MessageBox(strRespuesta, this.Page);
                mostrarMensaje(iTipo, strRespuesta);           

                /*if (iTipo == 2)
                    mostrarMensaje(iTipo, strRespuesta);
                else
                {
                    Response.Redirect("visitante.aspx?ban=nuevo");
                }*/
                    
            }
            catch (Exception ex)
            {
                btnGrabar.Enabled = true;
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }


        protected void btnGrabarIngreso_Click(object sender, EventArgs e)
        {
            int posFila = 0;
            string strUniqueId = string.Empty;
            try
            {
                if (!Validar("Actualizar"))
                {
                    //MessageBox(strRespuesta, this.Page);
                    mostrarMensaje(2, strRespuesta);
                    return;
                }

                foreach (GridViewRow fila in grvVisitantes.Rows)
                {
                    CheckBox chkPiso1 = (CheckBox)fila.FindControl("chkPiso1");
                    CheckBox chkPiso14 = (CheckBox)fila.FindControl("chkPiso14");

                    bool blnPiso1 = Convert.ToBoolean(grvVisitantes.DataKeys[posFila]["Piso1"].ToString().Trim());
                    bool blnPiso14 = Convert.ToBoolean(grvVisitantes.DataKeys[posFila]["Piso14"].ToString().Trim());
                    if ((!blnPiso1 && chkPiso1.Checked)
                        || (!blnPiso14 && chkPiso14.Checked))
                    {
                        strUniqueId = negocio.UpdateVisitante(grvVisitantes.DataKeys[posFila]["Id"].ToString()
                        , chkPiso1.Checked, chkPiso14.Checked,
                        Session["username"].ToString());
                    }
                    if (!strUniqueId.Equals("-1"))
                    {
                        iTipo = 1;
                        strRespuesta = "Registro(s) actualizado(s) correctamente";

                        //JULINAS 2018-08-16 15:10, corregir IF para SÓLO enviar MAIL, SI el piso aún no está marcado y se está marcando en esta rutina clic
                        //Para evitar qeu se envié a todos los otros Funcionarios que ya recibieron su visitante en el día
                        //if (chkPiso14.Checked)  //Sólo cuando llegó el Visitante a Piso 14-
                        if (!blnPiso14 && chkPiso14.Checked)  //Sólo cuando llegó el Visitante a Piso 14- 
                        {
                            string strDatosRegistro = grvVisitantes.DataKeys[posFila]["Autorizador"].ToString().Trim()
                                + "|" + grvVisitantes.DataKeys[posFila]["CorreoAutorizador"].ToString().Trim()
                                + "|" + grvVisitantes.DataKeys[posFila]["Nombre"].ToString().Trim();
                            SendMail("Ingreso", strDatosRegistro);
                        }
                        //JULINAS 2018-08-16 15:10, corregir IF para SÓLO enviar MAIL, SI el piso aún no está marcado y se está marcando en esta rutina clic
                    }
                    else
                    {
                        iTipo = 2;
                        strRespuesta = "Se ha presentado un Error Actualizando el Visitante: " + grvVisitantes.DataKeys[posFila]["Nombre"].ToString().Trim();
                        break; //sale de FOREACH
                    }
                    posFila = posFila + 1;
                }
                //MessageBox(strRespuesta, this.Page);
                mostrarMensaje(iTipo, strRespuesta);
                Consultar();
            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }

        public void ClearControls()
        {
            txtNombre.Text = string.Empty;
            txtIdentificacionAdd.Text = string.Empty;
            txtEmpresa.Text = string.Empty;
            txtFechaVisita.Text = string.Empty;

            //Consulta
            txtIdentificacion.Text = string.Empty;
            txtFechaIni.Text = string.Empty;
            txtFechaFin.Text = string.Empty;
        }

        public bool Validar(string strTipo)
        {
            DateTime Fecha;

            if (strTipo.Equals("Consultar") || strTipo.Equals("Grabar") || strTipo.Equals("Borrar")
                || strTipo.Equals("Actualizar")) 
            {
                if (Session["username"] == null || Session["username"].ToString().Trim().Length <= 0)
                    Response.Redirect("SessionEnded.aspx");
            }
            if (strTipo.Equals("Grabar"))
            {
                try
                {
                    DateTime.TryParse(txtFechaVisita.Text.Trim(), out Fecha);
                    Fecha = Convert.ToDateTime(txtFechaVisita.Text.Trim() + " " + txtHoraVisita.Text.Trim(), culture);

                    DateTime dtFechaCeroHoy = Convert.ToDateTime(DateTime.Now.Year + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00") + " 00:00:00", culture);

                    if ((dtFechaCeroHoy  - Fecha).TotalDays > 0)
                    {
                        strRespuesta = "La Fecha de Visita debe ser mayor o igual a " + DateTime.Now.Year + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    txtFechaVisita.Text = string.Empty;
                    txtFechaVisita.Focus();
                    strRespuesta = "Error validando Fecha y Hora de Visita";
                    return false;
                }
            }
            if (strTipo.Equals("Actualizar"))
            {
                int posFila = 0;
                bool blnEjecutado = false;
                //antes de Guardar, validar todos los datos a actualizar, para no permitir marcar piso 14 sin haber marcado piso 1.
                foreach (GridViewRow fila in grvVisitantes.Rows)
                {
                    CheckBox chkPiso1 = (CheckBox)fila.FindControl("chkPiso1");
                    CheckBox chkPiso14 = (CheckBox)fila.FindControl("chkPiso14");
                    if (!chkPiso1.Checked && chkPiso14.Checked)
                    {
                        strRespuesta = "No se permite hacer ingreso de Piso 14 sin haber ingresado Piso 1";
                        return false;
                    }
                }
                //antes de Guardar, validar todos los datos a actualizar, para no permitir marcar piso 14 sin haber marcado piso 1.

                //validar que efectivamente ha canbiado el estado en el CheckBox de Piso 1 o Piso 14
                foreach (GridViewRow fila in grvVisitantes.Rows)
                {
                    CheckBox chkPiso1 = (CheckBox)fila.FindControl("chkPiso1");
                    CheckBox chkPiso14 = (CheckBox)fila.FindControl("chkPiso14");

                    bool blnPiso1 = Convert.ToBoolean(grvVisitantes.DataKeys[posFila]["Piso1"].ToString().Trim());
                    bool blnPiso14 = Convert.ToBoolean(grvVisitantes.DataKeys[posFila]["Piso14"].ToString().Trim());
                    if ((!blnPiso1 && chkPiso1.Checked)
                        || (!blnPiso14 && chkPiso14.Checked))
                    {
                        blnEjecutado = true;
                    }
                    posFila = posFila + 1;
                }
                if (!blnEjecutado)  //No ha seleccionado ninguna fila para Cambio, no permitir Actualizar Nada.
                {
                    strRespuesta = "Se debe Seleccionar por lo menos un registro para Actualizar";
                    return false;                    
                }
                //validar que efectivamente ha canbiado el estado en el CheckBox de Piso 1 o Piso 14
            }

            if (strTipo.Equals("Borrar"))
            {
                bool blnEjecutado = false;              
                //validar que efectivamente ha seleccionado al menos una fila a eliminar
                foreach (GridViewRow fila in grvVisitantes.Rows)
                {
                    CheckBox chkEliminar = (CheckBox)fila.FindControl("chkEliminar");
                    if (chkEliminar.Checked)
                    {
                        blnEjecutado = true;
                    }
                }
                if (!blnEjecutado)  //No ha seleccionado ninguna fila para Cambio, no permitir Actualizar Nada.
                {
                    strRespuesta = "Se debe Seleccionar por lo menos un registro para Eliminar";
                    return false;
                }
                //validar que efectivamente ha seleccionado al menos una fila a eliminar
            }
            return true;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            DataTable dtVisitantes = new DataTable();
            try
            {
                Validar("Consultar");
                
                Consultar();
            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }


        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Validar("Consultar");
            vwMain.SetActiveView(vwConsultar);
            Consultar();    //Automáticamente busca por defecto lo del día.
        }


        
     

        protected void grvVisitantes_DataBound(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow fila in grvVisitantes.Rows)
                {
                    if (fila.RowType != DataControlRowType.Pager)
                    {
                        Label lblCrono = (Label)fila.Cells[10].FindControl("lblCrono");

                        double dblCrono = 0;
                        try
                        {
                            double.TryParse(lblCrono.Text.ToString().Trim(), out dblCrono);  //cronometro (minutos)
                        }
                        catch (Exception ex)
                        {
                            dblCrono = -1;
                        }

                        if (dblCrono >= 0)
                        {
                            lblCrono.Visible = true;
                            if (dblCrono >= VisitorMinutes) //Si supera los 20minutos, deberá mostrar Icono ROJO.
                            {
                                fila.Cells[10].BackColor = System.Drawing.Color.Red;
                            }
                            else if (dblCrono != 0)
                            {
                                fila.Cells[10].BackColor = System.Drawing.Color.Lime;
                            }
                        }

                        CheckBox chkPiso1 = (CheckBox)fila.Cells[8].FindControl("chkPiso1");
                        CheckBox chkPiso14 = (CheckBox)fila.Cells[9].FindControl("chkPiso14");

                        CheckBox chkEliminar = (CheckBox)fila.Cells[11].FindControl("chkEliminar");

                        //No habilitar la opción de eliminar si el Visitante ya Pasó algún piso.
                        if (chkPiso1.Checked || chkPiso14.Checked)
                            chkEliminar.Enabled = false;
                        //No habilitar la opción de eliminar si el Visitante ya Pasó algún piso.


                        if (Session["userRol"].ToString().Equals(Perfil.Funcionario.ToString())
                            || Session["userRol"].ToString().Equals(Perfil.Administrador.ToString())
                            )
                        {
                            //si es Usuario, no peude afectar los Pisos de Ingreso
                            chkPiso1.Enabled = false;
                            chkPiso14.Enabled = false;
                        }
                        else if (Session["userRol"].ToString().Equals(Perfil.Recepcionista.ToString()))
                        {
                            //RECEPCIONISTA NO TIENE POTESTAD DE ELIMINAR BAJO NINGUNA CIRCUNSTANCIA
                            chkEliminar.Enabled = false;

                            //según el perfíl de Recepcionista, permite o no marcar el Piso que este puede controlar
                            if (Convert.ToBoolean(Session["Piso1"]))
                                chkPiso1.Enabled = true;
                            else
                                chkPiso1.Enabled = false;

                            if (Convert.ToBoolean(Session["Piso14"]))
                                chkPiso14.Enabled = true;
                            else
                                chkPiso14.Enabled = false;

                            //deshabilitar el Piso 1, si ya esta marcado.
                            
                            if (chkPiso1.Checked)
                                chkPiso1.Enabled = false;
                            //deshabilitar el Piso 1, si ya esta marcado.

                            //deshabilitar el Piso 14, si ya esta marcado.
                            
                            if (chkPiso14.Checked)
                                chkPiso14.Enabled = false;
                            //deshabilitar el Piso 14, si ya esta marcado.

                            //JULINAS 2018-04-20 08:58 IMPEDIR MARCAR VISITA SI LA FechaVisita ES ANTERIOR A HOY.                               
                            DateTime dtFechaVisita;
                            DateTime.TryParse(fila.Cells[4].Text.Trim(), out dtFechaVisita);    //FechaVisita
                            
                            DateTime dtFechaCeroHoy = Convert.ToDateTime(DateTime.Now.Year + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00") + " 00:00:00", culture);

                            if ((dtFechaCeroHoy - dtFechaVisita).TotalDays > 0)
                            {
                                chkPiso1.Enabled = false;
                                chkPiso14.Enabled = false;
                            }

                            //JULINAS 2018-05-07 13:35 IMPEDIR MARCAR VISITA SI LA FechaVisita ES POSTERIOR A HOY.                               
                            if ((dtFechaVisita - dtFechaCeroHoy).TotalDays >= 1)
                            {
                                chkPiso1.Enabled = false;
                                chkPiso14.Enabled = false;
                            }                            

                            //Convert.ToBoolean(grvVisitantes.DataKeys[posFila]["FechaVisita"].ToString().Trim());
                           //JULINAS 2018-04-20 08:58 IMPEDIR MARCAR VISITA SI LA FechaVisita ES ANTERIOR A HOY.
                        }                        
                        
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(1, ex.Message);
            }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<visitante> GetVisitorName(string pre)
        {
            List<visitante> allVisits = new List<visitante>();
            using (VISITANTESEntities dc = new VISITANTESEntities())
            {
                //allVisits = (from a in dc.visitantes
                //             where a.nombres.StartsWith(pre)
                //             select a).ToList();
                //JULINAS 2018-04-16 PARA HACER DISTINCT POR DOCUMENTO
                allVisits = (from a in dc.visitantes
                             where a.nombres.StartsWith(pre)
                             orderby a.nombres
                             select a).GroupBy(g => g.identificacion).Select(x => x.FirstOrDefault()).ToList();
                //JULINAS 2018-04-16 PARA HACER DISTINCT POR DOCUMENTO
            }
            return allVisits;
        }

    }
}