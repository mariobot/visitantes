﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tst.aspx.cs" Inherits="RegistroVisitantes.tst" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label Text="" ID="lblInfo" runat="server" />
            <asp:Button Text="TSTConn" runat="server" ID="btnTst" OnClick="btnTst_Click" />
            <asp:GridView ID="gridTest" runat="server" AutoGenerateColumns="true" ></asp:GridView>
            <asp:Label Text="" ID="lblError" runat="server" />
        </div>
    </form>
</body>
</html>
