﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Visitante.aspx.cs" Inherits="RegistroVisitantes.Visitante" enableEventValidation="true" %>
<%@ MasterType VirtualPath="~/Default.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Main" Runat="Server">
        <link rel="stylesheet" href="scripts/jquery-ui.css" type="text/css" />
    <script language ="javascript" type="text/javascript" src="scripts/jquery.js"></script>
    <script language ="javascript" type="text/javascript" src="scripts/jquery-ui.js"></script>
    <style>
        .ul-autocomplete {
            font-size:11px;
        }
        .auto-style5 {
            height: 23px;
        }
        .auto-style7 {
            width: 399px;
        }
        .auto-style9 {
            width: 336px;
        }
        .auto-style10 {
            width: 200px;
        }
        .auto-style11 {
            width: 538px;
        }
        .auto-style12 {
            width: 606px;
        }
        .auto-style13 {
            width: 1388px;
        }
    </style>

    <script language="javascript" type="text/javascript">
        $(function () {
            $('#<%=txtNombre.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "Visitante.aspx/GetVisitorName",
                        data: "{ 'pre':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    nombres: item.nombres,
                                    empresa: item.empresa,
                                    identificacion: item.identificacion,
                                    json: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                focus: function (event, ui) {
                    $('#<%=txtNombre.ClientID%>').val(ui.item.nombres);
                    $('#<%=txtEmpresa.ClientID%>').val(ui.item.empresa);
                    $('#<%=txtIdentificacionAdd.ClientID%>').val(ui.item.identificacion);
                    return false;
                },
                select: function (event, ui) {
                    $('#<%=txtNombre.ClientID%>').val(ui.item.nombres);
                    $('#<%=txtEmpresa.ClientID%>').val(ui.item.empresa);
                    $('#<%=txtIdentificacionAdd.ClientID%>').val(ui.item.identificacion);
                    return false;
                },
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                .append("<a><br>Nombres:" + item.nombres + "<br>Empresa: " + item.empresa + "</a>")
                .appendTo(ul);
            };
        });
    </script>




    <%--<form id="form1" runat="server">--%>
        
        <asp:MultiView ID="vwMain" runat="server">
            <asp:View ID="vwConsultar" runat="server"> 
                <asp:UpdatePanel id="pnlupdConsultar" runat="server">
                <ContentTemplate>
                <asp:Panel ID="pnlConsultar" runat="server" GroupingText="Consultar Visitantes" Font-Bold="True" ForeColor="#833177" Width="99%">                
                <table style="width:75%;">
                    <tr>
                        <td class="auto-style5">
                            <asp:Label ID="lblIdentificacion" runat="server" Text="Identificación"></asp:Label><br />
                            <asp:TextBox ID="txtIdentificacion" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                        </td>
                        <asp:Panel runat="server" ID="pnlFechas" Visible="true">
                        <td class="auto-style5">
                            <asp:Label ID="lblFechaIni" runat="server" Text="Fecha Inicial"></asp:Label><br />
                            <asp:TextBox ID="txtFechaIni" runat="server" MaxLength="10" Width="80px"
                                onkeypress="javascript:  this.value = '';"
                                onkeydown="javascript:  this.value = '';"></asp:TextBox>
                            <asp:ImageButton runat="Server" ID="imgCal1" ImageUrl="~/Images/Calendar_scheduleHS.png" AlternateText="Click to show calendar" />
                            <cc1:CalendarExtender ID="txtFechaIni_CalendarExtender" runat="server" TargetControlID="txtFechaIni" Enabled="True" Format="dd/MM/yyyy"
                                PopupButtonID="imgCal1"/>
                            <asp:TextBox ID="txtHoraIni" runat="server" Width="90px" ValidationGroup="MKE" MaxLength="16" Text="00:00:00" />
                            <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server"
                                TargetControlID="txtHoraIni"
                                Mask="99:99:99"
                                MessageValidatorTip="true"
                                OnFocusCssClass="MaskedEditFocus"
                                OnInvalidCssClass="MaskedEditError"
                                MaskType="Time"
                                ErrorTooltipEnabled="True" /><br />
                            <cc1:MaskedEditValidator ID="MaskedEditValidator3" runat="server"
                                ControlExtender="MaskedEditExtender3"
                                ControlToValidate="txtHoraIni"
                                IsValidEmpty="False"
                                EmptyValueMessage="Tiempo es requerido"
                                InvalidValueMessage="Tiempo es inválido"
                                Display="Dynamic"
                                TooltipMessage="Ingrese una Hora"
                                EmptyValueBlurredText="*"
                                InvalidValueBlurredMessage="*"
                                ValidationGroup="MKE" />
                        </td>
                        <td class="auto-style5">
                            <asp:Label ID="lblFechaFin" runat="server" Text="Fecha Final"></asp:Label><br />
                            <asp:TextBox ID="txtFechaFin" runat="server" MaxLength="10" Width="80px"
                                onkeypress="javascript:  this.value = '';"></asp:TextBox>
                            <asp:ImageButton runat="Server" ID="ImgCal2" ImageUrl="~/Images/Calendar_scheduleHS.png" AlternateText="Click to show calendar" />
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" TargetControlID="txtFechaFin" Enabled="True" Format="dd/MM/yyyy"
                                PopupButtonID="ImgCal2" />

                            <asp:TextBox ID="txtHoraFin" runat="server" Width="90px" ValidationGroup="MKE" MaxLength="16" Text ="23:59:59" />
                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server"
                                TargetControlID="txtHoraFin"
                                Mask="99:99:99"
                                MessageValidatorTip="true"
                                OnFocusCssClass="MaskedEditFocus"
                                OnInvalidCssClass="MaskedEditError"
                                MaskType="Time"
                                AcceptAMPM="False"
                                ErrorTooltipEnabled="True" /><br />
                            <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server"
                                ControlExtender="MaskedEditExtender3"
                                ControlToValidate="txtHoraFin"
                                IsValidEmpty="False"
                                EmptyValueMessage="Tiempo es requerido"
                                InvalidValueMessage="Tiempo es inválido"
                                Display="Dynamic"
                                TooltipMessage="Ingrese una Hora"
                                EmptyValueBlurredText="*"
                                InvalidValueBlurredMessage="*"
                                ValidationGroup="MKE" />
                        </td>
                        </asp:Panel>
                        <td class="auto-style5">
                            <asp:Button ID="btnBuscar" runat="server" OnClick="btnBuscar_Click" Text="Buscar" ForeColor="#833177" ValidationGroup="Buscar" Width="120px" Font-Bold="True" />
                        </td>
                        <td class="auto-style5">
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" OnClick="btnNuevo_Click" ForeColor="#833177" Visible="False" Width="120px" Font-Bold="True" />
                        </td>
                    </tr>
                </table>
                <br /><br />
                
                    <asp:Panel ID="pnlResultado" runat="server" Width="99%">                    
                        <asp:GridView ID="grvVisitantes" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="Id,Identificacion,Nombre,Empresa,FechaVisita,Autorizador,CorreoAutorizador,FechaIngreso,Eliminar,Actualizar,Piso1,Piso14" OnDataBound="grvVisitantes_DataBound" Width="100%" Font-Bold="False">
                            <AlternatingRowStyle ForeColor="Black" />
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Identificacion" HeaderText="Identificación" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Empresa" HeaderText="Empresa" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaVisita" HeaderText="Fecha Visita" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Autorizador" HeaderText="Autorizador" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CorreoAutorizador" HeaderText="Correo Autorizador" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaIngreso" HeaderText="Fecha Ingreso" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" >
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Piso 1" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkPiso1" runat="server" Checked='<%# Bind("Piso1") %>' Enabled ="true"></asp:CheckBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                        
                                <asp:TemplateField HeaderText="Piso 14" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkPiso14" runat="server" Checked='<%# Bind("Piso14") %>' Enabled ="true"></asp:CheckBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Cronómetro">
                                    <ItemTemplate>
                                        <asp:Label Visible="false" ID="lblCrono" runat="server" Text='<%# Bind("cronometro") %>' Enabled ="true"></asp:Label>
                                    </ItemTemplate>
                                    <ControlStyle ForeColor="White" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle ForeColor="White" />
                                 </asp:TemplateField>

                               <asp:TemplateField HeaderText="Eliminar" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkEliminar" runat="server" Checked='<%# Bind("Eliminar") %>' Enabled ="true"></asp:CheckBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Actualizar" HeaderStyle-HorizontalAlign="Center" Visible="False">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkActualizar" runat="server" Checked='<%# Bind("Actualizar") %>' Enabled ="true"></asp:CheckBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:BoundField DataField="Piso1" HeaderText="Piso1" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" visible="false">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Piso14" HeaderText="Piso14" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" visible="false">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>


                            </Columns>
                            <HeaderStyle BackColor="#833177" ForeColor="White" />
                            <RowStyle ForeColor="Black" />
                        </asp:GridView>
                        <br />
                        <table style="width:75%;">
                        <tr>
                            <td class="auto-style13"></td>
                            <td class="auto-style13"></td>
                            <td class="auto-style13"></td>
                            <td align="right" class="auto-style11">
                                <asp:Button ID="btnInforme" runat="server" OnClick="btnInforme_Click" Text="Informe" ForeColor="#833177" Visible="False" Width="88px" Font-Bold="True" />
                            </td>
                            <td align="right" class="auto-style11">
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" OnClick="btnEliminar_Click" Font-Bold="True" ForeColor="#833177" Visible="False" Width="88px" />
                            </td>
                            <td align="right" class="auto-style11">
                                <asp:Button ID="btnGrabarIngreso" runat="server" ForeColor="#833177" OnClick="btnGrabarIngreso_Click" Text="Grabar" Visible="False" Width="88px" Font-Bold="True" />
                            </td>
                        </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
                </ContentTemplate>
                    <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnEliminar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnGrabarIngreso" EventName="Click" />
                        <asp:PostBackTrigger ControlID="btnNuevo" />
                        <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        <asp:PostBackTrigger ControlID="btnInforme"  />
                    </Triggers>
                </asp:UpdatePanel>

            </asp:View>

            <asp:View ID="vwGuardar" runat="server">
                <asp:UpdatePanel id="pnlupdGuardar" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlRegistrar" runat="server" GroupingText="Registrar Visitantes" Font-Bold="True" ForeColor="#833177" Width="55%">
                        <table style="width: 677px">
                             <tr>
                                <td class="auto-style7">
                                    <asp:Label ID="lblNombre" runat="server" Text="Nombre Completo *"></asp:Label><br />
                                    <div class="ui-widget" style="text-align:left">
                                        <asp:TextBox runat="server" ID="txtNombre" CssClass="textboxAuto" Width="300px" Font-Size="12px" MaxLength="200"/>
                                    </div>
                                    <asp:RequiredFieldValidator ID="vldNombre" runat="server" ControltoValidate="txtNombre" ErrorMessage="Ingresar Nombres" ForeColor="Red" ValidationGroup="Grabar" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator id="RegularExpressionValidatorFromNombre"
                                        ControlToValidate="txtNombre"
                                        Display="Dynamic"
                                        ValidationGroup="Grabar"
                                        ForeColor="Red"
                                        ErrorMessage="El formato del nombre no es válido!"
                                        ValidationExpression="^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$"
                                        runat="server"/> 
                                    <br /><br />
                                </td>
                                <td class="auto-style10">
                                    <asp:Label ID="lblDocumento" runat="server" Text="Identificación *"></asp:Label><br />
                                    <asp:TextBox ID="txtIdentificacionAdd" runat="server" MaxLength="50" Width="200px"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="vldIdentificacion" runat="server" ErrorMessage="Ingresar Identificación" ForeColor="Red" ValidationGroup="Grabar" ControlToValidate="txtIdentificacionAdd" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <br /><br />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style7">
                                    <asp:Label ID="lblEmpresa" runat="server" Text="Empresa *"></asp:Label><br />
                                    <asp:TextBox ID="txtEmpresa" runat="server" MaxLength="200" Width="300px"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="vldEmpresa" runat="server" ErrorMessage="Ingresar Empresa" ForeColor="Red" ValidationGroup="Grabar" ControlToValidate="txtEmpresa" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                                <td class="auto-style10">
                                    <asp:Label ID="lblFechaVisita" runat="server" Text="Fecha de la Visita *"></asp:Label><br />             
                                <asp:TextBox ID="txtFechaVisita" runat="server" MaxLength="20" Width="80px"
                                    onkeypress="javascript:  this.value = '';"
                                    onkeydown="javascript:  this.value = '';" />
                                <asp:ImageButton runat="Server" ID="ImgVisitDt" ImageUrl="~/Images/Calendar_scheduleHS.png" AlternateText="Click to show calendar" />
                                <cc1:CalendarExtender ID="txtFechaVisita_CalendarExtender" runat="server" TargetControlID="txtFechaVisita" Enabled="True" Format="dd/MM/yyyy"
                                    PopupButtonID="ImgVisitDt" />
                                <asp:TextBox ID="txtHoraVisita" runat="server" Width="85px" ValidationGroup="MKI" MaxLength="16" >10:00:00</asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server"
                                    TargetControlID="txtHoraVisita"
                                    Mask="99:99:99"
                                    MessageValidatorTip="true"
                                    OnFocusCssClass="MaskedEditFocus"
                                    OnInvalidCssClass="MaskedEditError"
                                    MaskType="Time"
                                    ErrorTooltipEnabled="True" />
                                <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server"
                                    ControlExtender="MaskedEditExtender3"
                                    ControlToValidate="txtHoraVisita"
                                    IsValidEmpty="False"
                                    EmptyValueMessage="Tiempo es requerido"
                                    InvalidValueMessage="Tiempo es inválido"
                                    Display="Dynamic"
                                    TooltipMessage="Ingrese una Hora"
                                    EmptyValueBlurredText="*"
                                    InvalidValueBlurredMessage="*"
                                    ValidationGroup="MKI" />
                                <asp:RequiredFieldValidator ID="vldFechaVisita" runat="server" ErrorMessage="Ingresar Fecha de Visita" ForeColor="Red" ValidationGroup="Grabar" ControlToValidate="txtFechaVisita" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table style="width: 677px">
                            <tr>
                                <td class="auto-style9" />
                                <td align="center">
                                    <asp:Button ID="btnGrabar" runat="server" OnClick="btnGrabar_Click" Text="Grabar" ValidationGroup="Grabar" ForeColor="#833177" Height="28px" Width="100px" Font-Bold="True"  />
                                </td>
                                <td align="center">
                                    <asp:Button ID="btnConsultar" runat="server" OnClick="btnConsultar_Click" Text="Consultar" ForeColor="#833177" Height="28px" Width="100px" Font-Bold="True" />
                                </td>
                            </tr>
                        </table>
                </asp:Panel>
                </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnGrabar" EventName="Click" />
                        <asp:PostBackTrigger ControlID="btnConsultar"  />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:View>


        </asp:MultiView>
        
        
        
<%--</form>--%>
</asp:Content>