﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using System.Diagnostics;
using System.Web.UI;
using System.IO;

using System.Text;

namespace RegistroVisitantes.App_Code
{
    public class BaseView:System.Web.UI.Page
    {   
        public enum Perfil 
        {
            Administrador = 1,
            Recepcionista = 2,
            Funcionario = 3
        };

        public enum Piso
        {
            Piso1 = 1,
            Piso14 = 2
        };
     
        public string strRespuesta = string.Empty;
        public int iTipo = 1;


        /// <summary>
        /// Mensaje de usuario basado en un Alert
        /// </summary>
        /// <param name="message">Mensaje</param>
        /// <param name="page">Página que lanza el mensaje</param>
        public static void MessageBox(string message, System.Web.UI.Page page)
        {
            if (!(message.IndexOf("HTML") > 0 && message.IndexOf("/HTML") > 0))
            {
                message = message.Replace("\r", " ");
                message = message.Replace("\"", "'");
                message = message.Replace("\r\n", " ");
                message = message.Replace("\n", " ");
                message = message.Replace("<br>", " ");
                message = message.Replace("<BR>", " ");
                message = message.Replace("<P>", " ");
                message = message.Replace("</P>", " ");
                message = message.Replace("<p>", " ");
                message = message.Replace("</p>", " ");
            }
            else
            {
                message = "El Origen retorna una cadena HTML";
            }
            page.RegisterClientScriptBlock("Message", "<script language='javascript'>alert(\"" + message.Trim() + "\");</script>");
        }



        /// <summary>
        /// Mensaje emergente con AJAX.
        /// </summary>
        /// <param name="iTipo">1 = Información, 2 = Exclamación</param>
        /// <param name="sMsgBox"></param>
        protected void mostrarMensaje(int iTipo, string sMsgBox)
        {
            Label lblMessage = (Label)Master.FindControl("lblMessage1");
            Image ImgMsgBox = (Image)Master.FindControl("ImgMsgBox1");

            AjaxControlToolkit.ModalPopupExtender ogp_MsgBox = (AjaxControlToolkit.ModalPopupExtender)Master.FindControl("ogp_MsgBox1");

            string sRutaImagen = "";
            switch (iTipo)
            {
                case 1:
                    {
                        sRutaImagen = "~/Images/MsgBox/msgInformacion.jpg";
                        lblMessage.ForeColor = System.Drawing.Color.Black;
                        break;
                    }
                case 2:
                    {
                        sRutaImagen = "~/Images/MsgBox/msgExclamacion.jpg";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        break;
                    }
            }
            ImgMsgBox.ImageUrl = sRutaImagen;
            lblMessage.Text = sMsgBox;
            ogp_MsgBox.Show();
        }


        
        public void LogException(Exception ex, String Origen, bool showError)
        {
            try
            {
                // Create the source and log, if it does not already exist.
                if (!EventLog.SourceExists("logErrorRegistroVisitantes"))
                {
                    EventLog.CreateEventSource("logErrorRegistroVisitantes", "Application");
                }
                // Create an EventLog instance and assign its source.
                EventLog eventLog = new EventLog();
                // Setting the source
                eventLog.Source = "logErrorRegistroVisitantes";

                // Write an entry to the event log.
                String error = "Error en: " + Origen + " |-| Mensaje: " + ex.Message + " |-| Pila: " + ex.StackTrace + "    " + " |-| String(): " + ex.ToString() + " |-| fecha:  " + DateTime.Now;
                eventLog.WriteEntry(error, EventLogEntryType.Error);
            }
            catch { }
        }


        protected  void Page_PreRender(object sender,  System.EventArgs e)
        {
            if (Session["username"] == null)
            {
                string strMensage = "La sesión de usuario ha caducado";
                MessageBox(strMensage, this.Page);
                Response.Redirect("SessionEnded.aspx");
            }

           

            ////////List<Control> controles = FindControlRecursive(this.Controls[0].ID.ToString());
            //////List<Control> controles = FindControlRecursive(this.Controls[0].ToString());

            //////foreach (Control con in controles)
            //////{
            //////    if (con.GetType().Name.Equals("LinkButton"))
            //////    {
            //////        LinkButton c = (LinkButton)con;
            //////        c.Enabled = false;
            //////    }
            //////    else
            //////        con.Visible = false;
            //////}
        }


        protected List<Control> FindControlRecursive(String nombre)
        {
            List<Control> controles = new List<Control>();
             FindControlRecursive(nombre, controles);
            return controles;
        }

        protected List<Control> FindControlRecursive(String nombre, List<Control> controles)
        {
            FindControlRecursive(nombre, controles, this);
            return controles;
        }

        protected List<Control> FindControlRecursive(String nombre, List<Control> controles, Control padre)
        {
            //if (padre.ID == nombre)
            if (padre.ToString() == nombre)
            {
                controles.Add(padre);
            }
            foreach (Control hijo in padre.Controls)
            {
                FindControlRecursive(nombre, controles, hijo);
            }
            return controles;
        }



        /// <summary>
        /// Crear = SI es para indicar que el Visitante ha sido creado, Ingreso = Si es para indicar que el Visitante ha llegado.
        /// </summary>
        /// <param name="strTipo">Crear,Ingreso</param>
        /// <param name="strDatosRegistro">Crear: IDVisitante|NomVisitante|EmpresaVistante|FechaVisita ** Ingreso: Autorizador|CorreoAutorizador|Nombre Visitante</param>
        public void SendMail(string strTipo, string strDatosRegistro)
        {
            try
            {
                /*  JULINAS 2016-10-05 10:25, HACER ENVÍO DE CORREO AUTOMÁTICO */
                if (System.Configuration.ConfigurationManager.AppSettings["MailFrom"] != null)
                {
                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                    message.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailFrom"].ToString());
                    message.Body = string.Empty;
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString());

                    if (strTipo.Equals("Crear"))
                    {
                        string[] arrDatosRegistro = strDatosRegistro.Split('|');
                        string strVisitante, strIdVisitante, strEmpresa, strFecha;
                        strVisitante = arrDatosRegistro[0];
                        strIdVisitante = arrDatosRegistro[1];
                        strEmpresa = arrDatosRegistro[2];
                        strFecha = arrDatosRegistro[3];

                        message.Subject = System.Configuration.ConfigurationManager.AppSettings["MailSubject1"].ToString();
                        message.To.Add(Session["userEmailAddress"].ToString().Trim());
                        message.Body = System.Configuration.ConfigurationManager.AppSettings["MailBody1"].ToString().Replace("|", "<br>");
                        message.Body = String.Format(message.Body, Session["username"].ToString(), strVisitante
                            , strVisitante, strIdVisitante, strEmpresa, strFecha);
                    }
                    else if (strTipo.Equals("Ingreso"))
                    {
                        string[] arrDatosRegistro = strDatosRegistro.Split('|');
                        string strAutorizador, strCorreoAutorizador, strVisitante;
                        strAutorizador = arrDatosRegistro[0];
                        strCorreoAutorizador = arrDatosRegistro[1];
                        strVisitante = arrDatosRegistro[2];

                        message.Subject = System.Configuration.ConfigurationManager.AppSettings["MailSubject3"].ToString();
                        message.To.Add(strCorreoAutorizador.Trim());
                        message.Body = System.Configuration.ConfigurationManager.AppSettings["MailBody3"].ToString().Replace("|", "<br>");
                        message.Body = String.Format(message.Body, strAutorizador, strVisitante);
                    }


                    message.BodyEncoding = Encoding.GetEncoding(1252);
                    message.IsBodyHtml = true;
                    //htmlBody is a string containing the entire body text
                    var htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(message.Body, new System.Net.Mime.ContentType("text/html"));
                    //This does the trick
                    htmlView.ContentType.CharSet = Encoding.UTF8.WebName;
                    message.AlternateViews.Add(htmlView);

                    try
                    {
                        smtp.Send(message);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void ExportarExcel(DataSet dtDatos, params object[] param)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=VisitorReport-" 
                + DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-"
                + DateTime.Now.Day.ToString("00") + "-" + DateTime.Now.Hour.ToString("00") 
                + DateTime.Now.Minute.ToString("00")  + ".xls");
            Response.Charset = String.Empty;
            EnableViewState = false;
            //Dim oStringWriter As New System.IO.StringWriter();
            StringWriter oStringWriter = new StringWriter();

            //Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter);
            HtmlTextWriter oHtmlTextWriter = new HtmlTextWriter(oStringWriter);


            //Dim grdplan As New GridView
            GridView grdplan = new GridView();

            grdplan.DataSource = dtDatos;
            grdplan.DataBind();

            
            grdplan.RenderControl(oHtmlTextWriter);


            string strTotalVisit = string.Empty;
            string strFechas = string.Empty;
            if (param != null)
            {
                string name;
                string val;

                strTotalVisit = (string)param[1];

                for (int i = 2; i < param.Length; i += 2)
                {
                    name = (string)param[i];
                    val = (string)param[i + 1];
                    strFechas += name + val.ToString();
                }
            }

            string strCabecera = "<table style=\"width: 100%;\" align=\"center\">";
            strCabecera += "<tr>";
            strCabecera += "<td>";
            strCabecera += "<table  align=\"center\"><tr><td align=\"center\"><font color=\"#833177\" size=\"5\"><b>INFORME DE VISITANTES</b></font><br /></td></tr></table>";
            strCabecera += "</td>";
            strCabecera += "</tr>";
            strCabecera += "<tr>";
            strCabecera += "<td>";
            strCabecera += "<table><tr><td align=\"center\">" + strFechas + "<br /></td></tr></table>";
            strCabecera += "<br /><br />";
            strCabecera += "</td>";
            strCabecera += "</tr>";
            strCabecera += "<tr>";
            strCabecera += "<td>";
            string strDetail = oStringWriter.ToString().Replace("<div>", "").Replace("</div>", "");
            strDetail = strDetail.Replace("style=\"border-collapse:collapse;\"", "style=\"border-collapse:collapse;\"  align=\"center\"");
            strDetail = strDetail.Replace("<tr>\r\n\t\t\t<th scope=\"col\">Identificaci", "<tr bgcolor=\"#833177\" style=\"color: #FFFFFF\">\r\n\t\t\t<th scope=\"col\">Identificaci");
            strDetail = strDetail.Replace("<td><span class=\"aspNetDisabled\"", "<td align=\"center\"><span class=\"aspNetDisabled\"");
            string strPiePag = "</td>";
            strPiePag += "</tr>";
            strPiePag += "<tr>";
            strPiePag += "<td>";
            strPiePag += "<table align=\"center\"><tr><td width=\"14%\"/><td width=\"14%\"/><td width=\"14%\"/><td width=\"14%\"/><td width=\"14%\"/><td width=\"14%\"/><td>Total Visitantes " + strTotalVisit + "</td></tr></table>";
            strPiePag += "</td>";
            strPiePag += "</tr>";
            strPiePag += "</table>";

            //Response.Write(oStringWriter.ToString());

            Response.Write("<div align=\"center\">" + strCabecera + strDetail + strPiePag + "</div>");

            Response.End();
        }




    }
}