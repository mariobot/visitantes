﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using BusinessLayer;

using System.DirectoryServices.AccountManagement;

namespace RegistroVisitantes
{
    public partial class Usuario : App_Code.BaseView // System.Web.UI.Page
    {
        // set up domain context
        PrincipalContext ctx = new PrincipalContext(ContextType.Domain,
        System.Configuration.ConfigurationManager.AppSettings["DomainName"].ToString());


        BusinessLayer.Negocio negocio = new BusinessLayer.Negocio();
        string strMensage = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //this.btnGrabar.Attributes.Add("onclick", "javascript:return confirm('¿Está seguro de almacenar la información diligenciada?')");
                this.btnEliminar.Attributes.Add("onclick", "javascript:return confirm('¿Está seguro de eliminar el/los usuario(s) seleccionado(s)?')");
                ddlPerfil.SelectedIndex = -1;
                LevelAccess();
                Consultar();
            }
        }

        public void LevelAccess()
        {
            try
            {
                //if (Session["userSamAccountName"].ToString().Equals("vjsalinas"))
                //    return;

                //SI EL ROL NO PERTENECE A LA PÁGINA SACARLO
                if (!Session["userRol"].ToString().Trim().Equals(Perfil.Administrador.ToString()))
                {
                    string strMensage = String.Format("el Rol '{0}' no Tiene Acceso a esta página", Session["userRol"].ToString().Trim());
                    //MessageBox(strMensage, this.Page);
                    mostrarMensaje(2, strMensage);
                    Response.Redirect("SessionEnded.aspx");
                }
                //SI EL ROL NO PERTENECE A LA PÁGINA SACARLO
            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }

        public void Consultar()
        {
            Validar("Consultar");
            DataTable dtUsuarios = new DataTable();
            try
            {
                vwMain.SetActiveView(vwConsultar);
                dtUsuarios = negocio.ListUsuarios();
                grvUsuarios.DataSource = dtUsuarios;
                grvUsuarios.DataBind();
            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            if (!Validar("Grabar"))
            {
                //MessageBox(strMensage, this.Page);
                mostrarMensaje(2, strMensage);
                return;
            }
            try
            {   
                btnGrabar.Enabled = false;
                string strUniqueId = string.Empty;
                // La siguiente validacion no es necesaria, El Selected retorna si esta seleccionado True or False
                bool blnPiso1 = chkLstPiso.Items[0].Selected.Equals(true) ? true : false;
                bool blnPiso14 = chkLstPiso.Items[1].Selected.Equals(true) ? true : false;
                strUniqueId = negocio.AddUsuario(txtNombre.Text.Trim(), txtCorreo.Text.Trim(), ddlPerfil.SelectedItem.Text.Trim()
                    , blnPiso1, blnPiso14, Session["username"].ToString());
                if (!strUniqueId.Equals("-1"))
                {
                    iTipo = 1;
                    strRespuesta = "Usuario registrado correctamente";
                    ClearControls();
                }
                else
                {
                    iTipo = 2;
                    strRespuesta = "Se ha presentado un Error creando el Usuario, Valide los datos ingresados";
                }
                //MessageBox(strRespuesta, this.Page);
                mostrarMensaje(iTipo, strRespuesta);                
                btnGrabar.Enabled = true;
            }
            catch (Exception ex)
            {
                btnGrabar.Enabled = true;
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }

        public void ClearControls()
        {
            txtNombre.Text = string.Empty;
            txtCorreo.Text = string.Empty;
            ddlPerfil.SelectedIndex = -1;
            chkLstPiso.Items[0].Selected = false;
            chkLstPiso.Items[1].Selected = false;
        }

        public bool Validar(string strTipo)
        {
            if (strTipo.Equals("Consultar") || strTipo.Equals("Grabar") || strTipo.Equals("Borrar"))
            {
                if (Session["username"] == null || Session["username"].ToString().Trim().Length <= 0)
                    Response.Redirect("SessionEnded.aspx");
            }
            if (strTipo.Equals("Grabar"))
            {
                //debe existir en Directorio Activo.                                
                if ( !ValidateActiveDirectory(txtCorreo.Text.Trim()) )
                {
                    strMensage = "Cuenta de Correo inválida en Directorio Activo";
                    return false;
                }

                if (ddlPerfil.SelectedItem.Text.Trim().Equals(Perfil.Recepcionista.ToString()))
                {
                    bool blnPiso1 = chkLstPiso.Items[0].Selected.Equals(true) ? true : false;
                    bool blnPiso14 = chkLstPiso.Items[1].Selected.Equals(true) ? true : false;
                    if (!blnPiso1 && !blnPiso14)
                    {
                        strMensage = "Para Recepcionista se requiere habilitar por lo menos un Piso.";
                        return false;
                    }
                }
            }

            if (strTipo.Equals("Borrar"))
            {
                bool blnEjecutado = false;
                //validar que efectivamente ha seleccionado al menos una fila a eliminar
                foreach (GridViewRow fila in grvUsuarios.Rows)
                {
                    CheckBox chkEliminar = (CheckBox)fila.FindControl("chkEliminar");
                    if (chkEliminar.Checked)
                    {
                        blnEjecutado = true;
                    }
                }
                if (!blnEjecutado)  //No ha seleccionado ninguna fila para Cambio, no permitir Borrar Nada.
                {
                    strMensage = "Se debe Seleccionar por lo menos un registro para Eliminar";
                    return false;
                }
                //validar que efectivamente ha seleccionado al menos una fila a eliminar
            }

            return true;
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            ClearControls();
            Validar("Consultar");
            vwMain.SetActiveView(vwGuardar);
        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Validar("Consultar");
            vwMain.SetActiveView(vwConsultar);
            Consultar();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            int posFila = 0;
            string strUniqueId = string.Empty;
            try
            {
                if (!Validar("Borrar"))
                {
                    //MessageBox(strMensage, this.Page);
                    mostrarMensaje(2, strMensage);
                    return;
                }
                foreach (GridViewRow fila in grvUsuarios.Rows)
                {
                    CheckBox chkEliminar = (CheckBox)fila.FindControl("chkEliminar");
                    if (chkEliminar.Checked)
                    {
                        strUniqueId = negocio.DeleteUsuario(grvUsuarios.DataKeys[posFila]["Id"].ToString());
                        if (!strUniqueId.Equals("-1"))
                        {
                            iTipo = 1;
                            strRespuesta = "Registro(s) eliminado(s) correctamente";
                        }
                        else
                        {
                            iTipo = 2;
                            strRespuesta = "Se ha presentado un Error Eliminando el Usuario: " + grvUsuarios.DataKeys[posFila]["Nombre"].ToString().Trim();
                            break; //sale de FOREACH
                        }
                    }
                    posFila = posFila + 1;
                }
                //MessageBox(strRespuesta, this.Page);
                mostrarMensaje(iTipo, strRespuesta);
                ClearControls();
                Consultar();
            }
            catch (Exception ex)
            {
                //MessageBox(ex.Message, this.Page);
                mostrarMensaje(2, ex.Message);
            }
        }

        protected void ddlPerfil_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPerfil.SelectedItem.Text.Trim().Equals(Perfil.Administrador.ToString()))
            {
                chkLstPiso.Enabled = false;
                chkLstPiso.Items[0].Selected = false;
                chkLstPiso.Items[1].Selected = false;
            }
            else
                chkLstPiso.Enabled = true;
        }

        /// <summary>
        /// Valida contra Directorio Activo que la cuenta de Correo sea válida.
        /// </summary>
        /// <param name="strEmail">Recibe Cuenta de Correo del Usuario a Registrar</param>
        /// <returns></returns>
        public bool ValidateActiveDirectory(string strEmail)
        {
            string[] arrAccount = strEmail.Split('@');
            if (ValidarUsuario(arrAccount[0]))
                return true;
            else
                return false;
        }
        
        /// <summary>
        /// Valida contra Directorio Activo que la el UserID sea válido.
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        public bool ValidarUsuario(string strUserID)
        {
            try
            {
                try
                {
                    // find a user
                    UserPrincipal user = UserPrincipal.FindByIdentity(ctx, strUserID);

                    if (user != null)
                    {
                        // do something here....
                        strRespuesta = "";
                        return true;
                    }
                    else
                    {
                        strRespuesta = "Error En Directorio Activo validando Usuario: " + strUserID;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    LogException(ex, "BaseView.ValidarUsuario", false);
                    strRespuesta = "se ha Presentado una Excepción Validando el Usuario contra el Directorio Activo";
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogException(ex, "BaseView.ValidarUsuario", false);
                strRespuesta = "se ha Presentado una Excepción Validando el Usuario en la Aplicación";
                return false;
            }
        }

        protected void grvUsuarios_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow fila in grvUsuarios.Rows)
            {
                fila.Cells[4].HorizontalAlign = HorizontalAlign.Center;
                fila.Cells[5].HorizontalAlign = HorizontalAlign.Center;
                fila.Cells[6].HorizontalAlign = HorizontalAlign.Center;
            }
        }
    }
}