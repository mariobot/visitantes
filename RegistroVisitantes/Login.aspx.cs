﻿using RegistroVisitantes.App_Code;
using System;
using System.Data;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace RegistroVisitantes
{
    public partial class Login : System.Web.UI.Page
    {
        BusinessLayer.Negocio negocio = new BusinessLayer.Negocio();
        public string strRespuesta = string.Empty;

        RegistroVisitantes.App_Code.BaseView BaseView = new BaseView();

        // set up domain context
        PrincipalContext ctx = new PrincipalContext(ContextType.Domain,
            System.Configuration.ConfigurationManager.AppSettings["DomainName"].ToString());

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bool blnAutoLogin = false;
                try
                {
                    blnAutoLogin = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AutoLogin"].ToString());
                }
                catch
                {
                    blnAutoLogin = false;
                }
                

                //Comentar para pruebas Debug(podeer entrar con diferentes Roles)
                if (blnAutoLogin)
                {
                    ////////JULINAS 2018-04-09 09:56 AUTENTICACIÓN AUTOMÁTICA
                    ////////Obtiene el User Name del Cliente.
                    //////ShowMessageBox(2, "Get Client User: " + HttpContext.Current.User.Identity.Name.ToString());

                    //////string strUser2 = WindowsIdentity.GetCurrent().Name;
                    //////ShowMessageBox(2, "strUser2: " + strUser2);  //strUser2: NT AUTHORITY\NETWORK SERVICE
                    ////////////return;
                    //////////////////2018-05-21 15:35
                    //////string userName = string.Empty;

                    //////if (System.Web.HttpContext.Current != null &&
                    //////    System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                    //////{
                    //////    System.Web.Security.MembershipUser usr = Membership.GetUser();
                    //////    if (usr != null)
                    //////    {
                    //////        userName = usr.UserName;
                    //////    }
                    //////}
                    //////ShowMessageBox(2, "userName: " + userName); //userName: 
                    ////////////return;

                    //////ShowMessageBox(2, "Request.LogonUserIdentity.Name.ToString(): " + Request.LogonUserIdentity.Name.ToString());   //Request.LogonUserIdentity.Name.ToString(): NT AUTHORITY\IUSR  
                    //////////return;

                    //////////MembershipUser myObject = Membership.GetUser(); //ESTE DA ERROR DE SQL SEEVER.
                    //////////string UserName = myObject.UserName.ToString(); //ESTE DA ERROR DE SQL SEEVER.
                    //////////ShowMessageBox(2, "UserName >> " + UserName);   //ESTE DA ERROR DE SQL SEEVER.
                    ////////////return;

                    //////////////////2018-05-21 15:35

                    //////string strUser0 = Environment.UserName;
                    //////ShowMessageBox(2, "strUser0 ** " + strUser0);   //strUser0 ** BOGSSGV110P1520$
                    //////////////return;

                    ////////solo si forms y windows authentication estan enabled y los demas disabled
                    ////////solo si forms y windows authentication estan enabled y los demas disabled
                    ////////solo si forms y windows authentication estan enabled y los demas disabled
                    //////ShowMessageBox(2, "HttpContext.Current.User.Identity.Name: " + HttpContext.Current.User.Identity.Name); //HttpContext.Current.User.Identity.Name: PRE\gamorales
                    ////////return;

                    string strUser = Environment.UserName;      //Error En Directorio Activo validando Usuario: BOGSSGV110P1520$

                    string[] arrLogin = HttpContext.Current.User.Identity.Name.Split('\\');
                    foreach (string strLogin in arrLogin)
                    {
                        txtUserName.Text = strLogin; //El Último, Ejemplo: INDRA\\vjsalinas , asigna INDRA y luego vjsalinas
                    }
                    if (txtUserName.Text.Trim().Length <= 0)    //En caso que obtener el usuario no se pueda desde WindowsIdentity.GetCurrent() lo tona de Environment.UserName
                        txtUserName.Text = strUser;


                    if (ValidarUsuario(txtUserName.Text.ToString().Trim()))
                    {
                        //ShowMessageBox(2, Session["userRol"].ToString() + "-" + BaseView.Perfil.Funcionario.ToString());
                        //return;
                        if (Session["userRol"].ToString().Equals(BaseView.Perfil.Funcionario.ToString()))
                            Response.Redirect("Visitante.aspx");
                        else if (Session["userRol"].ToString().Equals(BaseView.Perfil.Recepcionista.ToString()))
                            Response.Redirect("Visitante.aspx");
                        else if (Session["userRol"].ToString().Equals(BaseView.Perfil.Administrador.ToString()))
                            Response.Redirect("Usuario.aspx");
                        else
                            Response.Redirect("Default.aspx");
                    }
                    else
                        ShowMessageBox(2, strRespuesta);
                    //JULINAS 2018-04-09 09:56 AUTENTICACIÓN AUTOMÁTICA
                }
                //Comentar para pruebas Debug(podeer entrar con diferentes Roles)
            }
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            if (ValidarUsuario(txtUserName.Text.ToString().Trim()))
            {
                if(Session["userRol"].ToString().Equals(BaseView.Perfil.Funcionario.ToString()))
                    Response.Redirect("Visitante.aspx");
                else if (Session["userRol"].ToString().Equals(BaseView.Perfil.Recepcionista.ToString()))
                    Response.Redirect("Visitante.aspx");
                else if (Session["userRol"].ToString().Equals(BaseView.Perfil.Administrador.ToString()))
                    Response.Redirect("Usuario.aspx");
                else
                    Response.Redirect("Default.aspx");
            }
            else
                ShowMessageBox(2, strRespuesta);
        }


        public bool ValidarUsuario(string strUserID)
        {
            try
            {
                try
                {
                    // find a user
                    //UserPrincipal user = UserPrincipal.FindByIdentity(ctx, strUserID);
                    //UserPrincipal user = UserPrincipal.FindByIdentity(ctx, @"PRE\"+strUserID);
                    UserPrincipal user = UserPrincipal.FindByIdentity(ctx, @System.Configuration.ConfigurationManager.AppSettings["DomainName"].ToString() + "\\" + strUserID.Trim());
                    //strRespuesta = "Error En Directorio Activo validando Usuario: " + @System.Configuration.ConfigurationManager.AppSettings["DomainName"].ToString() + "\\" + strUserID.Trim();
                    //return false;
                    
                    if (user != null)
                    {                       
                        var usersSid = user.Sid; // GUI NUMBER                        
                        var username = user.DisplayName;    //Salinas Rodríguez, Victor Julian                        
                        var userSamAccountName = user.SamAccountName; //vjsalinas                        
                        var userEmailAddress = user.EmailAddress;   //vjsalinas@indracompany.com                        
                        
                        Session["username"] = username;
                        Session["userSamAccountName"] = userSamAccountName;
                        Session["userEmailAddress"] = userEmailAddress;

                        /*ShowMessageBox(1, "username: " + Session["username"] + " >> userSamAccountName: " + Session["userSamAccountName"]
                            + ">> userEmailAddress: " + Session["userEmailAddress"]);*/
                    }
                    else
                    {
                        strRespuesta = "Error En Directorio Activo validando Usuario: " + strUserID.Trim();
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    BaseView.LogException(ex, "Login.ValidarUsuario", false);
                    strRespuesta = "se ha Presentado una Excepción Validando el Usuario contra el Directorio Activo";
                    return false;
                }
                
                DataTable dtUsuario = new DataTable();

                //strRespuesta = Session["userEmailAddress"].ToString();
                //return false;

                if (Session["userEmailAddress"].ToString().Length > 0
                    && Session["userEmailAddress"].ToString().IndexOf("@") > 0)
                {
                    dtUsuario = negocio.GetUsuarioByEmail(Session["userEmailAddress"].ToString().Trim());
                    if (dtUsuario.Rows.Count > 1)   //AUNQUE IMPROBABLE, PEUDE ESTAR EL CORREO MÁS DE UNA VEZ.
                    {
                        strRespuesta = "Error validando Usuario: " + txtUserName.Text.Trim() + ", cuenta de correo duplicada";
                        return false;
                    }
                    else if (dtUsuario.Rows.Count == 1) //ES RECEPCIONISTA O ADMINISTRADOR
                    {
                        if (dtUsuario.Rows[0]["Perfil"].ToString().Trim().Equals("Administrador"))
                        {
                            Session["userRol"] = BaseView.Perfil.Administrador.ToString();
                            Session["Piso1"] = null;
                            Session["Piso14"] = null;
                        }
                        else if (dtUsuario.Rows[0]["Perfil"].ToString().Trim().Equals("Recepcionista"))
                        {
                            Session["userRol"] = BaseView.Perfil.Recepcionista.ToString();
                            Session["Piso1"] = dtUsuario.Rows[0]["Piso1"].ToString();
                            Session["Piso14"] = dtUsuario.Rows[0]["Piso14"].ToString();
                        }
                        else //Si se inventan otro Rol loco, será Recepcionista por Defecto.
                        {
                            Session["userRol"] = BaseView.Perfil.Recepcionista.ToString();
                            Session["Piso1"] = null;
                            Session["Piso14"] = null;
                        }
                    }
                    else if (dtUsuario.Rows.Count <= 0) //ES FUNCIONARIO
                        Session["userRol"] = BaseView.Perfil.Funcionario.ToString();
                }
                else
                {
                    strRespuesta = "Error validando cuenta de correo para Usuario: " + txtUserName.Text.Trim();
                    return false;
                }
                strRespuesta = "";
                return true;
                
            }
            catch (Exception ex)
            {
                BaseView.LogException(ex, "Login.ValidarUsuario", false);
                strRespuesta = "se ha Presentado una Excepción Validando el Usuario en la Aplicación";
                return false;
            }
        }


        protected void ButtonClickedHandler(object sender, EventArgs e)
        {
            try
            {
                string sSession = "";
                /*
                int iSecCodeApp = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SecurityAppCode"].ToString());
                sSession = SecurityService.KillRemoteUserSession(iSecCodeApp, txtUserName.Text);
                SecurityService.Close();
                Hashtable htSessionsToEnd = (Hashtable)Application["htSessionsToEnd"];
                if (htSessionsToEnd == null) htSessionsToEnd = new Hashtable();
                Application.Lock();
                htSessionsToEnd[sSession] = sSession;
                Application["htSessionsToEnd"] = htSessionsToEnd;
                Application.UnLock();
                */
                ShowMessageBox(1, "Su sesión ha sido terminada exitosamente. Intente acceder nuevamente a la aplicación.");
            }
            catch (Exception ex)
            {
                //Trace.Warn("ButtonClickedHandler", ex.Message);                
                BaseView.LogException(ex, "ButtonClickedHandler", false);
                ShowMessageBox(2, ex.Message);
            }
        }


        protected void ShowMessageBox(int iTipo, string sMsgBox)
        {
            string sRutaImagen = "";
            switch (iTipo)
            {
                case 1:
                    {
                        sRutaImagen = "~/Images/MsgBox/msgInformacion.jpg";
                        lblMessage.ForeColor = System.Drawing.Color.Black;
                        break;
                    }
                case 2:
                    {
                        sRutaImagen = "~/Images/MsgBox/msgExclamacion.jpg";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        break;
                    }
            }
            ImgMsgBox.ImageUrl = sRutaImagen;
            lblMessage.Text = sMsgBox;
            mpe_MsgBox.Show();
        }

        public void ShowConfirmBox(string sMsgBox)
        {
            lblConfirmText.ForeColor = System.Drawing.Color.Black;
            lblConfirmText.Text = sMsgBox;
            mpe_ConfirmBox.Show();
        }
    }
}