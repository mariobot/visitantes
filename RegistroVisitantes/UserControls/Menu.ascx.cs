﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

//////using Ecopetrol.Common.Seguridad.ServiceClient.SecurityService;

public partial class UserControls_Menu : System.Web.UI.UserControl
{
    protected void Menu_Load(object sender, EventArgs e)
    {
        
        if (Session["dsUserMenu"] == null)
        {
            DataSet dsUser = (DataSet)Session["dsUser"];
            //////SeguridadClient SecurityService = new SeguridadClient();
            DataSet dsUserMenu = new DataSet();
            //////if (SecurityService != null && dsUser != null)
                //////dsUserMenu = SecurityService.GetMenuByUser(Convert.ToInt32(ConfigurationManager.AppSettings["SecurityAppCode"]), Convert.ToInt32(dsUser.Tables[0].Rows[0]["UserId"]));

            dsUserMenu.Tables.Add();
            Session["dsUserMenu"] = dsUserMenu;
            //////SecurityService.Close();
        }
        else 
        {
            DataSet dsMenu = (DataSet)Session["dsUserMenu"];
            DataRow dr = dsMenu.Tables[0].NewRow();
            dr["MenuId"] = 0;
            dr["NombreMenu"] = "Inicio";
            //JULINAS 05.01.2010 tarde, kitar la opcion inicio quemada y debe mencionarse desde Menú en Compoennte de Seguridad.
            ////JULINAS 05.01.2010 dia, IR AL MENU INICIO DEL PORTAL PRINCIPAL (MTP)
            ////string servidor = System.Configuration.ConfigurationManager.AppSettings["MTP"].ToString();
            ////string puerto = System.Configuration.ConfigurationManager.AppSettings["puerto"].ToString();
            ////string app = System.Configuration.ConfigurationManager.AppSettings["app"].ToString();
            ////dr["URL"] = "http://" + servidor + ":" + puerto + "/" + app +  "/inicio.aspx";
            //dr["URL"] = "~/inicio.aspx";
            ////JULINAS 05.01.2010 dia, IR AL MENU INICIO DEL PORTAL PRINCIPAL (MTP)            
            //dr["MenuOrder"] = "0";
            //dsMenu.Tables[0].Rows.InsertAt(dr, 0);
            //JULINAS 05.01.2010 tarde, kitar la opcion inicio quemada y debe mencionarse desde Menú en Compoennte de Seguridad.
            //////XmlDSMenu.Data = dsMenu.GetXml();
            dsMenu.RejectChanges(); 
        }
               
    }
}
