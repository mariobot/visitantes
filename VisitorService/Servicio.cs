﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Collections;
using System.ServiceProcess;
using System.Configuration;

using System.Text;
/**
 * <Creación>
 * <Autor> Faruck </Autor>
 * <Fecha> 01/02/2017  </Fecha>
 * <Comentarios>Servicio de windows, que realiza la grabacion de video de eventos</Comentarios>
 * </Creación>
 */


namespace VisitorService
{
    public partial class Servicio : ServiceBase
    {

        private readonly static int VisitorMinutes = Int16.Parse(ConfigurationManager.AppSettings["VisitorMinutes"].ToString());

        System.Timers.Timer oTimer = null;
        double interval = 1000;

        public Servicio()
        {
            InitializeComponent();
            InitializeService();
            //activar al pasar a pruebas
            //inicializarLogs();  /*SE DEScOMENTA PARA DESPLEGAR SERVICIO*/
            CronometroVisitante();  //se comenta para desplegar servicio 
        }

        void InitializeService()
        {
            //se inicializa el timer para que este lanzandose cada n segundos
            oTimer = new System.Timers.Timer(interval);
            oTimer.Enabled = true;
            oTimer.AutoReset = true;            
            oTimer.Elapsed += new System.Timers.ElapsedEventHandler(oTimer_Elapsed);
            //oTimer.Start();//se comenta para DEBUG servicio 
            oTimer.Stop();  //se comenta para desplegar servicio 
        }

        void oTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //do {
            try
            {
                CronometroVisitante();
            }
            catch (Exception ex)
            {
                eventLog.WriteEntry(ex.ToString());
            }
            //} while (true);  

        }
        
        protected override void OnStart(string[] args)
        {
            eventLog.WriteEntry("Se ha iniciado el servicio de Visitante");            

            oTimer.Enabled = true;
            oTimer.Start();
        }

        protected override void OnStop()
        {
            eventLog.WriteEntry("Se ha detenido el servicio de Visitante");
        }

        public void CronometroVisitante()
        {
            try
            {
                DAO dao = new DAO();
                DataSet data = new DataSet();
                data = dao.GetVisitorsTimer(VisitorMinutes);

                foreach (DataRow itera in data.Tables[0].Rows)
                {
                    try
                    {
                       if (Double.Parse(itera["cronometro"].ToString()) > VisitorMinutes)
                           SendMail(itera);
                    }
                    catch (Exception ex)
                    {
                        eventLog.WriteEntry(ex.ToString());
                    }
                }
                dao = null;
                data.Dispose();
            }
            catch (Exception ex)
            {
                eventLog.WriteEntry(ex.ToString());
            }            
        }

        public void SendMail(DataRow fila)
        {
            try
            {
                DAO Dao = new DAO();
                
                /*  JULINAS 2016-10-05 10:25, HACER ENVÍO DE CORREO AUTOMÁTICO */
                if (System.Configuration.ConfigurationManager.AppSettings["MailFrom"] != null)
                {
                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                    message.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailFrom"].ToString());
                    message.Body = string.Empty;
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SmtpServer"].ToString());

                    string[] arrMailTo = System.Configuration.ConfigurationManager.AppSettings["MailTo"].ToString().Split(',');
                    foreach (string strMailTo in arrMailTo)
                    {
                        message.To.Add(strMailTo);
                    }
                    message.Subject = System.Configuration.ConfigurationManager.AppSettings["MailSubject"].ToString();
                    /*
                    message.Body = "Sr(a). {0}" + "<br>" +
                        "El visitante {1} ha superado el tiempo máximo de espera para anunciarse en el piso 14. Agradecemos tu seguimiento para ubicar a dicho invitado. " + "<br>" +
                        "Una copia de este correo ha sido enviada al Command Center para hacer el seguimiento pertinente. " + "<br>" +
                        "Agradecemos que se revise el caso y se reporte cualquier novedad al Command Center al correo {2} o al teléfono {3}."  + "<br>";
                    */
                    message.Body = System.Configuration.ConfigurationManager.AppSettings["MailBody"].ToString().Replace("|","<br>");

                    message.Body = String.Format(message.Body, fila["autorizador"].ToString(), fila["nombres"].ToString()
                        , arrMailTo[0].ToString(), System.Configuration.ConfigurationManager.AppSettings["CallTo"].ToString());


                    message.BodyEncoding = Encoding.GetEncoding(1252);
                    message.IsBodyHtml = true;
                    //htmlBody is a string containing the entire body text
                    var htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(message.Body, new System.Net.Mime.ContentType("text/html"));
                    //This does the trick
                    htmlView.ContentType.CharSet = Encoding.UTF8.WebName;
                    message.AlternateViews.Add(htmlView);

                    string strUniqueId = string.Empty;
                    try
                    {
                        smtp.Send(message);
                        strUniqueId = Dao.SetVisitorAlert(fila["id"].ToString());
                        if (strUniqueId.Equals("-1"))
                            eventLog.WriteEntry("Se presentó un Error Actualizando el Estado de Alerta Visitante: " + fila["nombres"].ToString());
                    }
                    catch (Exception ex)
                    {
                        strUniqueId = Dao.SetVisitorAlert(fila["id"].ToString());
                        if (strUniqueId.Equals("-1"))
                            eventLog.WriteEntry("Se presentó un Error Actualizando el Estado de Alerta Visitante: " + fila["nombres"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                eventLog.WriteEntry(ex.ToString());
            }
        }


        //metodo que crea el log de eventos de fallas
        public void inicializarLogs()
        {
           if (!System.Diagnostics.EventLog.SourceExists("EventosVisitante"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "EventosVisitante", "LogError");
            }
           eventLog.Source = "EventosVisitante";
            eventLog.Log = "LogError";            
        }
    }
}
