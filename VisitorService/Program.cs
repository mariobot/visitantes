﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Windows.Forms; //se agrega para hacer debug

namespace VisitorService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //se agrega para hacer debug
            /*SE DEScOMENTA PARA hcAER DEBUG*/
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ServiceTest());            
            
            /*SE DEScOMENTA PARA hcAER DEBUG*/


            /*SE DEScOMENTA PARA DESPLEGAR SERVICIO*/ 
            
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new Servicio() 
            };
            ServiceBase.Run(ServicesToRun);
            
            /*SE DEScOMENTA PARA DESPLEGAR SERVICIO*/  
        }
    }
}
