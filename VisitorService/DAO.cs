﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using DI.DataHelper;
using DI.DataHelper.DataHelper;

namespace VisitorService 
{
    class DAO : DI.DataHelper.DataHelper.DataLayer
    {
        //string strConn = ConfigurationManager.ConnectionStrings["strConn"].ConnectionString;
        string cadena;
        public DAO()
        {
            try
            {
                cadena = ConfigurationManager.ConnectionStrings["strConn"].ToString();

                helper = Factory.CreateSqlHelper(cadena);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string SetVisitorAlert(string VisitorId)
        {
            try
            {
                //SqlConnection oSqlConnection = new SqlConnection(strConn);
                //oSqlConnection.Open();
                //SqlCommand sqlT = new SqlCommand();
                //sqlT.CommandType = CommandType.Text;
                //sqlT.CommandText = String.Format("UPDATE visitante SET [alertaenviada] = {0}, nombremodificacion = '{1}', fechamodificacion = GETDATE() WHERE Id = '{2}'"
                //    , 1, "VisitorService" ,VisitorId);
                //sqlT.Connection = oSqlConnection;
                //sqlT.ExecuteNonQuery();
                //sqlT.Dispose();

                return Convert.ToString(helper.ExecuteReturn("Visitante_Alert", CommandType.StoredProcedure, "@ModificadoPor", "VisitorService",
                    "@Id",  VisitorId));
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetVisitorsTimer(int VisitorMinutes)
        {
            try
            {
                //SqlConnection oSqlConnection = new SqlConnection(strConn);
                //oSqlConnection.Open();

                //SqlCommand oSqlCommand = new SqlCommand();
                //oSqlCommand.CommandType = CommandType.Text;
                //oSqlCommand.CommandText = "SELECT CONVERT(NUMERIC(38,4), (DATEDIFF(SECOND,FECHAINGRESO, ISNULL(FECHAINGRESO2,GETDATE()) ) ))/60 as cronometro " +
                //    ", * FROM VISITANTE " +
                //"WHERE FECHAINGRESO IS NOT NULL " +
                //"and FECHAINGRESO2 IS NULL " +
                //"and alertaenviada = 0";

                //oSqlCommand.Connection = oSqlConnection;
                //DataSet ds = new DataSet();
                //SqlDataAdapter oSqlDataAdapter = new SqlDataAdapter(oSqlCommand);
                //oSqlDataAdapter.Fill(ds);

                //oSqlConnection.Close();
                ////se añade debido al error del pool maximo de conexiones
                //oSqlConnection.Dispose();
                //oSqlCommand.Dispose();
                //return ds;

                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                string strSqlSentence = "SELECT CONVERT(NUMERIC(38,4), (DATEDIFF(SECOND,FECHAINGRESO, ISNULL(FECHAINGRESO2,GETDATE()) ) ))/60 as cronometro " +
                    ", * FROM VISITANTE " +
                "WHERE FECHAINGRESO IS NOT NULL " +
                "and FECHAINGRESO2 IS NULL " +
                "and alertaenviada = 0";

                dt = helper.ExecuteDataTable(strSqlSentence, CommandType.Text, null);
                ds.Tables.Add(dt);
                return ds;
                
            }
            catch(Exception ex)
            {
                throw ex;   
            }
        }


    }
}
