using DI.DataHelper.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Web; //JULINAS 2017-10-13 09:35 incluirlo apra llamar llaves del web.config

namespace DI.DataHelper.DataHelper
{
    /// <summary>
    /// DbDal provee metodos para realizar operaciones y consultas con la base de datos
    /// </summary>
    public class SqlHelper : IDbHelper, ITransaction
    {
        private SqlConnection connection;
        private SqlTransaction transaction;
        /// <summary>
        /// Construye un nueva instancia de DbDal a partir de una cadena de conecci�n
        /// </summary>
        /// <param name="connectionString">Cadena de Conecci�n</param>
        public SqlHelper(string connectionString)
        {
            connection = new SqlConnection(connectionString);
            transaction = null;
            //System.Diagnostics.Debug.WriteLine("SqlHelper:" + connectionString);
        }
        /// <summary>
        /// Comienza una transaccion en esta instancia de la SqlDal
        /// </summary>
        public void Begin()
        {
            connection.Open();
            transaction = connection.BeginTransaction();
        }
        /// <summary>
        /// Deshace la transacci�n.
        /// </summary>
        public void Rollback()
        {
            if (transaction == null) return;
            transaction.Rollback();
            connection.Close();
            transaction.Dispose();
            transaction = null;
        }
        /// <summary>
        /// Efectua los cambios de la transacci�n.
        /// </summary>
        public void Commit()
        {
            transaction.Commit();
            connection.Close();
            transaction.Dispose();
            transaction = null;
        }
        /// <summary>
        /// Devuelve un DataSet como resultado de ejecutar una sentencia SQL
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>El DataSet resultado de la operaci�n</returns>
        public DataSet ExecuteDataSet(string sql, CommandType type, params object[] param)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            try
            {
                Open();
                adapter.Fill(ds);
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return ds;
        }
        /// <summary>
        /// Devuelve un DataTable como resultado de ejecutar una sentencia SQL
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>El DataTable resultado de la operaci�n</returns>
        public DataTable ExecuteDataTable(string sql, CommandType type, params object[] param)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = GetCommand(sql, type);
            //JULINAS 2017-10-13 09:35 INCREMENTAR TIMEOUT DEFAULT DE LA CONEXI�N, PARA CIERTOS PROCEDURES. 
            try
            {
                if (System.Configuration.ConfigurationSettings.AppSettings["SQLExtensionTime"].ToString().IndexOf(sql) > -1)
                    cmd.CommandTimeout = Convert.ToInt16(System.Configuration.ConfigurationSettings.AppSettings["SQLTimeOutMinutes"].ToString()) * 60;
            }
            catch (Exception ex){ }
            //JULINAS 2017-10-13 09:35 INCREMENTAR TIMEOUT DEFAULT DE LA CONEXI�N, PARA CIERTOS PROCEDURES. 
            CollectParameters(cmd, param);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            try
            {
                Open();
                adapter.Fill(dt);
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return dt;
        }
        /// <summary>
        /// Devuelve el valor ubicado en la primera fila, primera columna de la consulta
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>Object conteniendo el resultado</returns>
        public object ExecuteScalar(string sql, CommandType type, params object[] param)
        {
            object scalar;
            SqlCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            try
            {
                Open();
                scalar = cmd.ExecuteScalar();
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return scalar;
        }
        /// <summary>
        /// Devuelve el valor especificado por la instrucci�n RETURN del Stored Procedure, No tiene sentido ejecutar esto cuando no es un Stored Procedure.
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>Object conteniendo el resultado</returns>
        public object ExecuteReturn(string sql, CommandType type, params object[] param)
        {
            SqlCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            SqlParameter p = cmd.CreateParameter();
            p.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(p);
            try
            {
                Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return p.Value;
        }
        /// <summary>
        /// Ejectua una sentencia SQL que no devuelve un resultado en particular, tal como un INSERT o un UPDATE
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>El n�mero de columnas afectadas por la operaci�n</returns>
        public int ExecuteNonQuery(string sql, CommandType type, params object[] param)
        {
            int rows;
            SqlCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            try
            {
                Open(true); // cortegaa 29.01.2017 se agrega bandera para insertar auditoria solo para ExecuteNowQuery
                rows = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return rows;
        }

        /// <summary>
        /// Devuelve un DataReader que permite leer los datos de una consulta.
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>Un IDataReader para leer los datos.</returns>
        public IDataReader ExecuteReader(string sql, CommandType type, params object[] param)
        {
            if (transaction != null) throw new ApplicationException("No se puede usar DataReader en una transacci�n");
            IDataReader reader;
            SqlCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            try
            {
                Open();
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                Close();
                throw Process(e, sql);
            }
            return reader;
        }
        /// <summary>
        /// Prepara el comando donde se va a ejecutar la senetencia escogida.
        /// </summary>
        /// <param name="command">String que contienen el comando a ejecutar.</param>
        /// <param name="commandType">Tipo de comando.</param>
        /// <returns></returns>
        private SqlCommand GetCommand(string command, CommandType commandType)
        {
            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.CommandType = commandType;
            cmd.CommandTimeout = 240;
            if (transaction != null) cmd.Transaction = transaction;
            return cmd;
        }
        /// <summary>
        /// Habre la conexion a la base de datos teniendo encuenta si hay una transaccion pendiente.
        /// </summary>
        private void Open(bool bAuditoria=false) // cortegaa 29.01.2017 se agrega bandera para insertar auditoria solo para ExecuteNowQuery
        {
            if (transaction == null)
            {
                if (connection.State != ConnectionState.Open)  
                { 
                    connection.Open();
                    if (bAuditoria)
                        AuditoriaLog();   // cortegaa 29.01.2017 se agrega bandera para insertar auditoria solo para ExecuteNowQuery
                }
            }
        }
        /// <summary>
        /// Cierra la conexion a la base de datos teniendo encuenta si hay una transaccion pendiente.
        /// </summary>
        private void Close()
        {
            if (transaction == null) connection.Close();
        }
        /// <summary>
        /// Maneja las excepcions cuando se presenta un error
        /// </summary>
        /// <param name="e">Excepci�n a procesar.</param>
        /// <param name="cmd">String que representa la sentencia que se si iba a ejecutar.</param>
        /// <returns>Execpci�n procesada.</returns>
        private static Exception Process(Exception e, string cmd)
        {
            Trace.WriteLine("Error Ejecutando:" + cmd);
            Trace.WriteLine(e.Message);
            return (e is System.Data.SqlClient.SqlException) ? new Exception(e.Message.ToString() + e.Source.ToString() , e) : e;
        }
        /// <summary>
        /// Funci�n interna que asocia los parametros especificados con un comando
        /// </summary>
        /// <param name="cmd">El comando al que se van a asociar los parametros</param>
        /// <param name="param">conjunto de parametros en la forma {Nombre,Valor}</param>
        private static void CollectParameters(SqlCommand cmd, params object[] param)
        {
            if (param == null) return;
            SqlParameter prm;
            string name;
            object val;
            for (int i = 0; i < param.Length; i += 2)
            {
                name = (string)param[i];
                val = param[i + 1];
                prm = new SqlParameter();
                prm.ParameterName = name;
                prm.Direction = ParameterDirection.Input;
                switch (Type.GetTypeCode(val.GetType()))
                {
                    case TypeCode.Int64:
                        prm.SqlDbType = SqlDbType.BigInt;
                        prm.Value = (val.Equals((long)0) && name.EndsWith("ID")) ? DBNull.Value : val;
                        break;
                    case TypeCode.Int32:
                        prm.SqlDbType = SqlDbType.Int;
                        prm.Value = (val.Equals((int)0) && name.EndsWith("ID")) ? DBNull.Value : val;
                        break;
                    case TypeCode.Int16:
                        prm.SqlDbType = SqlDbType.SmallInt;
                        prm.Value = (val.Equals((short)0) && name.EndsWith("ID")) ? DBNull.Value : val;
                        break;
                    case TypeCode.Byte:
                        prm.SqlDbType = SqlDbType.TinyInt;
                        prm.Value = (val.Equals((byte)0) && name.EndsWith("ID")) ? DBNull.Value : val;
                        break;
                    case TypeCode.String:
                        prm.SqlDbType = SqlDbType.VarChar;
                        string tmp = ((string)val).Trim();
                        if (tmp.Length == 0) prm.Value = DBNull.Value;
                        else prm.Value = tmp;
                        break;
                    case TypeCode.Double:
                        prm.SqlDbType = SqlDbType.Float;
                        prm.Value = val;
                        break;
                    case TypeCode.Decimal:
                        prm.SqlDbType = SqlDbType.Decimal;
                        prm.Value = val;
                        break;
                    case TypeCode.DateTime:
                        prm.SqlDbType = SqlDbType.DateTime;
                        prm.Value = ((DateTime)val == DateTime.MinValue) ? DBNull.Value : val;
                        break;
                    case TypeCode.Boolean:
                        prm.SqlDbType = SqlDbType.Bit;
                        prm.Value = val;
                        break;
                    case TypeCode.Single:
                        prm.SqlDbType = SqlDbType.Real;
                        prm.Value = val;
                        break;
                        //JULINAS 2018-01-23 16:07 AGREGAR TIPODE  DATO PARA ARCHIVOS VARBINARY
                    case TypeCode.Object:
                        prm.SqlDbType = SqlDbType.VarBinary;
                        prm.Value = val;
                        break;
                    //JULINAS 2018-01-23 16:07 AGREGAR TIPODE  DATO PARA ARCHIVOS VARBINARY
                }
                cmd.Parameters.Add(prm);
            }
        }


        // cortegaa 29.01.2017
        // Ajustes AuditoriaLogs 

        /// <summary>
        /// Guardar auditoriaLog en el momento de realizar un ExecuteNowQuery
        /// </summary>
        private void AuditoriaLog()
        {
            string usuario = "No Asignada";
            string ip = "No Asignado";
            try
            {
                HttpContext context = HttpContext.Current;
                usuario = (string)(context.Session["idUsuario"]);
                usuario = usuario.ToString();
                ip = GetLocalIPAddress(); 
                ExecuteNonQueryAuditoriaLogs("AuditoriaLog_Temp", CommandType.StoredProcedure, "@usuario", usuario, "@ip", ip.ToString());
            }
            catch (Exception e)
            {
            }
        }

        /// <summary>
        /// capturar direccion IP del cliente
        /// </summary>
        /// <returns></returns>
        public static string GetLocalIPAddress()
        {
            var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        /// <summary>
        /// ExecuteNowquery
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="type"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private int ExecuteNonQueryAuditoriaLogs(string sql, CommandType type, params object[] param)
        {
            int rows;
            SqlCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            try
            {
                //   Open();
                rows = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                // Close();
            }
            return rows;
        }
       
        // cortegaa 29.01.2017

    }
}