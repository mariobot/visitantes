using System;
using DI.DataHelper.Common;

namespace DI.DataHelper.DataHelper
{
    /// <summary>
    /// Clase base para los objetos que constituyen la capa de datos de las aplicaciones.
    /// </summary>
    public class DataLayer
    {
        /// <summary>
        /// IDbHelper en el cual se ejecutan las operaciones sobre la base de datos
        /// </summary>
        protected IDbHelper helper;
        /// <summary>
        /// Devuelve un objecto que representa el contexto de la transaccion sobre la cual se ejecutan los operaciones.
        /// </summary>
        /// <returns>Un objeto ITransaction que representa la transacción.</returns>
        public ITransaction GetTransaction()
        {
            return (ITransaction)helper;
        }
        /// <summary>
        /// Indica que otro objeto DataLayer va a participar en la misma transacción del objeto actual. Importante, es necesario que los objetos usen la misma base de datos.
        /// </summary>
        /// <param name="dl">El objeto que que va a compartir la transacción.</param>
        public void JoinTransaction(DataLayer dl)
        {
            dl.helper = helper;
        }
        /// <summary>
        /// Obtiene el IDbHelper que esta usando actualmente el objeto.
        /// </summary>
        /// <returns>El IDbHelper.</returns>
        public IDbHelper GetHelper()
        {
            return helper;
        }
        /// <summary>
        /// Inicia una transacción.
        /// </summary>
        public void BeginTransaction()
        {
            ITransaction t = (ITransaction)helper;
            t.Begin();
        }
        /// <summary>
        /// Acomete las operaciones de la transacción.
        /// </summary>
        public void CommitTransaction()
        {
            ITransaction t = (ITransaction)helper;
            t.Commit();
        }
        /// <summary>
        /// Deshace las operaciones de la transacción.
        /// </summary>
        public void RollbackTransaction()
        {
            ITransaction t = (ITransaction)helper;
            t.Rollback();
        }
    }
}
