using System;
using System.IO;
using System.Security.Cryptography;
using DI.DataHelper.DataHelper;

namespace DI.DataHelper.DataHelper
{
    /// <summary>
    /// Summary description for DataBaseConnection.
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// Crea una instancia de un ayudante de acceso a datos para SQL Server.
        /// </summary>
        /// <returns>Devuelve un objeto IDbHelper que representa al ayudante.</returns>
        public static IDbHelper CreateSqlHelper(string connection)
        {
            return new SqlHelper(connection);
        }
        /// <summary>
        /// Crea una instancia de un ayudante de acceso a datos para un proveedor OleDb.
        /// </summary>
        /// <returns>Devuelve un objeto IDbHelper que representa al ayudante.</returns>
        public static IDbHelper CreateOleDbHelper(string connection)
        {
            return new OleDbHelper(connection);
        }
        /// <summary>
        /// Extrae una cadena de conexion encriptada en el archivo especificado.
        /// </summary>
        /// <param name="fileName">Nombre del archivo que contiene la cadena de conecci�n</param>
        /// <returns>La cadena de conexion contenida en el archivo.</returns>
        public static string ReadFromFile(string fileName)
        {
            string cs;
            RijndaelManaged RMCrypto = new RijndaelManaged();
            byte[] Key = { (byte)'S', (byte)'o', (byte)'f', (byte)'t', (byte)'w', (byte)'a', (byte)'r', (byte)'e', (byte)'L', (byte)'a', (byte)'y', (byte)'e', (byte)'r', (byte)'.', (byte)'.', (byte)'.' };
            byte[] IV = { (byte)'.', (byte)'.', (byte)'.', (byte)'r', (byte)'e', (byte)'y', (byte)'a', (byte)'L', (byte)'e', (byte)'r', (byte)'a', (byte)'w', (byte)'t', (byte)'f', (byte)'o', (byte)'S' };
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            CryptoStream cStream = new CryptoStream(fs, RMCrypto.CreateDecryptor(Key, IV), CryptoStreamMode.Read);
            StreamReader sr = new StreamReader(cStream);
            cs = sr.ReadToEnd().Trim();
            sr.Close();
            cStream.Close();
            fs.Close();
            return cs;
        }

    }
}
