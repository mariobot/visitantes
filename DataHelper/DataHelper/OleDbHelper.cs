using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using DI.DataHelper.Common;

namespace DI.DataHelper.DataHelper
{
    /// <summary>
    /// DbDal provee metodos para realizar operaciones y consultas con la base de datos
    /// </summary>
    public class OleDbHelper : IDbHelper, ITransaction
    {
        private OleDbConnection connection;
        private OleDbTransaction transaction;
        /// <summary>
        /// Construye un nueva instancia de DbDal a partir de una cadena de conecci�n
        /// </summary>
        /// <param name="connectionString">Cadena de Conecci�n</param>
        public OleDbHelper(string connectionString)
        {
            connection = new OleDbConnection(connectionString);
            transaction = null;
            System.Diagnostics.Debug.WriteLine("OleDbHelper:" + connectionString);
        }
        /// <summary>
        /// Comienza una transaccion en esta instancia de la OleDbDal
        /// </summary>
        public void Begin()
        {
            connection.Open();
            transaction = connection.BeginTransaction();
        }
        /// <summary>
        /// Deshace la transacci�n.
        /// </summary>
        public void Rollback()
        {
            if (transaction == null) return;
            transaction.Rollback();
            connection.Close();
            transaction = null;
        }
        /// <summary>
        /// Efectua los cambios de la transacci�n.
        /// </summary>
        public void Commit()
        {
            transaction.Commit();
            connection.Close();
            transaction = null;
        }
        /// <summary>
        /// Devuelve un DataSet como resultado de ejecutar una sentencia SQL
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>El DataSet resultado de la operaci�n</returns>
        public DataSet ExecuteDataSet(string sql, CommandType type, params object[] param)
        {
            DataSet ds = new DataSet();
            OleDbCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            OleDbDataAdapter adapter = new OleDbDataAdapter(cmd);
            try
            {
                Open();
                adapter.Fill(ds);
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return ds;
        }
        /// <summary>
        /// Devuelve un DataTable como resultado de ejecutar una sentencia SQL
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>El DataTable resultado de la operaci�n</returns>
        public DataTable ExecuteDataTable(string sql, CommandType type, params object[] param)
        {
            DataTable dt = new DataTable();
            OleDbCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            OleDbDataAdapter adapter = new OleDbDataAdapter(cmd);
            try
            {
                Open();
                adapter.Fill(dt);
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return dt;
        }
        /// <summary>
        /// Devuelve el valor ubicado en la primera fila, primera columna de la consulta
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>Object conteniendo el resultado</returns>
        public object ExecuteScalar(string sql, CommandType type, params object[] param)
        {
            object scalar;
            OleDbCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            try
            {
                Open();
                scalar = cmd.ExecuteScalar();
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return scalar;
        }
        /// <summary>
        /// Devuelve el valor especificado por la instrucci�n RETURN del Stored Procedure, No tiene sentido ejecutar esto cuando no es un Stored Procedure.
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>Object conteniendo el resultado</returns>
        public object ExecuteReturn(string sql, CommandType type, params object[] param)
        {
            OleDbCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            OleDbParameter p = cmd.CreateParameter();
            p.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(p);
            try
            {
                Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return p.Value;
        }
        /// <summary>
        /// Ejectua una sentencia SQL que no devuelve un resultado en particular, tal como un INSERT o un UPDATE
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>El n�mero de columnas afectadas por la operaci�n</returns>
        public int ExecuteNonQuery(string sql, CommandType type, params object[] param)
        {
            int rows;
            OleDbCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            try
            {
                Open();
                rows = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw Process(e, sql);
            }
            finally
            {
                Close();
            }
            return rows;
        }

        /// <summary>
        /// Devuelve un DataReader que permite leer los datos de una consulta.
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>Un IDataReader para leer los datos.</returns>
        public IDataReader ExecuteReader(string sql, CommandType type, params object[] param)
        {
            IDataReader reader;
            OleDbCommand cmd = GetCommand(sql, type);
            CollectParameters(cmd, param);
            try
            {
                Open();
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                Close();
                throw Process(e, sql);
            }
            return reader;
        }
        /// <summary>
        /// Prepara el comando donde se va a ejecutar la senetencia escogida.
        /// </summary>
        /// <param name="command">String que contienen el comando a ejecutar.</param>
        /// <param name="commandType">Tipo de comando.</param>
        /// <returns></returns>
        private OleDbCommand GetCommand(string command, CommandType commandType)
        {
            OleDbCommand cmd = new OleDbCommand(command, connection);
            cmd.CommandType = commandType;
            if (transaction != null) cmd.Transaction = transaction;
            return cmd;
        }
        /// <summary>
        /// Habre la conexion a la base de datos teniendo encuenta si hay una transaccion pendiente.
        /// </summary>
        private void Open()
        {
            if (transaction == null)
            {
                if (connection.State != ConnectionState.Open) connection.Open();
            }
        }
        /// <summary>
        /// Cierra la conexion a la base de datos teniendo encuenta si hay una transaccion pendiente.
        /// </summary>
        private void Close()
        {
            if (transaction == null) connection.Close();
        }
        /// <summary>
        /// Maneja las excepcions cuando se presenta un error
        /// </summary>
        /// <param name="e">Excepci�n a procesar.</param>
        /// <param name="cmd">String que representa la sentencia que se si iba a ejecutar.</param>
        /// <returns>Execpci�n procesada.</returns>
        private static Exception Process(Exception e, string cmd)
        {
            Trace.WriteLine("Error Ejecutando:" + cmd);
            Trace.WriteLine(e.Message);
            return (e is System.Data.OleDb.OleDbException) ? new Exception("Ocurrio una excepcion accesando a la base de datos", e) : e;
        }
        /// <summary>
        /// Funci�n interna que asocia los parametros especificados con un comando
        /// </summary>
        /// <param name="cmd">El comando al que se van a asociar los parametros</param>
        /// <param name="param">conjunto de parametros en la forma {Nombre,Valor}</param>
        private static void CollectParameters(OleDbCommand cmd, params object[] param)
        {
            if (param == null) return;
            OleDbParameter prm;
            string name;
            object val;
            for (int i = 0; i < param.Length; i += 2)
            {
                name = (string)param[i];
                val = param[i + 1];
                prm = new OleDbParameter();
                prm.ParameterName = name;
                prm.Direction = ParameterDirection.Input;
                switch (Type.GetTypeCode(val.GetType()))
                {
                    case TypeCode.Int64:
                        prm.OleDbType = OleDbType.BigInt;
                        prm.Value = (val.Equals((long)0) && name.EndsWith("ID")) ? DBNull.Value : val;
                        break;
                    case TypeCode.Int32:
                        prm.OleDbType = OleDbType.Integer;
                        prm.Value = (val.Equals((int)0) && name.EndsWith("ID")) ? DBNull.Value : val;
                        break;
                    case TypeCode.Int16:
                        prm.OleDbType = OleDbType.SmallInt;
                        prm.Value = (val.Equals((short)0) && name.EndsWith("ID")) ? DBNull.Value : val;
                        break;
                    case TypeCode.Byte:
                        prm.OleDbType = OleDbType.TinyInt;
                        prm.Value = (val.Equals((byte)0) && name.EndsWith("ID")) ? DBNull.Value : val;
                        break;
                    case TypeCode.String:
                        prm.OleDbType = OleDbType.VarChar;
                        string tmp = ((string)val).Trim();
                        if (tmp.Length == 0) prm.Value = DBNull.Value;
                        else prm.Value = tmp;
                        break;
                    case TypeCode.Double:
                        prm.OleDbType = OleDbType.Double;
                        prm.Value = val;
                        break;
                    case TypeCode.Decimal:
                        prm.OleDbType = OleDbType.Currency;
                        prm.Value = val;
                        break;
                    case TypeCode.DateTime:
                        prm.OleDbType = OleDbType.Date;
                        prm.Value = ((DateTime)val == DateTime.MinValue) ? DBNull.Value : val;
                        break;
                    case TypeCode.Boolean:
                        prm.OleDbType = OleDbType.Boolean;
                        prm.Value = val;
                        break;
                    case TypeCode.Single:
                        prm.OleDbType = OleDbType.Single;
                        prm.Value = val;
                        break;
                }
                cmd.Parameters.Add(prm);
            }
        }
    }
}