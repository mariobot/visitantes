using System;
using System.Data;

namespace DI.DataHelper.DataHelper
{
    /// <summary>
    /// Interface que define a los ayudates de acceso a bases de datos
    /// </summary>
    public interface IDbHelper
    {
        /// <summary>
        /// Devuelve un DataSet como resultado de ejecutar una sentencia SQL
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>El DataSet resultado de la operaci�n</returns>
        DataSet ExecuteDataSet(string sql, CommandType type, params object[] param);
        /// <summary>
        /// Devuelve un DataTable como resultado de ejecutar una sentencia SQL
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>El DataTable resultado de la operaci�n</returns>
        DataTable ExecuteDataTable(string sql, CommandType type, params object[] param);
        /// <summary>
        /// Devuelve el valor ubicado en la primera fila, primera columna de la consulta
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>Object conteniendo el resultado</returns>
        object ExecuteScalar(string sql, CommandType type, params object[] param);
        /// <summary>
        /// Devuelve el valor especificado por la instrucci�n RETURN del Stored Procedure, No tiene sentido ejecutar esto cuando no es un Stored Procedure.
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>Object conteniendo el resultado</returns>
        object ExecuteReturn(string sql, CommandType type, params object[] param);
        /// <summary>
        /// Ejectua una sentencia SQL que no devuelve un resultado en particular, tal como un INSERT o un UPDATE
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>El n�mero de columnas afectadas por la operaci�n</returns>
        int ExecuteNonQuery(string sql, CommandType type, params object[] param);
        /// <summary>
        /// Devuelve un IDataReader que permite leer los datos de una consulta.  Atencion: cerrar explicitamente el DataReader despues de usarlo. Si se usa y este se cierra, dentro de una transacci�n provoca un Rollback automatico.  NO se recomienda usar DataReaders dentro de transacciones.
        /// </summary>
        /// <param name="sql">Texto del comando a ejecutar</param>
        /// <param name="type">Tipo de sentencia que se va a ejecutar</param>
        /// <param name="param">Conjunto de parametros con la forma {Nombre,valor}, si no hay se coloca null (nothing en VB.NET)</param>
        /// <returns>Un IDataReader para leer los datos.</returns>
        IDataReader ExecuteReader(string sql, CommandType type, params object[] param);
    }
}
