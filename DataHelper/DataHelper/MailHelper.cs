using System;
using System.Web.Mail;

namespace DI.DataHelper.DataHelper
{
    /// <summary>
    /// Clase que se encarga de la construcción de correos electrónicos
    /// </summary>
    public class MailHelper
    {
        /// <summary>
        /// Envia correo a travez de un correo SMTP
        /// </summary>
        private string server;
        /// <summary>
        /// Crea un nuevo objeto MailDal
        /// </summary>
        /// <param name="mailServer">el nombre del servidor SMTP a travez del cual se enviaran correos</param>
        public MailHelper(string mailServer)
        {
            server = mailServer;
        }
        /// <summary>
        /// Envia correo electronico
        /// </summary>
        /// <param name="from">Dirección de correo del remitente</param>
        /// <param name="to">Dirección del destinatario</param>
        /// <param name="subject">Asunto</param>
        /// <param name="body">Cuerpo del mensaje</param>
        public void SendMail(string from, string to, string subject, string body)
        {
            MailMessage mail = new MailMessage();
            mail.To = to;
            mail.From = from;
            mail.Subject = subject;
            mail.Body = body;
            mail.Priority = MailPriority.Normal;
            mail.BodyFormat = MailFormat.Text;
            SmtpMail.SmtpServer = server;
            SmtpMail.Send(mail);
        }
        /// <summary>
        /// Envia un correo con archivos adjuntos;
        /// </summary>
        /// <param name="from">Dirección del remitente.</param>
        /// <param name="to">Dirección a la que se enviará.</param>
        /// <param name="subject">Asunto del correo.</param>
        /// <param name="body">Cuerpo del correo.</param>
        /// <param name="file">Arreglo de Strings que contiene el path a los archivos para adjuntar.</param>
        public void SendMail(string from, string to, string subject, string body, string[] file)
        {
            MailMessage mail = new MailMessage();
            mail.To = to;
            mail.From = from;
            mail.Subject = subject;
            mail.Body = body;
            mail.Priority = MailPriority.High;
            mail.BodyFormat = MailFormat.Text;
            for (int i = 0; i < file.Length; i++) mail.Attachments.Add(new MailAttachment(file[i]));
            SmtpMail.SmtpServer = server;
            SmtpMail.Send(mail);
        }
    }
}
