using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace DI.DataHelper.Security
{
    /// <summary>
    /// Summary description for Encripter.
    /// </summary>
    public class Crypto
    {
        /// <summary>
        /// Llave Publica
        /// </summary>
        //private static string EncripterKey = "SoftwareLayer...";
        /// <summary>
        /// Llave privada
        /// </summary>
        //private static string EncripterIV = "...reyaLerawtfoS";
        private static string EncripterKey = "SoftwareLayer...";
        /// <summary>
        /// Llave privada
        /// </summary>
        private static string EncripterIV = "...reyaLerawtfoS";
        /// <summary>
        /// Constructor
        /// </summary>
        public Crypto()
        {
        }
        /// <summary>
        /// Convierte un string en un array de Bytes
        /// </summary>
        /// <param name="StrData"></param>
        /// <returns></returns>
        private static byte[] ToByteArray(string StrData)
        {
            char[] Data = StrData.ToCharArray();

            byte[] RetData = new byte[Data.Length];
            for (int i = 0; i < Data.Length; i++)
            {
                RetData[i] = (byte)Data[i];
            }
            return RetData;
        }
        /// <summary>
        /// Encripta la informacion proporcionada
        /// </summary>
        /// <param name="Information">Cadena con la informacion a encriptar</param>
        /// <returns>Cadena con la representacion Hexadecimal (unicode) de los bytes encriptados</returns>
        public static string Encript(string Information)
        {
            if (Information.Length > 0)
            {
                RijndaelManaged myRijndael = new RijndaelManaged();
                //Get the key and IV.
                byte[] key = ToByteArray(EncripterKey);
                byte[] IV = ToByteArray(EncripterIV);

                //UnicodeEncoding ue=new UnicodeEncoding();
                ASCIIEncoding TextEncoder = new ASCIIEncoding();
                //byte[] tmp=ue.GetBytes(DataToEncript);
                byte[] tmp = TextEncoder.GetBytes(Information);

                //Get an encryptor.
                ICryptoTransform encryptor = myRijndael.CreateEncryptor(key, IV);

                MemoryStream ms = new MemoryStream();
                CryptoStream csEncrypt = new CryptoStream(ms, encryptor, CryptoStreamMode.Write);
                csEncrypt.Write(tmp, 0, tmp.Length);
                csEncrypt.FlushFinalBlock();
                csEncrypt.Close();
                ms.Close();

                return ToHexString(ms.ToArray());

            }
            else return "";
        }

        /// <summary>
        /// Desencripta la informacion encriptada
        /// </summary>
        /// <param name="HexEncriptedInformation">Cadena con la representacion Hexadecimal (unicode) de los bytes encriptados</param>
        /// <returns>Cadena de texto desencriptada</returns>
        public static string Decript(string HexEncriptedInformation)
        {
            if (HexEncriptedInformation.Length > 0)
            {
                RijndaelManaged myRijndael = new RijndaelManaged();

                byte[] key = ToByteArray(EncripterKey);
                byte[] IV = ToByteArray(EncripterIV);

                //UnicodeEncoding ue=new UnicodeEncoding();
                ASCIIEncoding TextEncoder = new ASCIIEncoding();
                byte[] tmp = StrHexToByteArray(HexEncriptedInformation);

                //Get an encryptor.
                ICryptoTransform decryptor = myRijndael.CreateDecryptor(key, IV);
                MemoryStream ms = new MemoryStream(tmp);
                CryptoStream csDecrypt = new CryptoStream(ms, decryptor, CryptoStreamMode.Read);

                byte[] fromEncrypt = new byte[tmp.Length];

                csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
                csDecrypt.Close();
                ms.Close();

                return TextEncoder.GetString(fromEncrypt).Replace("\0", "");
            }
            else
                return "";
        }

        /// <summary>
        /// Convierte un vector de bytes en su representacion de texto Hexadecimal
        /// </summary>
        /// <param name="Data">Array de bytes</param>
        /// <returns>Cadena de texto con la represntacion Hexadecimal de la informacion</returns>
        private static string ToHexString(byte[] Data)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < Data.Length; i++)
            {
                string StrHex = Data[i].ToString("X2");
                sb.Append(StrHex);
            }
            return sb.ToString();

        }
        /// <summary>
        /// Convierte una cadena con la representacion hexadecimal a un array de bytes
        /// </summary>
        /// <param name="HexString">Cadena con represntacion hexadecimal en unicode</param>
        /// <returns></returns>
        private static byte[] StrHexToByteArray(string HexString)
        {
            byte[] RetData = new Byte[HexString.Length / 2];

            int k = 0;
            for (int i = 0; i < HexString.Length; i += 2)
            {
                string HexNumber = HexString.Substring(i, 2);
                RetData[k++] = byte.Parse(HexNumber, System.Globalization.NumberStyles.HexNumber);
            }
            return RetData;
        }

        /// <summary>
        /// Ejemplos de encripcion
        /// </summary>
        public static void Example()
        {
            string original = "This is a much longer string of data than a public/private key algorithm will accept.";
            string roundtrip;
            ASCIIEncoding textConverter = new ASCIIEncoding();
            RijndaelManaged myRijndael = new RijndaelManaged();
            byte[] fromEncrypt;
            byte[] encrypted;
            byte[] toEncrypt;
            byte[] key;
            byte[] IV;

            //Create a new key and initialization vector.
            myRijndael.GenerateKey();
            myRijndael.GenerateIV();

            //Get the key and IV.
            key = myRijndael.Key;
            IV = myRijndael.IV;

            //Get an encryptor.
            ICryptoTransform encryptor = myRijndael.CreateEncryptor(key, IV);

            //Encrypt the data.
            MemoryStream msEncrypt = new MemoryStream();
            CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

            //Convert the data to a byte array.
            toEncrypt = textConverter.GetBytes(original);

            //Write all data to the crypto stream and flush it.
            csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
            csEncrypt.FlushFinalBlock();

            //Get encrypted array of bytes.
            encrypted = msEncrypt.ToArray();

            //This is where the message would be transmitted to a recipient
            // who already knows your secret key. Optionally, you can
            // also encrypt your secret key using a public key algorithm
            // and pass it to the mesage recipient along with the RijnDael
            // encrypted message. 

            //Get a decryptor that uses the same key and IV as the encryptor.
            ICryptoTransform decryptor = myRijndael.CreateDecryptor(key, IV);

            //Now decrypt the previously encrypted message using the decryptor
            // obtained in the above step.
            MemoryStream msDecrypt = new MemoryStream(encrypted);
            CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

            fromEncrypt = new byte[encrypted.Length];

            //Read the data out of the crypto stream.
            csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

            //Convert the byte array back into a string.
            roundtrip = textConverter.GetString(fromEncrypt);

            //Display the original data and the decrypted data.
            Console.WriteLine("Original: {0}", original);
            Console.WriteLine("Round Trip: {0}", roundtrip);

        }
        /// <summary>
        /// Ejemplo de encripcion
        /// </summary>
        public static void Example2()
        {
            string StrData = "Software Layer LTDA";
            string Encripted = Encript(StrData);

            System.Diagnostics.Trace.WriteLine("Original :" + StrData);
            System.Diagnostics.Trace.WriteLine("Encriptado :" + Encripted);

            string UnEncripted = Decript(Encripted);
            System.Diagnostics.Trace.WriteLine("Restaurado :" + UnEncripted);

        }
    }
}
