using System;

namespace DI.DataHelper.Common
{
    /// <summary>
    /// Interfaz que representa una transaccion gobernada por la capa del negocio.
    /// </summary>
    public interface ITransaction
    {
        /// <summary>
        /// Comienza una transacción.
        /// </summary>
        void Begin();
        /// <summary>
        /// Deshace la transacción.
        /// </summary>
        void Rollback();
        /// <summary>
        /// Acomete las operaciones realizadas por la transacción.
        /// </summary>
        void Commit();
    }
}
