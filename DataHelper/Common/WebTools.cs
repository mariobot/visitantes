using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;


namespace DI.DataHelper.Common
{
    /// <summary>
    /// Emun para opciones de llenado de controles de lista
    /// </summary>
    public enum FillOptions
    {
        ClearAll = 0,
        DontClear = 1,
        PutSelectCaption = 2
    }
    public enum months
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12
    }
    public enum Meses
    {
        Enero = 1,
        Febrero = 2,
        Marzo = 3,
        Abril = 4,
        Mayo = 5,
        Junio = 6,
        Julio = 7,
        Agosto = 8,
        Septiembre = 9,
        Octubre = 10,
        Noviembre = 11,
        Diciembre = 12
    }

    /// <summary>
    /// Contiene metodos para manejo y llenado de datos (contenidos en DataTables) para controles Web.
    /// </summary>
    public class WebTools
    {
        /// <summary>
        /// Colocala el valor las variables de session al objeto session, consultandolo en la base de datos
        /// </summary>
        /// <param name="User">Datatable con la informacion del usuario</param>
        /// <param name="Sesion">Objeto session</param>
        /// <param name="strPassword">Cadena que contiene la contrase�a del usuario</param>
        public static void ValidateUser(DataTable User, System.Web.SessionState.HttpSessionState Sesion, string strPassword)
        {
            string dbPass;
            if (User.Rows.Count > 0)
            {
                Sesion.Add("idUser", User.Rows[0]["idUsuario"].ToString());
                dbPass = User.Rows[0]["Password"].ToString();
                if (dbPass.ToLower() == strPassword.ToLower())
                {
                    Sesion.Add("name", User.Rows[0]["Nombre"].ToString());
                    Sesion.Add("idprofile", User.Rows[0]["idPerfil"].ToString());
                    Sesion.Add("email", User.Rows[0]["Email"].ToString());
                    Sesion.Add("login", User.Rows[0]["Login"].ToString());
                    Sesion.Add("iddistrito", User.Rows[0]["idDistrito"].ToString());
                    Sesion.Add("idoficina", User.Rows[0]["idOficina"].ToString());

                }
                else
                {
                    throw new System.Exception(User.Rows[0]["Nombre"].ToString() + ", ingreso la contrase�a incorrecta.");
                }
            }
            else
            {
                throw new System.Exception("Ingreso su login incorrecto.");
            }
        }
        /// <summary>
        /// Adiciona un Boton a un control Datagrid
        /// </summary>
        /// <param name="dg">Datagrig que se va a llenar</param>
        /// <param name="Type">Tipo de boton</param>
        /// <param name="DataName">Nombre del dato a mostrar</param>
        /// <param name="CommandName">Nombre del comando a ejecutar</param>
        /// <param name="HeaderText">Texto del titulo de la columna</param>
        public static void AddButtonColumntoDataGrid(DataGrid dg, ButtonColumnType Type, string DataName, string CommandName, string HeaderText)
        {
            ButtonColumn btnColumn = new ButtonColumn();

            btnColumn.CommandName = CommandName;
            btnColumn.ButtonType = Type;
            btnColumn.DataTextField = DataName;
            btnColumn.HeaderText = HeaderText;

            dg.Columns.Add(btnColumn);
        }

        /// <summary>
        /// Adiciona un Boton a un control Datagrid
        /// </summary>
        /// <param name="dg">Datagrig que se va a llenar</param>
        /// <param name="Type">Tipo de boton</param>
        /// <param name="ButtonText">Texto del boton</param>
        /// <param name="CommandName">Nombre del comando a ejecutar</param>
        public static void AddButtonColumntoDataGrid(DataGrid dg, ButtonColumnType Type, string ButtonText, string CommandName)
        {
            ButtonColumn btnColumn = new ButtonColumn();

            btnColumn.CommandName = CommandName;
            btnColumn.ButtonType = Type;
            btnColumn.Text = ButtonText;
            dg.Columns.Add(btnColumn);
        }

        /// <summary>
        /// Adiciona una columna a un control Datagrid
        /// </summary>
        /// <param name="dg">Datagrig que se va a llenar</param>
        /// <param name="DataName">Nombre del dato a mostrar</param>
        /// <param name="IsVisible">Indica si la columna es visible o no</param>
        /// <param name="HeaderText">Texto del titulo de la columna</param>
        public static void AddColumntoDataGrid(DataGrid dg, string DataName, Boolean IsVisible, string HeaderText)
        {
            BoundColumn dgCol = new BoundColumn();

            dgCol.DataField = DataName;
            dgCol.Visible = IsVisible;
            dgCol.HeaderText = HeaderText;
            dgCol.SortExpression = DataName;
            dg.Columns.Add(dgCol);
        }
        /// <summary>
        /// Adiciona una columna a un control Datagrid
        /// </summary>
        /// <param name="dg">Datagrig que se va a llenar</param>
        /// <param name="DataName">Nombre del dato a mostrar</param>
        /// <param name="IsVisible">Indica si la columna es visible o no</param>
        /// <param name="HeaderText">Texto del titulo de la columna</param>
        /// <param name="Align">Alineacion de los elementos de la columna</param>
        public static void AddColumntoDataGrid(DataGrid dg, string DataName, Boolean IsVisible, string HeaderText, HorizontalAlign Align)
        {
            BoundColumn dgCol = new BoundColumn();

            dgCol.DataField = DataName;
            dgCol.Visible = IsVisible;
            dgCol.HeaderText = HeaderText;
            dgCol.SortExpression = DataName;
            dgCol.ItemStyle.HorizontalAlign = Align;
            dg.Columns.Add(dgCol);
        }

        /// <summary>
        /// Adiciona una columna hipervinculo a un control Datagrid
        /// </summary>
        /// <param name="dg">Datagrig que se va a llenar</param>
        /// <param name="DataURL">Datos del URL</param>
        /// <param name="Text">Texto del URL</param>
        /// <param name="ColumnTitle">Titulo de la columna</param>
        /// <param name="FrameTarget">Frame de destino de la pagina a desplegar</param>
        public static void AddLinkColumntoDataGrid(DataGrid dg, string DataURL, string Text, string ColumnTitle, string FrameTarget)
        {
            HyperLinkColumn dgCol = new HyperLinkColumn();

            dgCol.DataNavigateUrlField = DataURL;
            dgCol.Text = Text;
            dgCol.HeaderText = ColumnTitle;
            dgCol.Target = FrameTarget;

            dg.Columns.Add(dgCol);
        }

        /// <summary>
        /// Adiciona una columna hipervinculo a un control Datagrid
        /// </summary>
        /// <param name="dg">Datagrig que se va a llenar</param>
        /// <param name="DataURL">Datos del URL</param>
        /// <param name="DataText">Texto del URL</param>
        /// <param name="ColumnTitle">Titulo de la columna</param>
        public static void AddLinkColumntoDataGrid(DataGrid dg, string DataURL, string DataText, string ColumnTitle)
        {
            HyperLinkColumn dgCol = new HyperLinkColumn();

            dgCol.DataNavigateUrlField = DataURL;
            dgCol.DataTextField = DataText;
            dgCol.HeaderText = ColumnTitle;

            dg.Columns.Add(dgCol);
        }

        /// <summary>
        /// Llena un control Web con los datos especificados provenientes de un DataTable
        /// </summary>
        /// <param name="lstCtl">Cualquier control que herede de ListControl</param>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="columnName">Columna donde estan los datos para mostrar</param>
        /// <param name="columnValue">Columna donde estan los valores a almacenar (advertencia! estos valores se convertiran en String)</param>
        /// <param name="clear">Booleano indicando si se borran o no los contenidos previos del control</param>
        public static void Fill(ListControl lstCtl, DataTable dt, string columnName, string columnValue, bool clear)
        {
            if (clear) lstCtl.Items.Clear();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                lstCtl.Items.Add(new ListItem(Tools.ToString(dt, i, columnName), Tools.ToString(dt, i, columnValue)));
            }
        }
        /// <summary>
        /// Llena un control Web con los datos contenidos en un arreglo de DataRows
        /// </summary>
        /// <param name="lstCtl">Cualquier control que herede de ListControl</param>
        /// <param name="dr">Arreglo de DataRows que contienen los datos</param>
        /// <param name="columnName">Columna donde estan los datos para mostrar</param>
        /// <param name="columnValue">Columna donde estan los valores a almacenar (advertencia! estos valores se convertiran en String)</param>
        /// <param name="clear">Booleano indicando si se borran o no los contenidos previos del control</param>
        public static void Fill(ListControl lstCtl, DataRow[] dr, string columnName, string columnValue, bool clear)
        {
            if (clear) lstCtl.Items.Clear();
            for (int i = 0; i < dr.Length; i++)
            {
                lstCtl.Items.Add(new ListItem(Tools.ToString(dr[i], columnName), Tools.ToString(dr[i], columnValue)));
            }
        }
        /// <summary>
        /// Llena un textBox con el dato contenido en la columna indicada de un DataRow
        /// </summary>
        /// <param name="txtCtl">Control TextBox</param>
        /// <param name="dr">DataRow</param>
        /// <param name="columnName">Nombre de la columna a mostrar</param>
        public static void Fill(TextBox txtCtl, DataRow dr, string columnName)
        {
            txtCtl.Text = Tools.ToString(dr, columnName);
        }
        /// <summary>
        /// Llena un Label con el dato contenido en la columna indicada de un DataRow
        /// </summary>
        /// <param name="lblCtl">Control Label</param>
        /// <param name="dr">DataRow</param>
        /// <param name="columnName">Nombre de la columna a mostrar</param>
        public static void Fill(Label lblCtl, DataRow dr, string columnName)
        {
            lblCtl.Text = Tools.ToString(dr, columnName);
        }
        /// <summary>
        /// Permite llenar un control DropDown con un objeto DataTable
        /// </summary>
        /// <param name="cmbCtl">Control que herede de ListControl</param>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="columnName">Columna donde estan los datos para mostrar</param>
        /// <param name="columnValue">Columna donde estan los valores a almacenar (advertencia! estos valores se convertiran en String)</param>
        /// <param name="TypeFill">Infica el tipo de llenado del control</param>
        public static void Fill(DropDownList cmbCtl, DataTable dt, string columnName, string columnValue, FillOptions TypeFill)
        {
            switch (TypeFill)
            {
                case FillOptions.ClearAll:
                    cmbCtl.Items.Clear();
                    break;
                case FillOptions.DontClear:
                    break;
                case FillOptions.PutSelectCaption:
                    cmbCtl.Items.Clear();
                    cmbCtl.Items.Add("[Seleccione]");
                    break;
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cmbCtl.Items.Add(new ListItem(Tools.ToString(dt, i, columnName), Tools.ToString(dt, i, columnValue)));
            }
        }

        /// <summary>
        /// Permite llenar un control DropDown con un objeto DataTable
        /// </summary>
        /// <param name="lstCtl">Control que herede de ListControl</param>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="columnName">Columna donde estan los datos para mostrar</param>
        /// <param name="columnValue">Columna donde estan los valores a almacenar (advertencia! estos valores se convertiran en String)</param>
        /// <param name="TypeFill">Infica el tipo de llenado del control</param>
        public static void Fill(ListBox lstCtl, DataTable dt, string columnName, string columnValue, FillOptions TypeFill)
        {
            switch (TypeFill)
            {
                case FillOptions.ClearAll:
                    lstCtl.Items.Clear();
                    break;
                case FillOptions.DontClear:
                    break;
                case FillOptions.PutSelectCaption:
                    lstCtl.Items.Clear();
                    lstCtl.Items.Add("[Seleccione]");
                    break;
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                lstCtl.Items.Add(new ListItem(Tools.ToString(dt, i, columnName), Tools.ToString(dt, i, columnValue)));
            }
        }
        /// <summary>
        /// Llena un control TreeVie tomando datos desde un DataTable, la funci�n es recursiva y se llama a si misma hasta que rellena todo el TreeView
        /// </summary>
        /// <param name="trvCtl">Control TreeView</param>
        /// <param name="dt">DataTable Orgen de los datos</param>
        /// <param name="columnName">Mobre de la columna a visualizar</param>
        /// <param name="columnValue">Columna Id del DataTable</param>
        /// <param name="columnValueParent">Columna que identifica el padre</param>
        /// <param name="indicePadre">Indice identificador del padre, "En este caso el valor 0 identifica los nodos padres", el primer valor pasado es Null</param>
        /// <param name="nodePadre"></param>
        public static void Fill(TreeView trvCtl, DataTable dt, string columnName, string columnValue, string columnValueParent, int indicePadre, TreeNode nodePadre)
        {   // Crear un DataView con los Nodos que dependen del Nodo padre pasado como par�metro.
            DataView dataViewHijos = new  System.Data.DataView(dt);
            dataViewHijos.RowFilter = dt.Columns[columnValueParent.ToString()].ColumnName + " = " + indicePadre.ToString();
                //columnNameidModuloPadre; 
            // Agregar al TreeView los nodos Hijos que se han obtenido en el DataView.
            foreach (DataRowView  dataRowCurrent in dataViewHijos )
            { TreeNode  nuevoNodo= new TreeNode();
            nuevoNodo.Text = dataRowCurrent[columnName.ToString()].ToString().Trim();
            nuevoNodo.Value = dataRowCurrent[columnValue.ToString()].ToString().Trim();
                // si el par�metro nodoPadre es nulo es porque es la primera llamada, son los Nodos
                // del primer nivel que no dependen de otro nodo.
               
                if (nodePadre == null)
                    {
                        trvCtl.Nodes.Add(nuevoNodo);
                    }
                    else 
                    {
                        nodePadre.ChildNodes.Add(nuevoNodo);
                    }
                    //TreeView trvCtl,DataTable dt, string columnName,string  idModulo, string idModuloPadre ,int indicePadre, TreeNode nodePadre
                    WebTools.Fill(trvCtl, dt, columnName, columnValue, columnValueParent, Int32.Parse(dataRowCurrent[columnValue].ToString()), nuevoNodo);
            }
        }


        /// <summary>
        /// Busca y selecciona el item que tiene el valor especificado en un ListControl
        /// </summary>
        /// <param name="cmbCtl">Cualquier control que herede de ListControl</param>
        /// <param name="val">String conteniendo el valor a especificar</param>
        /// <returns>Un booleano especificando si se encontr� o no el valor especificado</returns>
        public static bool FindInListControl(ListControl cmbCtl, string val)
        {
            for (int i = 0; i < cmbCtl.Items.Count; i++)
            {
                if (cmbCtl.Items[i].Value == val)
                {
                    cmbCtl.SelectedIndex = i;
                    return true;
                }
            }
            cmbCtl.SelectedIndex = -1;
            return false;
        }
        /// <summary>
        /// Busca y selecciona el item que tiene el valor especificado en un ListControl
        /// </summary>
        /// <param name="cmbCtl">Cualquier control que herede de ListControl</param>
        /// <param name="val">String conteniendo el valor a especificar</param>
        /// <param name="multiple">Se�ala si el control es de selecci�n multiple</param>
        /// <returns>Un booleano especificando si se encontr� o no el valor especificado</returns>
        public static bool FindInListControl(ListControl cmbCtl, string val, bool multiple)
        {
            bool found = false;
            //cmbCtl.SelectedIndex =-1;
            for (int i = 0; i < cmbCtl.Items.Count; i++)
            {
                if (cmbCtl.Items[i].Value == val)
                {
                    cmbCtl.Items[i].Selected = true;
                    found = true;
                }
            }
            if (found) return true;
            //cmbCtl.SelectedIndex=-1;
            return false;
        }

        /// <summary>
        /// Busca y selecciona el item que tiene el texto especificado en un ListControl
        /// </summary>
        /// <param name="cmbCtl">Cualquier control que herede de ListControl</param>
        /// <param name="text">String conteniendo el texto a especificar</param>
        /// <returns>Un booleano especificando si se encontr� o no el texto especificado</returns>
        public static bool FindTextInListControl(ListControl cmbCtl, string text)
        {
            for (int i = 0; i < cmbCtl.Items.Count; i++)
            {
                if (cmbCtl.Items[i].Text == text)
                {
                    cmbCtl.SelectedIndex = i;
                    return true;
                }
            }
            cmbCtl.SelectedIndex = -1;
            return false;
        }

        /// <summary>
        /// Busca y selecciona el item que tiene el valor especificado en un ListControl.
        /// La busqueda no es Case Sensitive.
        /// </summary>
        /// <param name="cmbCtl">Cualquier control que herede de ListControl</param>
        /// <param name="val">String conteniendo el valor a especificar</param>
        /// <returns>Un booleano especificando si se encontr� o no el valor especificado</returns>
        public static int FindLowerTextinControl(ListControl cmbCtl, string val)
        {
            int index = -1;
            for (int i = 0; i < cmbCtl.Items.Count; i++)
            {
                if (cmbCtl.Items[i].Text.ToLower() == val.ToLower())
                {
                    index = i;
                }
            }
            return index;
        }

        /// <summary>
        /// Obtiene el valor del item seleccionado actualmente
        /// </summary>
        /// <param name="lstCtl">El Control</param>
        /// <returns>El valor del item, "0" si no tiene seleccionado un item</returns>
        public static string GetValue(ListControl lstCtl)
        {
            if (lstCtl.SelectedItem != null) return lstCtl.SelectedItem.Value;
            else return "0";
        }
        /// <summary>
        /// Selecciona un ListItem en un ListControl
        /// </summary>
        /// <param name="lstCtrl">Control ListControl</param>
        /// <param name="lstItem">Objeto ListItem</param>
        public static void SetSelectedItem(ListControl lstCtrl, ListItem lstItem)
        {
            lstCtrl.SelectedIndex = lstCtrl.Items.IndexOf(lstItem);
        }

        /// <summary>
        /// Selecciona un ListItem en un ListControl
        /// </summary>
        /// <param name="lstCtrl">Control ListControl</param>
        /// <param name="Value">Valor del ListItem que se quiere seleccionar</param>
        public static void SetSelectedItem(ListControl lstCtrl, string Value)
        {
            for (int i = 0; i < lstCtrl.Items.Count; i++)
                if (lstCtrl.Items[i].Value == Value)
                    lstCtrl.SelectedIndex = i;
        }
        //		public static int FindNameColumn(DataTable dt,string val)
        //		{
        //			for(int i=0;i<dt.Columns.Count;i++)
        //			{
        //				if(dt.Columns[i].ColumnName==val)
        //				{					
        //					return i;
        //				}
        //			}
        //			return -1;
        //		}
        //
        //		public static int FindNameColumn(DataGrid dg,string val)
        //		{
        //			for(int i=0;i<dg.Columns.Count;i++)
        //			{
        //				if(dg.Columns[i].HeaderText==val)
        //				{					
        //					return i;
        //				}
        //			}
        //			return -1;
        //		}

        /// <summary>
        /// Convierte los items seleccionados en un ListControl.
        /// </summary>
        /// <param name="lstCtl">Un control ListControl</param>
        /// <param name="valueColumnName">Nombre de la columna para el valor.</param>
        /// <param name="textColumnName">Nombre de la columna para el item.</param>
        /// <returns>Un DataTable conteniendo los items</returns>
        public static DataTable ItemsToDataTable(ListControl lstCtl, string valueColumnName, string textColumnName)
        {
            DataTable dt = new DataTable(lstCtl.ToString());
            dt.Columns.Add(valueColumnName, typeof(string));
            dt.Columns.Add(textColumnName, typeof(string));
            for (int i = 0; i < lstCtl.Items.Count; i++)
            {
                Tools.AddDataRow(dt, lstCtl.Items[i].Value, lstCtl.Items[i].Text);
            }
            return dt;
        }
        /// <summary>
        /// Convierte los items seleccionados en un ListControl.
        /// </summary>
        /// <param name="lstCtl">Un control ListControl</param>
        /// <param name="valueColumnName">Nombre de la columna para el valor.</param>
        /// <param name="textColumnName">Nombre de la columna para el item.</param>
        /// <param name="attributeTitle">Nombre de uan columna adicional</param>
        /// <returns></returns>
        public static DataTable ItemsToDataTable(ListControl lstCtl, string valueColumnName, string textColumnName, string attributeTitle)
        {
            DataTable dt = new DataTable(lstCtl.ToString());
            dt.Columns.Add(valueColumnName, typeof(string));
            dt.Columns.Add(textColumnName, typeof(string));
            dt.Columns.Add(attributeTitle, typeof(string));
            for (int i = 0; i < lstCtl.Items.Count; i++)
            {
                Tools.AddDataRow(dt, lstCtl.Items[i].Value, lstCtl.Items[i].Text, lstCtl.Items[i].Attributes[attributeTitle]);
            }
            return dt;
        }
        /// <summary>
        /// Obtiene el titulo de la columna que sirve para ordenar un datagrid
        /// </summary>
        /// <param name="drg">El Datatable</param>
        /// <param name="sortExpression">La expresi�n de ordenamiento</param>				
        /// <returns>El Texto con el nombre de la columna que ordena el datagrid</returns>
        public static string GetSortExpressionText(DataGrid drg, string sortExpression)
        {
            for (int i = 0; i < drg.Columns.Count; i++)
            {
                if (drg.Columns[i].SortExpression == sortExpression)
                    return drg.Columns[i].HeaderText;
            }
            return "";
        }

        //
        //		public static void AddHyperLinkColumn(DataGrid dtgControl, string Text, DataTable Url)
        //		{
        //			System.Web.UI.WebControls.HyperLinkColumn hlkColumn = new HyperLinkColumn();
        //			hlkColumn.Text = "Seleccionar";
        //		}
        /// <summary>
        /// Determina si el control tiene un item seleccionado.
        /// </summary>
        /// <param name="lstCtrl">Un control ListControl.</param>
        /// <returns>True: Si el control tiene un item seleccionado, False: Si no.</returns>
        public static bool IsSelectedItem(ListControl lstCtrl)
        {
            if (lstCtrl.SelectedIndex >= 0) { return true; }
            else { return false; }
        }

        /// <summary>
        /// Devuelve un ArrayList conteniendo los valores de los items seleccionados
        /// </summary>
        /// <param name="lstCtl">El control de selecci�n</param>
        /// <returns>ArrayList Conteniendo los valores seleccionados</returns>
        public static System.Collections.ArrayList GetSelectedValues(ListControl lstCtl)
        {
            System.Collections.ArrayList selected = new System.Collections.ArrayList();

            for (int i = 0; i < lstCtl.Items.Count; i++)
                if (lstCtl.Items[i].Selected) selected.Add(lstCtl.Items[i].Value);

            return selected;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstCtl"></param>
        /// <returns></returns>
        public static ListItemCollection GetSelectedItems(ListControl lstCtl)
        {
            ListItemCollection selected = new ListItemCollection();

            for (int i = 0; i < lstCtl.Items.Count; i++)
                if (lstCtl.Items[i].Selected) selected.Add(lstCtl.Items[i]);

            return selected;
        }

        /// <summary>
        /// Validar el contenido de un control
        /// </summary>
        /// <param name="txtControl">TextBox que se quiere validar</param>
        /// <param name="content">Tipo de contenido del TextBox</param>
        /// <param name="errorColor">Color con que se resaltar� el TextBox en caso de error</param>
        /// <param name="empty">Permitir el control vacio o no</param>
        /// <returns>True = si el contenido es v�lido, False = si el contenido es erroneo</returns>
        public static bool ValidateControl(TextBox txtControl, ContentType content, System.Drawing.Color errorColor, bool empty)
        {
            txtControl.Text = txtControl.Text.Trim();
            if (txtControl.Text == "" && !empty)
            {
                txtControl.BackColor = errorColor;
                return false;
            }
            if (content != ContentType.AlphaNumeric)
                for (int i = 0; i < txtControl.Text.Length; i++)
                {
                    switch (content)
                    {
                        case ContentType.Numeric:
                            if (!char.IsDigit(txtControl.Text, i))
                            {
                                txtControl.BackColor = errorColor;
                                return false;
                            }
                            break;
                        case ContentType.Text:
                            if (!char.IsLetter(txtControl.Text, i))
                            {
                                txtControl.BackColor = errorColor;
                                return false;
                            }
                            break;
                    }
                }
            txtControl.BackColor = System.Drawing.Color.White;
            return true;
        }

        /// <summary>
        /// Valida la selecci�n de un ListControl 
        /// </summary>
        /// <param name="lstControl">ListControl a validar</param>
        /// <param name="errorColor">Color con que se resaltar� el ListControl en caso de error</param>
        /// <param name="emptyValue">Valor no permitido en la selecci�n</param>
        /// <returns>True = si la selecci�n es v�lida, False = si la selecci�n es erronea</returns>
        public static bool ValidateControl(ListControl lstControl, System.Drawing.Color errorColor, string emptyValue)
        {
            if ((lstControl.SelectedItem == null) || (lstControl.SelectedItem.Value == emptyValue))
            {
                lstControl.BackColor = errorColor;
                return false;
            }
            lstControl.BackColor = System.Drawing.Color.White;
            return true;
        }

        /// <summary>
        /// Mensaje de usuario basado en un Alert
        /// </summary>
        /// <param name="message">Mensaje</param>
        /// <param name="page">P�gina que lanza el mensaje</param>
        public static void MessageBox(string message, System.Web.UI.Page page)
        {
            if (!(message.IndexOf("HTML") > 0 && message.IndexOf("/HTML") > 0))
            {
                message = message.Replace("\r", " ");
                message = message.Replace("\"", "'");
                message = message.Replace("\r\n", " ");
                message = message.Replace("\n", " ");
                message = message.Replace("<br>", " ");
                message = message.Replace("<BR>", " ");
                message = message.Replace("<P>", " ");
                message = message.Replace("</P>", " ");
                message = message.Replace("<p>", " ");
                message = message.Replace("</p>", " ");
            }
            else
            {
                message = "El Origen retorna una cadena HTML";
            }
            page.RegisterClientScriptBlock("Message", "<script language='javascript'>alert(\"" + message.Trim() + "\");</script>");
        }
        /// <summary>
        /// Mensaje de usuario basado en un Confirm
        /// </summary>
        /// <param name="message">Mensaje</param>
        /// <param name="page">P�gina que lanza el mensaje</param>
        /// <param name="OkCancel">Indica si Es un Cofirm</param>
        public static void MessageBox(string message, System.Web.UI.Page page, bool OkCancel)
        {
            //TODO: Not Implemented yet
            if (!(message.IndexOf("HTML") > 0 && message.IndexOf("/HTML") > 0))
            {
                message = message.Replace("\r", " ");
                message = message.Replace("\"", "'");
                message = message.Replace("\r\n", " ");
                message = message.Replace("\n", " ");
                message = message.Replace("<br>", " ");
                message = message.Replace("<BR>", " ");
                message = message.Replace("<P>", " ");
                message = message.Replace("</P>", " ");
                message = message.Replace("<p>", " ");
                message = message.Replace("</p>", " ");
            }
            else
            {
                message = "El Origen retorna una cadena HTML";
            }
            page.RegisterClientScriptBlock("Message", "<script language='javascript'> confirm('" + message.Trim() + "');</script>");
            //			page.RegisterStartupScript("Message", "<script language='javascript'> confirm('" + message.Trim() + "');</script>");
        }

        /// <summary>
        /// Enumeraci�n de los posibles tipos de contenido para un TextBox
        /// </summary>
        public enum ContentType
        {
            /// <summary>
            /// Permite caracteres alfabeticos unicamente
            /// </summary>
            Text = 1,
            /// <summary>
            /// Permite caracteres numericos unicamente
            /// </summary>
            Numeric,
            /// <summary>
            /// Permite caracteres alfanum�ricos
            /// </summary>
            AlphaNumeric
        }
        /// <summary>
        /// Convierte a excel la informacion de un datagrid
        /// </summary>
        /// <param name="dg">Control onDataGrid</param>
        /// <param name="path">Path donde se guardar� el archivo</param>
        public static void DataGridToExcel(DataGrid dg, string path)
        {

            System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);

            ClearControls(dg);
            dg.RenderControl(oHtmlTextWriter);

            string strData = "<HTML><HEAD><meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel\"/></HEAD>";
            strData += oStringWriter.ToString() + "</HTML>";

            FileStream Stream = null;
            StreamWriter FileWriter = null;

            Stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);

            //Abre un objeto para escribir en el archivo
            FileWriter = new StreamWriter(Stream);

            FileWriter.Write(strData);

            //Cierra el flujo al archivo
            if (FileWriter != null) FileWriter.Close();

            //Cierra el objeto Writer
            if (Stream != null) Stream.Close();
        }
        /// <summary>
        /// Limpia los controles de un control padre
        /// </summary>
        /// <param name="control">Objeto Control</param>
        public static void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }

            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                    }
                    catch
                    {
                    }
                    control.Parent.Controls.Remove(control);
                }
                else
                    if (control.GetType().GetProperty("Text") != null)
                    {
                        LiteralControl literal = new LiteralControl();
                        control.Parent.Controls.Add(literal);
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
                        control.Parent.Controls.Remove(control);
                    }
            }
            return;
        }

        /// <summary>
        /// Verifica si un valor es numerico
        /// </summary>
        /// <param name="Value">Valor a comparar</param>
        /// <returns>Booleano</returns>
        public static bool VerifyIsDecimal(string Value)
        {
            bool valid = false;
            for (int j = 0; j < Value.Length; j++)
            {
                if (!char.IsDigit(Value.ToString(), j))
                {
                    if (Value[j].ToString() == ".")
                        valid = true;
                    else
                    {
                        valid = false;
                        return valid;
                    }
                }
                else
                {
                    valid = true;
                }
            }
            return valid;
        }
        /// <summary>
        /// Verifica si un valor es decimal
        /// </summary>
        /// <param name="Value">Valor a comparar</param>
        /// <returns>Booleano</returns>
        public static bool VerifyIsNumeric(string Value)
        {
            bool valid = false;
            for (int j = 0; j < Value.Length; j++)
            {
                if (!char.IsDigit(Value.ToString(), j))
                {
                    valid = false;
                    break;
                }
                else
                {
                    valid = true;
                }
            }
            return valid;
        }

        public static void ResetControls(ControlCollection _ColtrolCollection)
        {

            foreach (Control _Object in _ColtrolCollection)
            {
                switch (_Object.GetType().ToString())
                {
                    case "System.Web.UI.WebControls.TextBox":
                        ((TextBox)(_Object)).Text = "";
                        break;
                    case "System.Web.UI.WebControls.DropDownList":
                        ((DropDownList)(_Object)).SelectedIndex = -1;
                        break;
                    case "System.Web.UI.WebControls.CheckBox":
                        ((CheckBox)(_Object)).Checked = false;
                        break;
                    case "System.Web.UI.WebControls.GridView":
                        break;

                }
                if (_Object.Controls.Count > 0)
                {
                    ResetControls(_Object.Controls);

                }
            }

        }
    }
}
