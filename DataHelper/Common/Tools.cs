using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DI.DataHelper.Common
{
    /// <summary>
    /// Contiene funciones recuperar datos de DataTables, DataRows u Objects.
    /// Las funciones aqui contenidas hacen los chequeos pertientes para DBNull o null (nothing en VB.NET)
    /// y en caso de encontrarse estos valores devuelven el valor por defecto del tipo de dato correspondiente.
    /// 
    /// Si es necesario se hace un casting al tipo dato especificado
    /// </summary>
    public class Tools
    {


        /// <summary>
        /// Verifica si un string es una fecha.
        /// </summary>
        /// <param name="fecha">valor de fecha a evaluar</param>
        /// <returns>El Booleano</returns>
        public static bool IsDate(string fecha)
        {
            bool isDate = true;

            try
            {
                DateTime dt = DateTime.Parse(fecha);
            }
            catch
            {
                isDate = false;
            }
            return isDate;
        }
        
        
        
        
        /// <summary>
        /// Devuelve el Booleano en la columna y fila del DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Booleano</returns>
        public static bool ToBoolean(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return false;
            return Convert.ToBoolean(dt.Rows[row][columnName]);
        }
        /// <summary>
        /// Devuelve el Booleano en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Booleano</returns>
        public static bool ToBoolean(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return false;
            return Convert.ToBoolean(dr[columnName]);
        }
        /// <summary>
        /// Devuelve el Booleano contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El Boooleano</returns>
        public static bool ToBoolean(object o)
        {
            if (o == null || o == DBNull.Value) return false;
            return Convert.ToBoolean(o);
        }
        /// <summary>
        /// Devuelve el Byte en la columna, fila y DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Byte</returns>
        public static byte ToByte(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return 0;
            return Convert.ToByte(dt.Rows[row][columnName]);
        }
        /// <summary>
        /// Devuelve el Byte en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Byte</returns>
        public static byte ToByte(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return 0;
            return Convert.ToByte(dr[columnName]);
        }
        /// <summary>
        /// Devuelve el Byte contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El Byte</returns>
        public static byte ToByte(object o)
        {
            if (o == null || o == DBNull.Value) return 0;
            return Convert.ToByte(o);
        }
        /// <summary>
        /// Devuelve el Int16 en la columna y fila del DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Int16</returns>
        public static short ToInt16(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return 0;
            return Convert.ToInt16(dt.Rows[row][columnName]);
        }
        /// <summary>
        /// Devuelve el Int16 en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Int16</returns>
        public static short ToInt16(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return 0;
            return Convert.ToInt16(dr[columnName]);
        }
        /// <summary>
        /// Devuelve el Int16 contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El Int16</returns>
        public static short ToInt16(object o)
        {
            if (o == null || o == DBNull.Value) return 0;
            return Convert.ToInt16(o);
        }
        /// <summary>
        /// Devuelve el Int32 en la columna, fila y DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Int32</returns>
        public static int ToInt32(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return 0;
            return Convert.ToInt32(dt.Rows[row][columnName]);
        }
        /// <summary>
        /// Devuelve el Int32 en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Int32</returns>
        public static int ToInt32(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return 0;
            return Convert.ToInt32(dr[columnName]);
        }
        /// <summary>
        /// Devuelve el Int32 contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El Int32</returns>
        public static int ToInt32(object o)
        {
            if (o == null || o == DBNull.Value || o.ToString() == "") return 0;
            return Convert.ToInt32(o);
        }
        /// <summary>
        /// Devuelve el Int64 en la columna y fila del DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Int64</returns>
        public static long ToInt64(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return 0;
            return Convert.ToInt64(dt.Rows[row][columnName]);
        }
        /// <summary>
        /// Devuelve el Int64 en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Int64</returns>
        public static long ToInt64(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return 0;
            return Convert.ToInt64(dr[columnName]);
        }

        /// <summary>
        /// Devuelve el Int64 contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El Int64</returns>
        public static long ToInt64(object o)
        {
            if (o == null || o == DBNull.Value) return 0;
            return Convert.ToInt64(o);
        }
        /// <summary>
        /// Devuelve el string en la columna, fila y DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El string</returns>
        public static string ToString(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return "";
            return Convert.ToString(dt.Rows[row][columnName]);
        }
        /// <summary>
        /// Devuelve el String en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El String</returns>
        public static string ToString(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return "";
            return Convert.ToString(dr[columnName]);
        }
        /// <summary>
        /// Devuelve el String contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El String</returns>
        public static string ToString(object o)
        {
            if (o == null || o == DBNull.Value) return "";
            return Convert.ToString(o);
        }
        /// <summary>
        /// Devuelve el Single en la columna y fila del DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Single</returns>
        public static float ToSingle(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return 0;
            return Convert.ToSingle(dt.Rows[row][columnName]);
        }

        /// <summary>
        /// Devuelve el Single en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Single</returns>
        public static float ToSingle(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return 0;
            return Convert.ToSingle(dr[columnName]);
        }
        /// <summary>
        /// Devuelve el Single contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El Single</returns>
        public static float ToSingle(object o)
        {
            if (o == null || o == DBNull.Value) return 0;
            return Convert.ToSingle(o);
        }
        /// <summary>
        /// Devuelve el Double en la columna y fila del DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Double</returns>
        public static double ToDouble(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return 0;
            return Convert.ToDouble(dt.Rows[row][columnName]);
        }
        /// <summary>
        /// Devuelve el Double en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Double</returns>
        public static double ToDouble(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return 0;
            return Convert.ToDouble(dr[columnName]);
        }
        /// <summary>
        /// Devuelve el Double contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El Double</returns>
        public static double ToDouble(object o)
        {
            if (o == null || o == DBNull.Value) return 0;
            return Convert.ToDouble(o);
        }
        /// <summary>
        /// Devuelve el DateTime en la columna, fila y DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El DateTime</returns>
        public static DateTime ToDateTime(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return DateTime.MinValue;
            return Convert.ToDateTime(dt.Rows[row][columnName]);
        }
        /// <summary>
        /// Devuelve el DateTime en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El DateTime</returns>
        public static DateTime ToDateTime(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return DateTime.MinValue;
            return Convert.ToDateTime(dr[columnName]);
        }
        /// <summary>
        /// Devuelve el DateTime contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El DateTime</returns>
        public static DateTime ToDateTime(object o)
        {
            if (o == null || o == DBNull.Value) return DateTime.MinValue;
            return Convert.ToDateTime(o);
        }
        /// <summary>
        /// Devuelve el Decimal en la columna y fila del DataTable especificado.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="row">Numero de la Fila</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Decimal</returns>
        public static decimal ToDecimal(DataTable dt, int row, string columnName)
        {
            if (dt.Rows[row].IsNull(columnName)) return 0;
            return Convert.ToDecimal(dt.Rows[row][columnName]);
        }
        /// <summary>
        /// Devuelve el Decimal en la columna del DataRow especificado
        /// </summary>
        /// <param name="dr">DataRow que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>El Decimal</returns>
        public static decimal ToDecimal(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName)) return 0;
            return Convert.ToDecimal(dr[columnName]);
        }
        /// <summary>
        /// Devuelve el Decimal contenido en el Object especificado
        /// </summary>
        /// <param name="o">Objecto que contiene el valor</param>
        /// <returns>El Decimal</returns>
        public static decimal ToDecimal(object o)
        {
            if (o == null || o == DBNull.Value || o == "") return 0;
            return Convert.ToDecimal(o);
        }
        /// <summary>
        /// Devuelve la fila donde el valor especificado es igual al contenido en la columna dada.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos</param>
        /// <param name="columnName">Nombre de la columna donde se va buscar</param>
        /// <param name="value">Valor que se va a buscar</param>
        /// <returns>El n�mero de la fila, -1 si no se encuentra</returns>
        public static int FindInDataTable(DataTable dt, string columnName, object @value)
        {
            int col = dt.Columns.IndexOf(columnName);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][col].Equals(@value))
                {
                    return i;
                }
            }
            return -1;
        }
        /// <summary>
        /// Actualiza un DataRow de un DataTable
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="rowIndex">indice del DataRow a actualizar</param>
        /// <param name="values">Pares de valores de la forma {Nombre de la columna,valor}</param>
        public static void UpdateDataRow(DataTable dt, int rowIndex, params object[] values)
        {
            if (rowIndex > dt.Rows.Count - 1) return;
            DataRow dr = dt.Rows[rowIndex];
            for (int i = 0; i < values.Length; i += 2)
            {
                if (!dt.Columns.Contains((string)values[i])) continue;
                dr[(string)values[i]] = values[i + 1];
            }
        }
        /// <summary>
        /// Agrega los valores especificados a un DataTable
        /// </summary>
        /// <param name="dt">DataTable a ser modificado</param>
        /// <param name="values">secuencia valores, deben estar en el orden correspondiente en el DataTAble</param>
        public static void AddDataRow(DataTable dt, params object[] values)
        {
            dt.Rows.Add(values);
        }
        /// <summary>
        /// Agrega un array de DataRows a un DataTable.  los DataRows deben tener el mismo esquema que el DataTable
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="dr">Arreglo de DataTable</param>
        public static void AddDataRow(DataTable dt, DataRow[] dr)
        {
            for (int i = 0; i < dr.Length; i++)
            {
                dt.ImportRow(dr[i]);
            }
        }
        /// <summary>
        /// Ordena los contenidos de un DataTable de acuerdo con los valores contenidos en la columna especificada
        /// </summary>
        /// <param name="dt">DataTable para ordenar</param>
        /// <param name="NameField">Nombre de la columna por donde se va ordenar</param>
        /// <returns>DataTable con los datos ordenados</returns>
        private static DataTable SortTable(DataTable dt, string NameField)
        {
            DataTable dtSort = dt.Clone();
            DataRow[] dr = dt.Select("", NameField);
            DataRow[] draux = (DataRow[])dr.Clone();

            for (int i = 0; i < draux.Length; i++) dtSort.ImportRow(draux[i]);
            return dtSort;
        }
        /// <summary>
        /// Filtra los datos de un DataTable segun el criterio especificado
        /// </summary>
        /// <param name="dt">DataTable para filtrar</param>
        /// <param name="Filter">String conteniendo el filtro</param>
        /// <returns>DataTable con los datos filtrados</returns>
        public static DataTable ApplyFilter(DataTable dt, string Filter)
        {
            DataTable dtResults = dt.Clone();
            DataRow[] dr = dt.Select(Filter);
            DataRow[] draux = (DataRow[])dr.Clone();
            for (int i = 0; i < draux.Length; i++) dtResults.ImportRow(draux[i]);
            return dtResults;

        }

        /// <summary>
        /// Busca un String especificado en un arreglo de Strings
        /// </summary>
        /// <param name="vector">El array de Strings</param>
        /// <param name="val">String a buscar</param>
        /// <returns>La posici�n donde esta el valor, -1 si no lo encuentra</returns>
        public static int FindInVec(string[] vector, string val)
        {
            for (int i = 0; i < vector.Length; i++)
            {
                if (vector[i] == val)
                {
                    return i;
                }
            }
            return -1;
        }
        /// <summary>
        /// Verifica que la excepcion (o excepcion interna) sea una excepcion de SQL debida a un problema de integridad referencial.
        /// </summary>
        /// <param name="e">La Excepcion</param>
        /// <returns>Booleano indicando si es debido a integridad referencial o no.</returns>
        public static bool IsIntegrityException(Exception e)
        {
            SqlException sqlEx;
            sqlEx = e as SqlException;
            if (sqlEx != null)
            {
                if (sqlEx.Class == 16 || sqlEx.Class == 14) return true;
            }
            if (e.InnerException != null) sqlEx = e.InnerException as SqlException;
            if (sqlEx != null)
            {
                if (sqlEx.Class == 16 || sqlEx.Class == 14) return true;
            }
            return false;
        }
        /// <summary>
        /// Convierte el contenido de la columna de un DataTable a un string separados por la cadena especificada.
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos.</param>
        /// <param name="columnName">Nombre de la columna que contiene los datos.</param>
        /// <param name="separator">Separador de la lista.</param>
        /// <returns></returns>
        public static string ColumnToString(DataTable dt, string columnName, string separator)
        {
            if (dt.Rows.Count == 0) return "";
            int col = dt.Columns.IndexOf(columnName);
            System.Text.StringBuilder sb = new System.Text.StringBuilder(dt.Rows[0][col].ToString());
            for (int i = 1; i < dt.Rows.Count; i++)
            {
                sb.Append(separator);
                sb.Append(dt.Rows[i][col]);
            }
            return sb.ToString();
        }
        /// <summary>
        /// Convierte un datatable en un array Bi-Dimensional tipo object
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos.</param>	
        /// <returns>Object Array bidimensional con los Datos del Datatable </returns>
        public static object[,] DataTableToArray(DataTable dt)
        {

            int rowCnt = dt.Rows.Count;
            int colCnt = dt.Columns.Count;

            object[,] arr = new object[rowCnt, colCnt];

            for (int xIndex = 0; xIndex < dt.Rows.Count; xIndex++)
            {
                for (int yIndex = 0; yIndex < dt.Columns.Count; yIndex++)
                {
                    object cellData = dt.Rows[xIndex][yIndex];
                    arr[xIndex, yIndex] = cellData;
                }
            }

            return arr;


        }
        /// <summary>
        /// Convierte un datatable en un array Bi-Dimensional tipo object		
        /// </summary>
        /// <param name="dt">DataTable que contiene los datos.</param>
        /// <param name="columns">Columnas que contienen los datos a ser pasados al array</param>
        /// <returns>Object Array bidimensional con los Datos del Datatable </returns>
        public static object[,] DataTableToArray(DataTable dt, params string[] columns)
        {

            int rowCnt = dt.Rows.Count;
            int colCnt = columns.Length;

            object[,] arr = new object[rowCnt, colCnt];

            for (int xIndex = 0; xIndex < dt.Rows.Count; xIndex++)
            {
                for (int yIndex = 0; yIndex < columns.Length; yIndex++)
                {
                    object cellData = dt.Rows[xIndex][columns[yIndex]];
                    arr[xIndex, yIndex] = cellData;
                }
            }

            return arr;


        }
        /// <summary>
        /// Escoje los elementos distintintos en una columna dada de un DataTable
        /// </summary>
        /// <param name="dt">DataTable donde se realizar� la consulta.</param>
        /// <param name="columnName">Nombre de la columna que contiene los datos.</param>
        /// <returns>Arreglo de Objects con los valores distintos</returns>
        public static object[] SelectDistinct(DataTable dt, string columnName)
        {
            int col = dt.Columns.IndexOf(columnName);
            object tmp;
            System.Collections.ArrayList result = new System.Collections.ArrayList();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                tmp = dt.Rows[i][col];
                if (result.IndexOf(tmp) < 0) result.Add(tmp);
            }
            return result.ToArray();
        }
        /// <summary>
        /// Retorna el error completo de la exepcion ingresada
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string GetAllError(Exception ex)
        {
            //Copia el objeto de exepcion
            Exception t = ex;
            System.Text.StringBuilder StrResults = new System.Text.StringBuilder();

            //Recorre hasta que no hayan mas exepciones
            while (t != null)
            {
                StrResults.Append(t.Message + " ::: ");
                t = t.InnerException;
            }

            StrResults.Append(" StactTrace: " + ex.StackTrace);
            return StrResults.ToString();
        }
        /// <summary>
        /// Constructor con separador
        /// </summary>
        /// <param name="ex">Exepcion</param>
        /// <param name="Separator">Cadena que separa las exepciones</param>
        /// <returns></returns>
        public static string GetAllError(Exception ex, string Separator)
        {
            //Copia el objeto de exepcion
            Exception t = ex;
            System.Text.StringBuilder StrResults = new System.Text.StringBuilder();

            //Recorre hasta que no hayan mas exepciones
            while (t != null)
            {
                StrResults.Append(t.Message + Separator);
                t = t.InnerException;
            }
            return StrResults.ToString();
        }

        /// <summary>
        /// Convierte un DataTable a un string XML
        /// </summary>
        /// <param name="dt">DataTable con la informaci�n</param>
        /// <param name="dataTableName">Nombre del DataTable</param>
        /// <returns>Cadena XML con la representaci�n del DataTable</returns>
        public static string DataTableToXML(DataTable dt, string dataTableName)
        {
            DataSet dsContents = new DataSet("Contents_" + dataTableName);
            dt.TableName = dataTableName;
            dsContents.Tables.Add(dt);
            return dsContents.GetXml();
        }

        /// <summary>
        /// Convierte un DataTable a un string separado por valores en filas y columnas(ejemplo, ","y ";")
        /// </summary>
        /// <param name="dt">DataTable con la informaci�n</param>
        /// <param name="rowSeparator">Separador de las filas</param>
        /// <param name="columnSeparator">Separador de la columnas</param>
        /// <param name="includeColumnNames">Si incluye o no nombres de las columnas</param>
        /// <returns>Cadena con la representaci�n del DataTable</returns>
        public static string DataTableToString(DataTable dt, string rowSeparator, string columnSeparator, bool includeColumnNames)
        {
            string localData = "";
            if (dt == null || dt.Rows.Count < 1) return localData;
            //Primera fila con nombres de las columnas
            if (includeColumnNames)
            {
                for (int i = 0; i < dt.Columns.Count; i++) localData += ToString(dt.Columns[i].ColumnName) + columnSeparator;
                localData += rowSeparator;
            }
            // A partir de la segunda fila con los datos
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++) localData += ToString(dt.Rows[i][j]) + columnSeparator;
                localData.Remove(localData.Length - 1, 1);
                localData += rowSeparator;
            }
            localData.Remove(localData.Length - 1, 1);
            return localData;
        }

        /// <summary>
        /// Enumeraci�n para el tipo de ordenamiento de un DataTable
        /// </summary>		
        public enum OrderType
        {
            /// <summary>
            /// Orden Ascendente
            /// </summary>
            ASC = 0,
            /// <summary>
            /// Orden Descendente
            /// </summary>
            DESC
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="ColumName"></param>
        /// <returns></returns>
        public static decimal DataTableSum(DataTable dt, string ColumName)
        {
            decimal sum = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sum += ToDecimal(dt, i, ColumName);
            }

            return sum;
        }

        /// <summary>
        /// Ordenar un datatable
        /// </summary>
        /// <param name="dt">Datatable a ordenar</param>
        /// <param name="FieldName">Campo por el que se va a ordenar</param>
        /// <param name="SortUp">True=Ascendente, False=Descendente</param>
        /// <returns></returns>
        public static DataTable SortDataTable(DataTable dt, string FieldName, bool SortUp)
        {
            if (SortUp) { FieldName += " ASC"; }
            else { FieldName += " DESC"; }

            DataTable dtResults = dt.Clone();
            DataRow[] dr = dt.Select("", FieldName);
            DataRow[] draux = (DataRow[])dr.Clone();
            for (int i = 0; i < draux.Length; i++) dtResults.ImportRow(draux[i]);
            return dtResults;
        }
        /// <summary>
        /// Guarda un datatable en un CSV
        /// </summary>
        /// <param name="dtExport">DataTable de entrada</param>
        /// <param name="PathFileName">Nombre del archivo</param>
        /// <param name="PutHeader">Si hay que colocar titulos de columnas</param>
        /// <param name="CharSeparator">Separador de caracteres</param>
        public static void DataTableToFile(DataTable dtExport, string PathFileName, bool PutHeader, string CharSeparator)
        {
            FileStream Stream = null;
            StreamWriter FileWriter = null;

            try
            {
                //Debe de haber informacion que exportar
                if (dtExport == null || PathFileName == "") return;

                //si el archivo existe lo elimina
                if (File.Exists(PathFileName)) File.Delete(PathFileName);

                //Abre un flujo a un archivo en modo escritura
                Stream = new FileStream(PathFileName, FileMode.OpenOrCreate, FileAccess.Write);

                //Abre un objeto para escribir en el archivo
                FileWriter = new StreamWriter(Stream);

                //Si hay que colocar los encabezados de la tabla
                if (PutHeader)
                {
                    //Se escribe el primero para no repetir el separador
                    FileWriter.Write(dtExport.Columns[0].Caption);

                    //Escribir los titulos de las columnas del datatable
                    for (int j = 1; j < dtExport.Columns.Count; j++)
                        FileWriter.Write(CharSeparator + dtExport.Columns[j].Caption);

                    //Escribe la linea escrita
                    FileWriter.WriteLine();
                }

                //Si hay filas en el dtExport
                if (dtExport.Rows.Count > 0)
                {
                    //Recorrer cada fila y escribirla en el archivo fila por fila
                    for (int i = 0; i < dtExport.Rows.Count; i++)
                    {
                        //Escribe el primer datos para no repetir el separador
                        FileWriter.Write(dtExport.Rows[i][0].ToString());

                        //Escribe los demas datos
                        for (int n = 1; n < dtExport.Columns.Count; n++)
                            FileWriter.Write(CharSeparator + dtExport.Rows[i][n].ToString());

                        //Escribe la linea
                        FileWriter.WriteLine();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //Cierra el flujo al archivo
                if (FileWriter != null) FileWriter.Close();

                //Cierra el objeto Writer
                if (Stream != null) Stream.Close();
            }
        }

        /// <summary>
        /// Filtra un datatable segun el filtro proporcionado
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="Filter"></param>
        /// <param name="Sort"></param>
        /// <returns></returns>
        public static DataTable FilterDataTable(DataTable dt, string Filter, string Sort)
        {
            DataTable dtAux = new DataTable();
            dtAux = dt.Clone();
            DataRow[] VecRows = dt.Select(Filter, Sort);
            DataRow dtRow1;

            foreach (DataRow dtRow in VecRows)
            {
                dtRow1 = dtAux.NewRow();
                dtRow1.ItemArray = dtRow.ItemArray;
                dtAux.Rows.Add(dtRow1);
            }
            return dtAux;
        }
        public static TimeSpan CalcularHorasLuz(DateTime fechaIni, DateTime fechaFin)
        {
            try
            {
                int dias = 0;
                TimeSpan hora6 = TimeSpan.FromHours(6);
                TimeSpan hora18 = TimeSpan.FromHours(18);
                TimeSpan horasLuz;

                if (fechaIni.CompareTo(fechaFin) >= 0)
                {
                    return TimeSpan.FromHours(0);
                }

                dias = TimeSpan.FromTicks(fechaFin.Ticks).Days - TimeSpan.FromTicks(fechaIni.Ticks).Days;   //D�as transcurridos

                horasLuz = TimeSpan.FromHours((dias + 1) * 12);  //N�mero de horas luz posibles por el n�mero de d�as transcurridos

                TimeSpan horas1 = fechaIni.TimeOfDay.Subtract(hora6);
                TimeSpan horas2 = hora18.Subtract(fechaFin.TimeOfDay);

                if (horas1.TotalHours >= 0 && horas1.TotalHours < 12)
                {
                    horasLuz = horasLuz.Subtract(horas1);
                }
                else if (horas1.TotalHours >= 12)
                {
                    horasLuz = horasLuz.Subtract(TimeSpan.FromHours(12));
                }
                if (horas2.TotalHours >= 0 && horas2.TotalHours < 12)
                {
                    horasLuz = horasLuz.Subtract(horas2);
                }
                else if (horas2.TotalHours >= 12)
                {
                    horasLuz = horasLuz.Subtract(TimeSpan.FromHours(12));
                }

                return horasLuz;
            }
            catch (Exception ex)
            {
                return TimeSpan.FromHours(0);
            }
        }

        public static string ConstruirCondiText(string campo, object[] args)
        {
            string cond = "";
            string condIn = "";
            string condNoIn = "";
            string condLike = "";

            if (args.Length % 2 == 0 && args.Length >= 2)
            {
                for (int i = 0; i < args.Length; i = i + 2)
                {
                    switch (args[i].ToString())
                    {
                        case "=":
                            condIn += "'" + args[i + 1].ToString().Trim() + "',";
                            break;
                        case "!=":
                            condNoIn += "'" + args[i + 1].ToString().Trim() + "',";
                            break;
                        case "like":
                            condLike += "%" + args[i + 1].ToString().Trim() + "%";
                            break;
                    }
                }

                if (condIn.Trim() != "")
                {
                    condIn = condIn.Remove(condIn.LastIndexOf(","));
                    condIn = campo + " IN (" + condIn + ")";
                    cond = condIn + " AND ";
                }
                if (condNoIn.Trim() != "")
                {
                    condNoIn = condNoIn.Remove(condNoIn.LastIndexOf(","));
                    condNoIn = campo + " NOT IN (" + condNoIn + ")";
                    cond = condNoIn + " AND ";
                }
                if (condLike.Trim() != "")
                {
                    condLike = campo + " LIKE '" + condLike + "' ";
                    cond = condLike + " AND ";
                }
                cond = cond.Remove(cond.LastIndexOf(" AND "));
            }
            return cond;
        }

        public static string ConstruirCondiFecha(string campo, object[] args)
        {
            string cond = "";
            DateTime fecha;

            if (args.Length % 2 == 0 && args.Length >= 2)
            {
                for (int i = 0; i < args.Length; i = i + 2)
                {
                    if (DateTime.TryParse(args[i + 1].ToString().Trim(), out fecha))
                    {
                        fecha = fecha.Date;
                        switch (args[i].ToString())
                        {
                            case "=":
                                cond += campo + " >= '" + fecha.ToString("dd/MM/yyyy") + "'";
                                cond += " AND " + campo + " < '" + fecha.AddDays(1).ToString("dd/MM/yyyy") + "'";
                                break;
                            case "!=":
                                cond += campo + " < '" + fecha.ToString("dd/MM/yyyy") + "'";
                                cond += " AND " + campo + " >= '" + fecha.AddDays(1).ToString("dd/MM/yyyy") + "'";
                                break;
                            case ">=":
                                cond += campo + " >= '" + fecha.ToString("dd/MM/yyyy") + "'";
                                break;
                            case ">":
                                cond += campo + " > '" + fecha.AddDays(1).ToString("dd/MM/yyyy") + "'";
                                break;
                            case "<=":
                                cond += campo + " < '" + fecha.AddDays(1).ToString("dd/MM/yyyy") + "'";
                                break;
                            case "<":
                                cond += campo + " < '" + fecha.ToString("dd/MM/yyyy") + "'";
                                break;
                        }
                    }
                }
            }
            return cond;
        }

        public static string ConstruirCondiNum(string campo, object[] args)
        {
            string cond = "";

            if (args.Length % 2 == 0 && args.Length >= 2)
            {
                for (int i = 0; i < args.Length; i = i + 2)
                {
                    switch (args[i].ToString())
                    {
                        case "=":
                            cond += campo + " = " + args[i + 1].ToString().Trim();
                            break;
                        case "!=":
                            cond += campo + " != " + args[i + 1].ToString().Trim();
                            break;
                        case ">=":
                            cond += campo + " >= " + args[i + 1].ToString().Trim();
                            break;
                        case ">":
                            cond += campo + " > " + args[i + 1].ToString().Trim();
                            break;
                        case "<=":
                            cond += campo + " <= " + args[i + 1].ToString().Trim();
                            break;
                        case "<":
                            cond += campo + " < " + args[i + 1].ToString().Trim();
                            break;
                    }
                }
            }
            return cond;
        }

        public static bool CreateTextDelimiterFileSeparadoPorComas(string fileName, DataTable dt,string Delimitador =",")
        {
            try
            {
                int col = 0;
                string value = String.Empty;

                // Creamos el archivo de texto con la codificaci�n por defecto.
                StreamWriter sw = new StreamWriter(fileName, false, Encoding.Default);

                // La primera l�nea del archivo de texto contiene
                // el nombre de los campos.
                foreach (DataColumn dc in dt.Columns)
                {
                    // Incluimos el nombre del campo entre comillas dobles.
                    value += dc.ColumnName + Delimitador;
                }

                sw.WriteLine(value.Remove(value.Length - 1, 1));
                value = String.Empty;

                // Recorremos todas las filas del objeto DataTable
                // incluido en el conjunto de datos.
                //
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        value += row[col].ToString() + Delimitador;

                        // Siguiente columna
                        col += 1;

                    }

                    // Al escribir los datos en el archivo, elimino el
                    // �ltimo car�cter delimitador de la fila.
                    //
                    sw.WriteLine(value.Remove(value.Length - 1, 1));
                    value = String.Empty;
                    col = 0;

                } // Siguiente fila

                // Nos aseguramos de cerrar el archivo
                //
                sw.Close();

                // Se ha creado con �xito el archivo de texto.
                return true;
            }

            catch (Exception ex)
            {
                return false;
            }

        }

        public static bool CreateTextDelimiterFile(string fileName,DataTable dt)
        {
            try
            {
                int col = 0;
                string value = String.Empty;

                // Creamos el archivo de texto con la codificaci�n por defecto.
                StreamWriter sw = new StreamWriter(fileName, false, Encoding.Default);

                // La primera l�nea del archivo de texto contiene
                // el nombre de los campos.
                foreach (DataColumn dc in dt.Columns)
                {
                    // Incluimos el nombre del campo entre comillas dobles.
                    value += dc.ColumnName + '\t';
                }

                sw.WriteLine(value.Remove(value.Length - 1, 1));
                value = String.Empty;

                // Recorremos todas las filas del objeto DataTable
                // incluido en el conjunto de datos.
                //
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        value +=  row[col].ToString() + '\t';

                        // Siguiente columna
                        col += 1;

                    }

                    // Al escribir los datos en el archivo, elimino el
                    // �ltimo car�cter delimitador de la fila.
                    //
                    sw.WriteLine(value.Remove(value.Length - 1, 1));
                    value = String.Empty;
                    col = 0;

                } // Siguiente fila

                // Nos aseguramos de cerrar el archivo
                //
                sw.Close();

                // Se ha creado con �xito el archivo de texto.
                return true;
            }

            catch (Exception ex)
            {
                return false;
            }

        }

    }
}
